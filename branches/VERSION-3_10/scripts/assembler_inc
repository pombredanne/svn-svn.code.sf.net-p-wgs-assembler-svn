#!/usr/local/bin/bash
#
###########################################################################
#
# This file is part of Celera Assembler, a software program that 
# assembles whole-genome shotgun reads into contigs and scaffolds.
# Copyright (C) 1999-2004, Applera Corporation. All rights reserved.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received (LICENSE.txt) a copy of the GNU General Public 
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
###########################################################################
#
#
# Usage: $ assembler_inc [-P] [-Q] (-c|-a) arg1 arg2
#
# -c   : Create new project stores.
# -a   : Append to existant project stores.
# arg1 : The first argument is the genome name.
# arg2 : The second argument is the file name for a .frg file starting 
#        with ADT message and followed by a batch of FRG, DST, ...
#        messages to be processed.
#
#######################################################################
NEW_INC_MODE="-a -i"
OLD_INC_MODE="-a"
ANALYSIS_MODE="-A 1"

rc=0
trap "rm -f $s $t; exit \$rc" 0
case $? in 0);; *) rc=$?; exit;; esac
set -- `getopt acAPQX "$@"`

while
	test X"$1" != X--
do
	case "$1"
	in
	-a)	NEW_INC_MODE="-a -i"; OLD_INC_MODE="-a"
		shift 1;;
	-c)	NEW_INC_MODE="-f -c -o" ; OLD_INC_MODE="-f"
		shift 1;;
	-P)	OUTPUT_MODE="-P"
		shift 1;;
	-Q)	SIM_MODE="-XQ"
		shift 1;;
        -A)	NEW_INC_MODE="-A -i"; OLD_INC_MODE="-a"
		shift 1;;
	*)	echo "invalid parameter $1" 1>&2
                exit
		shift
		;;
	esac
done
shift
#
# The command options are now parsed.
#
##########################################################
#
Sprefix=${1}
Bfile=${2}

echo "Began the Celera Incremental Assembler at " `date` " in " `pwd` "."
echo "By the way, this script depends on AS_BIN=${AS_BIN}."
echo "* The project name is <${Sprefix}>."
echo "* The batch file is <${Bfile}>."
#
gkpStore=${Sprefix}.gkpStore
urcStore=${Sprefix}.urcStore
frgStore=${Sprefix}.frgStore
fgbStore=${Sprefix}.fgbStore

#Bprefix=`GateKeeperNextName $Sprefix ${gkpStore}`

#AS_BIN="${AS_ROOT}/bin"
#REPEAT_LIB="${AS_ROOT}/lib/$Sprefix.lib"
#if [ ! -e ${REPEAT_LIB} ]; then
#  REPEAT_LIB="${AS_ROOT}/lib/drosophila_repeats.lib" ;
#fi

TIMER="/usr/bin/time"

# CELSIM_CMD="${AS_BIN}/celsim"
GATEKEEPER_CMD="${AS_BIN}/gatekeeper ${OUTPUT_MODE} ${SIM_MODE}"
# Gatekeeper needs -N for the Chromosome 22 FASTA file.
#URCSCREENER_CMD="${AS_BIN}/urc_screener -ra ${OUTPUT_MODE}"
URCSCREENER_CMD="${AS_BIN}/lsf_screener.sh -R -s -n 10000 -q assembly -v 1 ${OUTPUT_MODE} ${SIM_MODE}"
OVERLAP_CMD="${AS_BIN}/overlap -w ${OUTPUT_MODE}"
#OVERLAP_CMD="${AS_BIN}/overlap_script.pl -w ${OUTPUT_MODE} ${SIM_MODE}"
FGB_CMD="${AS_BIN}/fgb ${OUTPUT_MODE} ${ANALYSIS_MODE}"

echo "=========== GKP: gatekeeper ====================="
echo "  $gkpStore is the gatekeeper store directory"
if 
$TIMER $GATEKEEPER_CMD -n $Sprefix ${NEW_INC_MODE} ${gkpStore} ${Bfile} 2> $Bprefix.log
then echo "gatekeeper successful" ; else echo "fatal error" ; exit 1 ; fi
Bprefix=`GateKeeperCurrName $Sprefix ${gkpStore}`
echo "* GateKeeperCurrName is $Bprefix."

echo ""
echo "=========== URT: screen for ubiquitous repeats =="
#echo "  $REPEAT_LIB is the screen item library"
echo "  $urcStore is the screener store directory"
echo $TIMER $URCSCREENER_CMD ${NEW_INC_MODE} ${urcStore} $Bprefix.inp 2>> $Bprefix.log

if 
#$TIMER $URCSCREENER_CMD ${REPEAT_LIB} $Bprefix.inp 2>> $Bprefix.log
$TIMER $URCSCREENER_CMD ${NEW_INC_MODE} ${urcStore} $Bprefix.inp 2>> $Bprefix.log
then echo "urc_screener successful" ; else echo "fatal error" ; exit 1 ; fi

echo ""
echo "=========== OVL: Create overlap records ========="
echo "  $frgStore is the fragment store directory"
if 
$TIMER $OVERLAP_CMD ${OLD_INC_MODE} ${frgStore} $Bprefix.urc 2>> $Bprefix.log
#$TIMER $OVERLAP_CMD ${NEW_INC_MODE} ${frgStore} $Bprefix.urc 2>> $Bprefix.log
then echo "overlaper successful" ; else echo "fatal error" ; exit 1 ; fi

# Count the number of overlaps in the batch
${AS_BIN}/countmessages < $Bprefix.ovl > $Bprefix.cnt
number_of_fragments=`gawk '/OFG/ { print $3}' $Bprefix.cnt`
number_of_overlaps=`gawk '/OVL/ { print $3}' $Bprefix.cnt`
echo "The number of fragments = ${number_of_fragments:=0}."
echo "The number of overlaps  = ${number_of_overlaps:=0}."

echo ""
echo "=========== FGB: fragment graph builder ============"
echo "  $fgbStore is the overlap store directory"
export FGB_LINE="$FGB_CMD -n ${number_of_fragments} -m ${number_of_overlaps} ${NEW_INC_MODE} ${fgbStore} $Bprefix.ovl" 
#
echo $FGB_LINE
if $FGB_LINE 2>> $Bprefix.log
then echo "fgb successful" ; else echo "fatal error" ; exit 1 ; fi
# -O $Bprefix.olp


