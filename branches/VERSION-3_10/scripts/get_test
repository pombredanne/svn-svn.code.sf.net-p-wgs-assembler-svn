#!/usr/local/bin/bash
#
###########################################################################
#
# This file is part of Celera Assembler, a software program that 
# assembles whole-genome shotgun reads into contigs and scaffolds.
# Copyright (C) 1999-2004, Applera Corporation. All rights reserved.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received (LICENSE.txt) a copy of the GNU General Public 
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
###########################################################################
#
#
# Usage: $ chmod a+x $AS_ROOT/scripts/get_DAILY
#        $ bsub -K < $AS_ROOT/scripts/get_DAILY
#
# Purpose: Sets up the work space and data space for the periodic test runs.
#          If successful, then submits a periodic test run.
#
# The source code that successfully completed a previous phase of the testing 
# is checked out of the CVS repository and tagged as "under test" for this
# phase of testing. In addition, the fragment files (.frg) are prepared.
#
############################################################################
#
# The following are the independent environmental variables.
# You may have to change these settings.
#
# Did you set your environmental variable LSB_HOSTS?  
# If not, use the next lines.
# 
RELEASE_DIRS1="cds/AS cds/SEP cds/SYS/UID cds/SYS/inc"
#
#CHECK_OUT2=AS_PRIV_CGB_001
#RELEASE_DIRS2="cds/AS/src/AS_CGB"
#
CHECK_IN1=AS_PRIV_ASSEMBLER_UNDER_${RUN_TAG}_TEST
#
# We should check if RUN_TAG is set to anything!!!!!!
#
#BSUB -P AssemblyTest      # the default project name
#BSUB -q express -m assembly2
#BSUB -C 0                  # the default core limit
# #BSUB -q overlapper -m cfs104a
# #BSUB -m cfs101a cfs102a cfs103a assembly2
# the default candidate hosts that have /work/assembly
# and /data/assembly2 mounted.
# #BSUB -R "select[mem>3000 && swap>5000]"
# #BSUB -R "select[mem>3000 && swap>5000] rusage[mem=3000:duration=30]"
# the default resource requirements
# #BSUB -n 4                 # run with 4 processors
# #BSUB -G assembly
# #BSUB -o /work/assembly/asteam/periodic_run/lsf_output
#           the default stdout and stderr
#
###############################################################
###############################################################
###############################################################
# These are the dependent environmental variables.  Please do not
# change these unless you study the entire process.
#
#
CVSROOT=/cm/cvs
export CVSROOT
export CVSREAD
export CVSUMASK=002
umask 002
#
#
echo "Test for disk access to the source and data directories."
#
if mkdir -p $AS_RELEASE_PATH ; then
  echo "Can use $AS_RELEASE_PATH on $HOSTNAME."
else 
mail $USER,asteam <<EOF 
Subject: $RUN_TAG job failed.
Could not use $AS_RELEASE_PATH on $HOSTNAME.
EOF
  echo "Could not use $AS_RELEASE_PATH on $HOSTNAME." ; exit 1
fi
#
echo "Esablished disk access to the source and data directories."
#
###############################################################
#
#if shlock -p $$ -f $AS_RELEASE_PATH/$RUN_TAG.lock ; then 
#  echo "This is a periodic run at " $THETIME 
#else
#  echo "A $RUN_TAG job is already running." ; exit 1
#fi
mkdir -p $AS_RELEASE_PATH
if [ -e $AS_RELEASE_PATH/$RUN_TAG.lock ] ; then 
mail $USER,asteam <<EOF 
Subject: Assembler $RUN_TAG job is already running.
An Assembler $RUN_TAG job is already running.
If this is a mistake, then execute:
"rm /work/assembly/asteam/periodic_run/WEEKLY.dir/WEEKLY.lock"
EOF
  echo "$AS_RELEASE_PATH/$RUN_TAG.lock already exists!"
  echo "A $RUN_TAG job might already be running." ; exit 1
else
  cat <<EOF > $AS_RELEASE_PATH/$RUN_TAG.lock
A lock file.
EOF
  echo "A $RUN_TAG job started at `date`."
fi
#
date
pwd
#
# Force a built to occur.
#
SEP_PATH=$AS_RELEASE_PATH/cds/SEP ; export SEP_PATH
AS_ROOT=$AS_RELEASE_PATH/cds/AS ; export AS_ROOT
#
echo `env`
#
#( cd $AS_RELEASE_PATH ; rm -rf $RELEASE_DIRS1 ; cvs co -r ${PRIOR_TAG} $RELEASE_DIRS )
#

if [ -n "${PRIOR_TAG}" ] ; then
 CHECK_OUT1=AS_PRIV_ASSEMBLER_RAN_${PRIOR_TAG}_TEST
else
 CHECK_OUT1=${DEFAULT_TAG}
fi 
echo CHECK_OUT1=$CHECK_OUT1
echo AS_RELEASE_PATH=$AS_RELEASE_PATH
#
( cd $AS_RELEASE_PATH ; 
  if [ -e cds ] ; then 
     cvs update -r ${CHECK_OUT1} ${RELEASE_DIRS1} ;
  else
     cvs co -r ${CHECK_OUT1} ${RELEASE_DIRS1} ;
  fi
)
#
#( cd $AS_RELEASE_PATH ; 
#  if [ -n "${CHECK_OUT2}" ] ; then
#    if [  -e cds ] ; then
#       cvs update -r ${CHECK_OUT2} ${RELEASE_DIRS2} ;
#    else
#       cvs co -r ${CHECK_OUT2} ${RELEASE_DIRS2} ;
#    fi ;
#  fi )
#
# In case the periodic job fails, this CVS tag makes it reproducible.
( cd $AS_RELEASE_PATH/cds ; cvs tag -F $CHECK_IN1 )
# Unfortunately, the CGW needs /usr/lib/libdxml.a to build.
# That library is not available on all of the LSF compute farm.
# So we spawn a job back to assembly2.
#bsub -K -q express -m assembly2 -J DailyMake <<EOF 
( cd $AS_RELEASE_PATH/cds/SYS/UID/src ; gmake all )
( cd $AS_RELEASE_PATH/cds/AS/src ; gmake all )
#EOF
###############################################
#
# Make the fragment file:
for GENOME in $GENOME_LIST ; do
  echo $GENOME $THETIME
  mkdir -p $AS_DATA_PATH/$GENOME
  cd $AS_DATA_PATH/$GENOME
  rm -f core
  ln -f $AS_RELEASE_PATH/cds/AS/testcases/AS_specfiles/$GENOME.sim .
  gmake -f $AS_ROOT/testcases/data_makefile $GENOME.frg
done
#
###############################################
rm -f $AS_RELEASE_PATH/$RUN_TAG.lock 
exit 0
