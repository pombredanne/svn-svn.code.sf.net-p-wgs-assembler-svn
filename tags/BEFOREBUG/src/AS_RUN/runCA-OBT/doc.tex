\documentclass[twoside,11pt]{article}
\usepackage{amsmath,amssymb}
\usepackage{moreverb}
\usepackage{fancyheadings}
\usepackage{ulem}
\usepackage{parskip}
\usepackage{calc,ifthen,epsfig}
\usepackage{longtable}
\sloppy

%  A few float parameters
%
\renewcommand{\dbltopfraction}{0.9}
\renewcommand{\dblfloatpagefraction}{0.9}
%\renewcommand{\textfraction}{0.05}

\begin{document}
\pagestyle{fancy}

\rhead[]{}
\chead[runCA-OBT]{runCA-OBT}
\lhead[\today]{\today}

\newcommand{\runCA}{{\sc runCA-OBT.pl}}

\normalem

\title{Celera Assembler Users Guide}
\author{Brian P. Walenz\thanks{bwalenz@jcvi.org}}

\maketitle

%\tableofcontents
%\listoffigures
%\listoftables

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
\label{chap:intro}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Downloading, Compiling and Installation}
\label{chap:build}

Download the assembler and optional components.  The optional
components require using the tip of development, otherwise, you can
download a release.

Make a place to install the assembler.  All further steps should start
from within this directory.

\begin{verbatim}
% mkdir wgs
% cd wgs
\end{verbatim}

Download the assembler.

\begin{verbatim}
% cvs -d:pserver:anonymous@wgs-assembler.cvs.sf.net:/cvsroot/wgs-assembler login
% cvs -d:pserver:anonymous@wgs-assembler.cvs.sf.net:/cvsroot/wgs-assembler co -P src
\end{verbatim}

Optional, download, configure, compile and install kmer.

\begin{verbatim}
% svn co https://kmer.svn.sourceforge.net/svnroot/kmer/trunk kmer
% cd kmer
% sh configure.sh
% gmake
% gmake install
% cd ..
\end{verbatim}

Optional, download, configure, compile and install the UMD Overlapper.  UMD Overlapper is developed at the University of Maryland. For detailed usage and run instructions, visit http://www.genome.umd.edu/overlapper.htm.

\begin{verbatim}
% mkdir -p UMDOverlapper/Linux-amd64
% cd UMDOverlapper/Linux-amd64
% curl -o completeUMDDist.tar.gz ftp://genomepc2.umd.edu/pub/completeUMDDist.tar.gz
% gzip -dc completeUMDDist.tar.gz | tar -xf -
% perl Install.perl
% cd ../..
\end{verbatim}

Optional, download, configure, compile and install Figaro.  Figaro is developed at the University of Maryland. For detailed usage and run instructions, visit http://amos.sourceforge.net/Figaro/Figaro.html.

\begin{verbatim}
% curl -o Figaro.tar.gz http://internap.dl.sourceforge.net/sourceforge/amos/Figaro-1.05.tar.gz
% gzip -dc Figaro.tar.gz | tar -xf -
% mkdir -p figaro
% mv Figaro-1.05 figaro/Linux-amd64
% cd figaro/Linux-amd64
% make install
% cd ../..
\end{verbatim}

Compile and install the Celera Assembler.  Binaries are installed into
an architecture specific directory, for example, Linux-amd64.  If kmer
or UMD Overlapper or figaro are compiled, this step will also copy those modules
to the architecture specific directory.

\begin{verbatim}
% cd src
% gmake
\end{verbatim}

Check that several key executables exist.  ``overmerry'' is built if
kmer is installed, ``runUMDOverlapper'' is built if the UMD overlapper
is installed, ``figaro'' is built if figaro is installed.  This directory 
can be moved to a system-wide location if desired.

\begin{verbatim}
% cd ..
% ls -l Linux-amd64/bin/gatekeeper
% ls -l Linux-amd64/bin/consensus
% ls -l Linux-amd64/bin/cgw
% ls -l Linux-amd64/bin/overmerry
% ls -l Linux-amd64/bin/runUMDOverlapper
% ls -l Linux-amd64/bin/figaro
\end{verbatim}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Running}

\runCA\ has three options:

\begin{longtable}{lp{3.0in}}
-d {\it directory} &
Place the assembly in {\it directory}, if {\it directory} doesn't exist,
create it.  This is a required option.
\\
-p {\it prefix} &
Call the assembly {\it prefix}, for example, 'prefix.asm'.  If not
specified, the prefix {\tt asm} is used.
\\
-s {\it specfile} &
Read options from the specifications file {\it specfile}.  These
options may also be supplied on the command line, as
'optionKey=optionValue', for example, 'useGrid=1'.  See the example
below for the appropriate way to quote spaces!
\end{longtable}

Any remaining command line parameters are either fragment files or
options, depending on if the parameter refers to a file or not.  For
example,

\begin{verbatim}
perl $ASMBIN/runCA-OBT.pl \
  -d /assembly/bigfoot-v1 \
  -p bigfoot \
  useGrid=1 \
  ovlMemory="2GB --hashload 0.8 --hashstrings 110000" \
  ovlHashBlockSize=600000 \
  ovlRefBlockSize=7630000 \
  frgCorrBatchSize=1000000 \
  frgCorrThreads=4 \
  fragments1.frg \
  fragments2.frg.gz \
  fragments3.frg.bz2 \
  fragments4.frg
\end{verbatim}

would assemble the {\tt bigfoot} genome using four fragment files, two
of which are compressed.  Instead of a {\it specfile} we specify our
few options on the command line.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Specification File}

The specification file contains algorithmic and computational options
for an assembler run.  For example, how many threads to use when
overlapping, if overlap-based trimming should be used, how many rounds
of extendClearRanges, etc.

White-space is allowed in option values.  A line of:
\begin{verbatim}
ovlMemory    =    1GB --hashload 0.8
\end{verbatim}
will set the {\tt ovlMemory} option to the value {\tt 1GB --hashload
0.8}.  Whitespace is trimmed from both ends.

\subsection{Suggested Configurations}

The following is some hints for running assemblies of various sizes.
Emphasis here is on making the assembly run with acceptable
computational performance.  It is left as an exercise to the reader to
decide how to get acceptable algorithmic performance (i.e., a decent
assembly).

\subsubsection{Microbes}

No special settings are needed.

\subsubsection{Hybrid Assembly Microbes}

An assembly using both long (ABI 3730) and short (454 FLX) reads is
termed a ``hybrid'' assembly.  Starting with version 4.2, Celera
Assembler supports mixing long and short reads.  Use the SFF file as
generated by the 454 instrument.  Use of the BOG unitigger and the mer
overlapper are highly recommended.

\begin{verbatim}
perl $ASMBIN/runCA-OBT.pl \
  -d /assembly/bigfoot-hybrid \
  -p bigfoot \
  unitigger=bog \
  overlapper=mer \
  abi3730.frg \
  flx.sff
\end{verbatim}

\subsubsection{Flies}

No special setting are needed, however, using the grid ({tt
useGrid=1}) is suggested.  See {\em Mammals} below.

Our default specFile for a test-assembly of a fly uses SGE and
increases the memory allowed.  It is presented as yet another example:

\begin{verbatim}
useGrid      = 1
scriptOnGrid = 1

fakeUIDs = 1

merylMemory  = 4000

sge          = -A assembly
sgeScript    = -pe thread 1
sgeOverlap   = -pe thread 2 -p -999
sgeConsensus = -p -999

ovlMemory        = 4GB --hashload 0.8 --hashstrings 100000
ovlThreads       = 4
ovlHashBlockSize = 180000
ovlRefBlockSize  = 4000000

frgCorrBatchSize = 600000
frgCorrThreads   = 4
\end{verbatim}

\subsubsection{Mammals}

Use of the grid is essential for both overlapper and consensus.

Overlapper very quickly overwhelms the NFS server, so we increase both
the hash size (the number of fragments ``in core'') and the reference
block size (the number of fragments ``per job'').  To accomodate this,
we need to use a carefully constructed {\tt ovlMemory} string that
overrides some reasonable defaults.

Fragment correction also benefits greatly by increasing the batch
size.  See the {\tt frgCorrBatchSize} below for details.  At this
size, we might as well use all processors available.

\begin{verbatim}
useGrid=1
ovlMemory=2GB --hashload 0.8 --hashstrings 110000
ovlHashBlockSize=600000
ovlRefBlockSize=7630000
frgCorrBatchSize=1000000
frgCorrThreads=4
\end{verbatim}

If specified on the command line, be sure to quote the spaces in {\tt
ovlMemory}, either include the whole thing in quotes or use backslashes.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Outputs}

The {\tt 9-terminator} directory inside the assembly directory
contains the outputs of the assembler.  The assembly itself is in the
file {\tt \$prefix.asm}.

\subsection{QC metrics}

The primary quality report on the assembly is {\tt \$prefix.qc}.  It
contains statistics on lengths of contigs and scaffolds, counts of
fragment status -- placed in a contig, not used, etc -- and counts of
mate status.

The Overlap Based Trimming module generates two files of interest
when quality checking an assembly.  A summary of chimera and spur
detection/removal/fixing is in {\tt 0-overlaptrim/\$prefix.chimera.summary} with the gory
details (including UID's) is in {\tt 0-overlaptrim/\$prefix.chimera.report}.  The
before/after trimming results are in {\tt 0-overlaptrim/\$prefix.mergeLog}; the columns are
uid, iid, original clear, new clear, and a free-form text annotation
of if the fragment was deleted and why.

\subsection{Assembled Sequences}

Five sequence types are output: singleton reads, unitigs, contigs,
scaffolds and degenerates.  A degenerate is a unitig that was never
placed in a scaffold; however, it has both a contig UID and a
(degenerate) scaffold UID.

All but singleton reads also have quality values in a separate file.

\begin{center}
\begin{tabular}{|c|c|}
\hline
\hline
singleton reads      & {\tt \$prefix.singleton.fasta} \\
\hline
unitigs              & {\tt \$prefix.utgcns.fasta} \\
                     & {\tt \$prefix.utgqlt.fasta} \\
\hline
degenerates          & {\tt \$prefix.degcns.fasta} \\
                     & {\tt \$prefix.degqlt.fasta} \\
\hline
contigs              & {\tt \$prefix.ctgcns.fasta} \\
                     & {\tt \$prefix.ctgqlt.fasta} \\
\hline
scaffolds            & {\tt \$prefix.scfcns.fasta} \\
                     & {\tt \$prefix.scfqlt.fasta} \\
\hline
\end{tabular}
\end{center}

\subsection{Position Mappings}

Although included in the assembly file itself, the so-called
``posmap'' files describe the location of some smaller object in a
larger object, say, the location of a fragment in a contig.

Lines are generally:

smallUID bigUID beg end orientation

saying the ``smallUID'' thing is in the ``bigUID'' thing from position
``beg'' to ``end''.  Coordinates are in the ungapped sequence, the
same sequence as is in the assembled sequences outputs.

\begin{center}
\begin{tabular}{|c|c|}
\hline
\hline
contig length   & \$prefix.posmap.ctglen \\
scaffold length & \$prefix.posmap.scflen \\
\hline
fragments in contigs   & \$prefix.posmap.frgctg \\
fragments in scaffolds & \$prefix.posmap.frgscf \\
\hline
unitigs in contigs     & \$prefix.posmap.utgctg \\
unitigs in scaffolds   & \$prefix.posmap.utgscf \\
\hline
contigs in scaffolds   & \$prefix.posmap.ctgscf \\
\hline
variation records in contigs   & \$prefix.posmap.varctg \\
variation records in scaffolds & \$prefix.posmap.varscf \\
\hline
fragments in surrogates  & \$prefix.posmap.frgsurr \\
surrogates in scaffolds  & \$prefix.posmap.surrscf \\
\hline
\end{tabular}
\end{center}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Options}

\subsection{Error Rates}
\label{sec:erates}

There are four configurable error rates, described below.

$utg \le ovl \le cns \le cgw$.  Usually, $ovl = cns$.

Error rates set via environment variables (AS\_OVL\_ERROR\_RATE,
AS\_CGW\_ERROR\_RATE and AS\_CNS\_ERROR\_RATE) will be used, unless they are
changed via the spec file or command line options.

Note that the unitigger error rate cannot be set via the environment.

\begin{longtable}{lp{3.0in}}

ovlErrorRate={\it float} &
Error limit on overlaps, for both Overlap Based Trimming and normal
overlaps.  The overlapper modules will not report overlaps over this
limit.
Expressed as a fraction error, $0.0 \le error \le 0.25$.
The default is 0.06 (6\% error).
\\

utgErrorRate={\it integer} &
The error rate above which unitigger discards overlaps.
Expressed as errors per thousand bases.
The default is 15 (1.5\% error).
\\

cnsErrorRate={\it float} &
Error rate for consensus.  Consensus will expect to find alignments
below this level, but it doesn't strictly enforce it.
Expressed as a fraction error, $0.0 \le error \le 0.25$.
The default is 0.06 (6\% error).
\\

cgwErrorRate={\it float} &
Error rate for scaffolder.  Scaffolder will try to merge unitigs and
contigs up to this error rate.
Expressed as a fraction error, $0.0 \le error \le 0.25$.
The default is 0.10 (10\% error).
\\

\end{longtable}


\subsection{Stopping Options}
\label{sec:stopopts}

\runCA\ can stop after various modules have computed.  There is no
corresponding {\it startBefore} option because \runCA\ requires a very
specific directory layout that is both difficult to describe and
difficult to recreate manually.  It {\em is} however possible to get
much the same effect using the {\it do*} options.

The default, obviously, is to not stop early.

\begin{longtable}{lp{3.0in}}

stopAfter=initialStoreBuilding &
Stop after the fragment and gatekeeper stores are created.
\\

stopAfter=overlapBasedTrimming &
Stop after the fragment and gatekeeper stores are created, and the
Overlap Based Trimming algorithm has updated the clear ranges.
{\tt OBT} is an alias for this.
\\

stopAfter=overlapper &
Stop after the overlapper finishes, and the overlap store is created.
\\

stopAfter=unitigger &
Stop after the unitigger finishes.
\\

stopAfter=consensusAfterUnitigger &
Stop after the consensus after unitigger finishes.
\\

stopAfter=scaffolder &
Stop after all stages of scaffolding are finished.
\\

stopAfter=consensusAfterScaffolder &
Stop after the consensus after scaffolding finishes.
\\

\end{longtable}


\subsection{General Configuration Options}

\begin{longtable}{lp{3.0in}}

doBackupFragStore={\it integer} &
If zero, do not backup the fragment store before steps that modify it.
You probably don't want to disable this.  The default is to backup the
fragment store.
\\

fakeUIDs={\it integer} &
If zero, use real UID's from the UID server.  Otherwise, use UID's
starting from this value.  The default is to use real UID's.
\\

uidServer={\it string} &
Pass this string to modules that access the UID server (currently,
{\tt AS\_TER/terminator} and {\tt AS\_OBT/dumpDistanceEstimates}).
This is empty by default.
\\

pathMap={\it string} &
A file containing a mapping of hostname to directory.  The directory
should contain the Celera Assembler binaries.  In most cases, runCA can
determine the correct binaries to use, and this option is not needed.

This option is useful in heterogeneous environments.  For example, Bri
was comparing FreeBSD 7.0 to FreeBSD 6.3.  Both builds wanted to call
the binary directory ``FreeBSD-amd64'', and neither host could run the
other hosts binaries.  A pathMap was created to tell where the
binaries are for that specific machine.

\begin{verbatim}
node5.home /home/work/wgs/7.0/FreeBSD-amd64/bin
node6.home /home/work/wgs/7.0/FreeBSD-i386/bin
node7.home /home/work/wgs/6.3/FreeBSD-amd64/bin
node8.home /home/work/wgs/6.3/FreeBSD-i386/bin
\end{verbatim}

Be sure to use the hostname as returned by ``uname -n''.
\\

\end{longtable}


\subsection{Sun Grid Engine Options}

\runCA has extremely flexible (read: complicated) SGE support.  By
default, SGE is not used, and all components of the assembler are run
on the machine you start \runCA on.

Enabling {\tt useGrid=1} will let {\tt overlapper} and {\tt consensus}
use the grid, however, these must be manually submitted (a submission
command is provided, so it's not difficult).  After each stage
finished, you must restart \runCA to proceed with the assembly.  This
method allows the large memory components of the assembler to run on a
machine with no grid access, but still manually use a grid for the
computationally expensive pieces.  For a long time, JCVI had a large
memory Alpha not on the grid, and we ran \runCA there.  The filesystem
was shared between the Alpha and the grid, and we used this mode for
large assemblies.  It's a pain, though, so get your sysadmin to put
the big machine on the grid.

Enabling {\tt scriptOnGrid=1} will launch \runCA on the grid, and
\runCA will take care of submitting all parallel components, and
restarting \runCA after each finishes.

\begin{longtable}{lp{3.0in}}

useGrid={\it integer} &
If zero, no module will use the grid.  If non-zero, the grid will be
used for modules that support it, and that are enabled.  Each module
may independently decide to not use the grid.  The default is to not
use the grid.
\\

scriptOnGrid={\it integer} &
If zero, run only the parallel components on the grid.  If one, submit
the controlling script (aka \runCA) to the grid.  This is disabled by
default.
\\

ovlOnGrid={\it integer} &
If zero, do not use the grid for overlapping.  This is enabled by
default, however, {\em useGrid} is not enabled by default, thus,
overlapping, by default is not done on the grid.
\\

frgCorrOnGrid={\it integer} &
If zero, do not use the grid.  Fragment error correction makes heavy
use of the fragment store.  Unless your grid has fast
access to this store, use of the grid is strongly discouraged.
\\

ovlCorrOnGrid={\it integer} &
If zero, do not use the grid.  XXXXX.  Unless your grid has fast
access to this store, use of the grid is strongly discouraged.
\\

cnsOnGrid={\it integer} &
If zero, do not use the grid for consensus.  This is enabled by
default, however, {\em useGrid} is not enabled by default, thus,
consensus, by default is not done on the grid.
\\

maxGridJobSize={\it integer} &
Submit no more than this many jobs concurrently.
\\

sge={\it string} &
{\it string} is passed to the {\tt qsub} command used to submit ANY job to the grid.
\\

sgeScript={\it string} &
{\it string} is passed to the {\tt qsub} command used to submit \runCA to the grid.
\\

sgeOverlap={\it string} &
{\it string} is passed to the {\tt qsub} command used to submit overlap jobs to the grid.
\\

sgeConsensus={\it string} &
{\it string} is passed to the {\tt qsub} command used to submit consnsus jobs to the grid.
\\

sgeFragmentCorrection={\it string} &
{\it string} is passed to the {\tt qsub} command used to submit fragment correction jobs to the grid.
\\

sgeOverlapCorrection={\it string} &
{\it string} is passed to the {\tt qsub} command used to submit overlap correction jobs to the grid.
\\

\end{longtable}


\subsection{Gatekeeper Options}
\label{sec:gkpopts}

\begin{longtable}{lp{3.0in}}
gkpFixInsertSizes={\it integer} &
If true (1), {\tt gatekeeper} will fix insert size estimates that have
a too large or too small standard deviation.  Acceptable insert sizes
estimates are $0.1*mean < std.dev. < \frac{1}{3}*mean$.  If the
standard deviation is outside this range, it is reset to $0.1 * mean$.
The default is to fix estimates.
See also {\tt computeInsertSize} in Section~\ref{sec:scafopts}.
\\
\end{longtable}


\subsection{Vector Trimming Options}
\begin{longtable}{lp{3.0in}}
vectorIntersect={\it path} &
The path to a file containing a list of the vector clear range for
each read.  Format {\it uid vector-left vector-right}, one UID per
line.  Coordiates are base-based.
\\
vectorTrimmer={\em ca} or {\em umd} or {\em figaro} &
Select which vector trimmer to use. The default is {\em ca} (see ~\ref{sec:obtopts}). 
If {\em vectorIntersect} is provided this parameter is not used. Note that setting both 
vectorTrimmmer={\em umd} and overlapper={\em umd} is redundant as the UMD overlapper 
runs vector trimming by default.
\\
\end{longtable}

\subsubsection{Figaro Vector Trimmer Options}

\begin{longtable}{lp{3.0in}}
figaroFlags={\it string} &
Flags supplied to the figaro vector trimmer.  Default: ``-T 30 -M 100 -E 500 -V f''.
\\
\end{longtable}


\subsection{Overlap Based Trimming Options}
\label{sec:obtopts}

Overlap Based Trimming invokes the overlap module, see
Section~\ref{sec:overlapopts} for options to configure the overlapper.
It is not possible to configure the overlapper differently for overlap
based trimming and normal overlaps.

Overlap based trimming writes several log files:

\begin{enumerate}
\item {\it asm}.initialTrimLog -- one line per read.  Immutable reads do not
get modified, and do now appear in the log.  Whitespace separated list
of uid,iid pair, original clear begin, end, quality trim begin and
end, vector clear begin and end, final clear begin, end.

\item {\it asm}.mergeLog -- one line per read.  Whitespace separated list of
IID, final left and right trimming.  Trimming due to chimera and spur
detection are not included here.  All reads are reported.

\item {\it asm}.chimera.report -- many lines per read.  It shows the type of
problem fixed, the resulting clear range, and any evidence for the
change.
\end{enumerate}

\begin{longtable}{lp{3.0in}}
doOverlapTrimming={\it integer} &
If non-zero, do overlap-based trimming.  The default is to do overlap
based trimming.
\\
\end{longtable}


\subsection{Overlapper Options}
\label{sec:overlapopts}

Overlapper performs an all-fragments against all-fragments alignment.
Each pair of fragments is aligned to decide if they overlap.  In
effect, it is populating an array with alignment information.
Overlapper is able to segment the computation on both axes of the
array.  The fragments along one axis are used to construct a
hash-table to seed the alignments.  The fragments along the other axis
then query the hash-table one at a time.

For small assemblies, one can simply divide the number of fragments by
the amount of parallelization one wishes to get and use that.  To get
16 jobs, divide your number of fragments by 4.

For large assemblies, the author advocates using a large {\it
ovlRefBlockSize}, and using {\it ovlHashBlockSize} to control the number of
jobs.  See {\it ovlMemory}.

Section~\ref{sec:erates} discusses how to change the allowed error rate with
the {\tt ovlErrorRate} option.

Three options exist to select the style of overlapper to use, and one to
control how much memory is used to build a store of overlaps.

\begin{longtable}{lp{3.0in}}
overlapper={\em ovl} or {\em mer} or {\em umd} &
Select which overlapper to use.
If {\em umd} is used, OBT is disabled.
Default is {\em ovl}.
\\

obtOverlapper={\em ovl} or {\em mer} &
Select which overlapper to use for computing OBT overlaps.
\\

ovlOverlapper={\em ovl} or {\em mer} or {\em umd} &
Select which overlapper to use for computing normal overlaps.
\\

ovlStoreMemory={\it integer} &
The amount of memory, in megabytes, to use for building overlap
stores.  The default is 1024MB memory.
\\
\end{longtable}

\subsubsection{Classic Overlapper Options}

The classic overlapper is tried and true.  It operates much like a
classic seed-and-extend algorithm, but is highly tuned to operate on
short sequences, and to look for overlaps that would promote assembly,
instead of sequence homology.

\begin{longtable}{lp{3.0in}}

ovlThreads={\it integer} &
The number of compute threads to use.  Usually the number of CPUs your
host has.  Even if your grid schedules N jobs per N CPU host, there is
an advantage to telling each job to use N threads -- when one jobs
does I/O, the other jobs will use the now mostly idle CPU.  The default is 2 threads.
\\

ovlStart={\it integer} &
The fragment IID at which to begin overlaps.  Only useful if you have
added fragments to an existing store and wish to compute the new
overlaps.  You should probably also use {\it stopAfter=overlapper}.
\\

ovlHashBlockSize={\it integer} &
The number of fragments to use for hash table construction.
Overlapper will, internally, fragment this range so as to not exceed
its memory limit.  The default is 200,000 fragments.
\\

ovlRefBlockSize={\it integer} &
The number of fragments to use for generating alignments.  The default
is 2,000,000 fragments.
\\

ovlMemory={\it integer} &
One of a set of predefined memory sizes, optionally followed by any
detailed overlap memory switched (not documented here).  The memory sizes are:
{\it 256MB}, {\it 1GB}, {\it 2GB}, {\it 4GB}, {\it 8GB} and {\it 16GB}.
The default is {\it 2GB}.
\\

ovlMerSize={\it integer} &
Sets the ovl overlapper, mer overlapper and meryl k-mer size used when
computing normal overlaps.  Default: 22.
\\

ovlMerThreshold={\tt integer} &
Mers with count larger than this value will not be used to seed
normal overlaps.  Only for {\em ovl} overlapper.  The special value {\it 0}
disables mer counting for OVL.  Default: 500.
\\

obtMerSize={\it integer} &
Sets the ovl overlapper, mer overlapper and meryl k-mer size used when
computing overlaps for Overlap Based Trimming.  Default: 22.
\\

obtMerThreshold={\tt integer} &
Mers with count larger than this value will not be used to seed
overlaps for Overlap Based Trimming.  Only for {\em ovl} overlapper.  The special value {\it 0}
disables mer counting for OBT.  Default: 1000.
\\
\end{longtable}


\subsubsection{Mer Overlapper Options}

Like the classic overlapper, the mer overlapper also uses a
seed-and-extend methodology.  However, all seeds are found first,
allowing a second pass to examine all overlaps for a given fragment.
The second pass computes the first half of Fragment Error Correction
(Section~\ref{sec:frgcorr}).

The mer overlapper also uses Classic Overlapper options {\tt
obtMerSize} and {\tt ovlMerSize}.

\begin{longtable}{lp{3.0in}}
merCompression={\it integer} &
If the mer overlapper is used, compress homopolymer runs to this many
letters.  This value applies to the meryl mer counts too.  For
example, {\tt ACTTTAAC} with merCompression=1 would be {\tt ACTAC}.
The default is 1.
\\

merOverlapperThreads={\it integer} &
The number of compute threads to use.  Usually the number of CPUs your
host has.
The default is 2 threads.
\\

merOverlapperSeedBatchSize={\it integer} &
The number of fragments used per batch of seed finding.  The amount
of memory used is directly proportional to the number of fragments.
(sorry, no documentation on what that relationship is, yet).
The default is 100,000 fragments.
\\

merOverlapperExtendBatchSize={\it integer} &
The number of fragments used per batch of seed extension.  The amount
of memory used is directly proportional to the number of fragments.
See option {\tt frgCorrBatchSize} in Section~\ref{sec:frgcorr} for
hits, but use those numbers with caution.
The default is 75,000 fragments.
\\

merOverlapperSeedConcurrency={\it integer} &
If not on the grid, run this many seed finding processes on the local machine at the same time.  Default: 1.
\\

merOverlapperExtendConcurrency={\it integer} &
If not on the grid, run this many seed extension processes on the local machine at the same time.  Default: 1.
\\

\end{longtable}

The UMD overlapper combines fragment trimming with overlapping.

\subsubsection{UMD Overlapper Options}

\begin{longtable}{lp{3.0in}}
umdOverlapperFlags={\it string} &
Flags supplied to the UMD overlapper.  Default: ``-use-uncleaned-reads -trim-error-rate 0.03 -max-minimizer-cutoff 150''.
\\
\end{longtable}

\subsection{Meryl Options}
\label{sec:merylopts}

A far superior {\em meryl} mer counter is available if CA is compiled
with {\em kmer} support.

\begin{longtable}{lp{3.0in}}
merylMemory={\it integer} &
Amount of memory that meryl is allowed to use.  Only if {\em kmer} is
used.  Default: 800MB.
\\
merylThreads={\it integer} &
Number of threads that meryl is allowed to use.  Only if {\em kmer} is
used.  Default: 1 thread.
\\
\end{longtable}



\subsection{Fragment Error Correction Options}
\label{sec:frgcorr}

\begin{longtable}{lp{3.0in}}
frgCorrBatchSize={\it integer} &
The number of reads to load into core at once.  Fragment error
correction will then scan the entire fragment store, recomputing
overlaps.  As a (very) rough guide:

\begin{center}
\begin{tabular}{|c|c|}
\hline
frgCorrBatchSize & Memory (MB) \\
\hline
\hline
   10,000 &     132 \\
   50,000 &     650 \\
  100,000 &    1300 \\
  200,000 &    2500 \\
  500,000 &    6300 \\
1,000,000 &   13000 \\
2,000,000 &   23000 \\
2,500,000 &   32000 \\
3,000,000 &   32000 \\
\hline
\end{tabular}
\end{center}

The larger batch sizes assume {\tt correct-frags} is NOT using
a temporary internal store.  This is probably not enabled by default.
\\

doFragmentCorrection={\it integer} &
If non-zero, do fragment error correction (and, implicitly, overlap
error correction).  The default is to do fragment error correction.
\\

frgCorrThreads={\it integer} &
The number of threads to use for fragment error correction.
\\

frgCorrConcurrency={\it integer} &
If the grid is NOT enabled, run this many fragment correction jobs at the same
time.  Default is 1.
\\

ovlCorrBatchSize={\it integer} &
{\it documentation needed!}  1,000,000 uses about 2.5GB memory.  400,000 uses 750MB.
Default: 200,000.
\\

ovlCorrConcurrency={\it integer} &
If the grid is NOT enabled, run this many overlap correction jobs at the same
time.  Default is 4.
\\
\end{longtable}




\subsection{Unitigger Options}

The Best Overlap Graph (BOG) is an implementaiton of a simpler
algorithm for building unitigs.  See Section~\ref{sec:erates} for
a discussion about error rates.

\begin{longtable}{lp{3.0in}}
unitigger={\em utg} or {\em bog} &
Use the original unitigger, or the best overlap graph unitigger.
Default: utg.
\\

utgGenomeSize={\it integer} &
The genome size, in bases, to force unitigger to use.  By default,
unitigger will supply a reasonable estimate, and this option is not
usually specified.
\\

utgEdges={\it integer} &
The estimated number of edges unitigger will encounter.  If you find
that unitigger is exhausting its process size, setting this to
slightly more than the actual number of edges might help.  Otherwise,
do not use.
\\

utgFragments={\it integer} &
The estimated number of fragments unitigger will encounter.  If you
find that unitigger is exhausting its process size, setting this to
slightly more than the actual number of fragments might help.
Otherwise, do not use.
\\

utgBubblePopping={\it integer} &
If zero, do not pop bubbles in unitigger.  If one, pop bubbles.  Default is to pop bubbles.
\\

utgRecalibrateGAR={\it integer} &
If one, recalibrate the global fragment arrival rate based on large unitigs.  Default is to recalibrate.
\\

bogPromiscuous={\it integer} &
Default 0.
\\

bogEjectUnhappyContain={\it integer} &
Default 0.
\\

bogBadMateDepth={\it integer} &
Split unitigs with more than this number of overlapping bad mates.  Default 7.
\\

\end{longtable}


\subsection{Scaffolder Options}
\label{sec:scafopts}

\begin{longtable}{lp{3.0in}}

cgwOutputIntermediate={\it integer} &
if non-zero, intermediate CGW runs will output the .cgw,
.cgw\_scaffolds and .cgw\_contigs files.  The default is to NOT output
these files.
\\

cgwPurgeCheckpoints={\it integer} &
If non-zero (the default) remove all but the final CGW checkpoint file
after cgw finishes successfully.
\\

cgwDemoteRBP={\it integer} &
If one, demote some unitigs to repeat status based on patterns in
overlaps.  Default is one.
\\

astatLowBound={\it integer} &
Default: 1.
\\

astatHighBound={\it integer} &
Default: 5.
\\

stoneLevel={\it integer} &
The aggressiveness of stone throwing in the last iteration of cgw.  Default: 2.
\\

computeInsertSize={\it integer} &
If non-zero, compute a scratch scaffolding to better estimate insert
sizes before scaffolding starts.  The estimates are updated in the
gatekeeper store, which replaces the original values.  This is
reasonably expensive for large assemblies, but does generally if
slightly improve the result.  Note that insert size estimates are also
re-estimated while scaffolding, but not updated in the gatekeeper
store.  The default is to compute the initial update for assemblies
with fewer than 1,000,000 fragments.
\\

cgwDistanceSampleSize={\it integer} &
Do not update insert size estimates unless there are this many mates to use as evidence.
Default: 100.
\\

doResolveSurrogates={\it integer} &
If non-zero, resolve surrogates.
\\

doExtendClearRanges={\it integer} &
If non-zero, do that many rounds of {\it extendClearRanges}.  Stones are disabled
until after all rounds.
\\

extendClearRangesStepSize={\it integer} &
Run extendClearRanges in batches of this many scaffolds.  The default
is to use the larger of 5000 or one eigthth the number of scaffolds,
whichever is larger.  Note that a cgw checkpoint and a gatekeeper store
backup are saved for each batch; it is VERY easy to run out of disk space
on a large assembly if the step size is too small.
\\

\end{longtable}


\subsection{Consensus Options}

These options apply to both post-unitigger and post-scaffolder consensus.

\begin{longtable}{lp{3.0in}}
cnsPartitions={\it integer} &
The approximate number of partitions unitigger and scaffolder will
generate for consensus.  There will be no more than this, but likely
will be fewer.  The default is 128 partitions, or partitions
consisting of about {\it cnsMinFrags} fragments, whichever results in fewer
partitions.
\\

cnsMinFrags={\it integer} &
The minimum number of fragments in a consensus partition.  Default
is 75,000.
\\

cnsConcurrency={\it integer} &
If the grid is not enabled, run this many consensus jobs at the same
time.  Default is 2.
\\
\end{longtable}


\subsection{Terminator Options}
\begin{longtable}{lp{3.0in}}
createAGP={\it integer} &
If non-zero, create an AGP file for the scaffolds.  Default is 0.
\\

createACE={\it integer} &
If non-zero, create an ACE file for the scaffolds.  Default is 0.
\\

createPosMap={\it integer} &
If non-zero, create the ``posmap'' files that map fragments, contigs,
variation records, etc, with contig and scaffold coordinates.  Default
is 1.
\\

merQC={\it integer} &
If one, compute a mer based QC report.  The default is to NOT compute the report.
\\

merQCmemory={\it integer} &
Use xMB of memory, at most, when computing the merQC.  Default is 1024MB.
\\

merQCmerSize={\it integer} &
Use size k mers for the merQC.  Default is 22.
\\

cleanup={\it string} &
Remove temporary/intermediate files after the assembly finishes.
\\

\end{longtable}

\end{document}
