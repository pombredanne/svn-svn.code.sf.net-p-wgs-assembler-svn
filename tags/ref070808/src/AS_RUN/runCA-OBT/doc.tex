\documentclass[twoside,11pt]{article}
\usepackage{amsmath,amssymb}
\usepackage{moreverb}
\usepackage{fancyheadings}
\usepackage{ulem}
\usepackage{parskip}
\usepackage{calc,ifthen,epsfig}
\usepackage{longtable}
\sloppy


%\begin{tabularx}{\linewidth}{lX}


%  A few float parameters
%
\renewcommand{\dbltopfraction}{0.9}
\renewcommand{\dblfloatpagefraction}{0.9}
%\renewcommand{\textfraction}{0.05}

\begin{document}
\pagestyle{fancy}

\rhead[]{}
\chead[runCA-OBT]{runCA-OBT}
\lhead[\today]{\today}

\newcommand{\runCA}{{\sc runCA-OBT.pl}}

\normalem

\title{runCA-OBT\\
{\small or, how to run the Celera Assembler with Overlap Based Trimming in 54 easy steps}}
\author{Brian P. Walenz\thanks{bwalenz@venterinstitute.org}}

\maketitle

%\tableofcontents
%\listoffigures
%\listoftables

%\section{Introduction}
%\label{chap:intro}

\section{Running}

\runCA\ has three options:

\begin{longtable}{lp{3.0in}}
-d {\it directory} &
Place the assembly in {\it directory}, if {\it directory} doesn't exist,
create it.  This is a required option.
\\
-p {\it prefix} &
Call the assembly {\it prefix}, for example, 'prefix.asm'.  If not
specified, the prefix {\tt asm} is used.
\\
-s {\it specfile} &
Read options from the specifications file {\it specfile}.  These
options may also be supplied on the command line, as
'optionKey=optionValue', for example, 'useGrid=1'.  See the example
below for the appropriate way to quote spaces!
\end{longtable}

Any remaining command line parameters are either fragment files or
options, depending on if the parameter refers to a file or not.  For example,

\begin{verbatim}
perl $ASMBIN/runCA-OBT.pl \
  -d /home/work/assembly/bigfoot-v1 \
  -p bigfoot \
  useGrid=1 \
  ovlMemory=''2GB --hashload 0.8 --hashstrings 110000'' \
  ovlHashBlockSize=600000 \
  ovlRefBlockSize=7630000 \
  frgCorrBatchSize=1000000 \
  frgCorrThreads=4 \
  fragments1.frg \
  fragments2.frg.gz \
  fragments3.frg.bz2 \
  fragments4.frg
\end{verbatim}

would assemble the {\tt bigfoot} genome using four fragment files, two
of which are compressed.  Instead of a {\it specfile} we specify our
few options on the command line.

\section{Specification File}

The specification file contains algorithmic and computational options
for an assembler run.  For example, how many threads to use when
overlapping, if overlap-based trimming should be used, how many rounds
of extendClearRanges, etc.

White-space is allowed in option values.  A line of:
\begin{verbatim}
ovlMemory    =    1GB --hashload 0.8
\end{verbatim}
will set the {\tt ovlMemory} option to the value {\tt 1GB --hashload
0.8}, in particular, trimming whitespace from the ends.

\subsection{Suggested Configurations}

The following is some hints for running assemblies of various sizes.
Emphasis here is on making the assembly run with acceptable
computational performance.  It is left as an exercise to the reader to
decide how to get acceptable algorithmic performance (i.e., a decent
assembly).

\subsubsection{Microbes}

No special settings are needed.

\subsubsection{Flies}

No special setting are needed, however, using the grid ({tt
useGrid=1}) is suggested.  If possible, use local disk for the
fragment store.  See {\em Mammals} below.

\subsubsection{Mammals}

Use of the grid is essential for both overlapper and consensus.

Overlapper very quickly overwhelms the NFS server, so we increase both
the hash size (the number of fragments ``in core'') and the reference
block size (the number of fragments ``per job'').  To accomodate this,
we need to use a carefully constructed {\tt ovlMemory} string that
overrides some reasonable defaults.

Fragment correction also benefits greatly by increasing the batch
size.  See the {\tt frgCorrBatchSize} below for details.  At this
size, we might as well use all processors available.

Our settings are then:

\begin{verbatim}
useGrid=1
ovlMemory=2GB --hashload 0.8 --hashstrings 110000
ovlHashBlockSize=600000
ovlRefBlockSize=7630000
frgCorrBatchSize=1000000
frgCorrThreads=4
\end{verbatim}

If specified on the command line, be sure to quote the spaces in {\tt
ovlMemory}, either include the whole thing in quotes or use backslashes.


\section{QC output}

For lack of a better place to put this....

OBT produces two files of interest for QC checks, in the {\tt
0-overlaptrim} directory.  A summary of chimera and spurs
detection/removal/fixing is in {\tt *.chimera.summary} and the gory
details (including UID's) is in {\tt *.chimera.report}.  The
before/after trimming results are in {\tt *.mergeLog}; the columns are
uid, iid, original clear, new clear, and a free-form text annotation
of if the fragment was deleted and why.



\section{Options}



\subsection{Stopping Options}
\label{sec:obtopts}

\runCA\ can stop after various modules have computed.  There is no
corresponding {\it startBefore} option because \runCA\ requires a very
specific directory layout that is both difficult to describe and
difficult to recreate manually.  {\em XXXX but it is possible to get
much the same effect using the {\it do*} options, and having some
Stores around.}


\begin{longtable}{lp{3.0in}}

stopAfter=initialStoreBuilding &
Stop after the fragment and gatekeeper stores are created.
\\

stopAfter=overlapBasedTrimming &
Stop after the fragment and gatekeeper stores are created, and the
Overlap Based Trimming algorithm has updated the clear ranges.
{\tt OBT} is an alias for this.
\\

stopAfter=overlapper &
Stop after the overlapper finishes, and the overlap store is created.
\\

stopAfter=unitigger &
Stop after the unitigger finishes.
\\

stopAfter=consensusAfterUnitigger &
Stop after the consensus after unitigger finishes.
\\

stopAfter=scaffolder &
Stop after all stages of scaffolding are finished.
\\

stopAfter=consensusAfterScaffolder &
Stop after the consensus after scaffolding finishes.
\\

\end{longtable}


\subsection{General Configuration Options}

\begin{longtable}{lp{3.0in}}
binRoot={\it path} &
The path to the wgs-assembler build directory.  This is the directory
where the Celera Assembler {\it src/} directory is.  If our {\it
gatekeeper} binary is located in {\it
/opt/software/wgs-assembler/FreeBSD/bin/gatekeeper}, then we would
specify {\it binRoot=/opt/software/wgs-assembler}.  If not specified,
this defaults to the location of \runCA.
\\

grid={\it string} &
The architecture of the machines in the grid. This should be the name
of the Celera Assembler directory containing the grid binaries, for
example, ``Linux-i686'', ``Darwin-ppc'', ``OSF1-alpha'' and
``FreeBSD0-amd64''.  If not specified, it defaults to the architecture
of the current host, or ``Linux-i686'' if that exists.
\\

useGrid={\it integer} &
If zero, no module will use the grid.  If non-zero, the grid will be
used for modules that support it, and that are enabled.  Each module
may independently decide to not use the grid.  The default is to not
use the grid.
\\

doBackupFragStore={\it integer} &
If zero, do not backup the fragment store before steps that modify it.
You probably don't want to disable this.  The default is to backup the
fragment store.
\\

fakeUIDs={\it integer} &
If zero, use real UID's from the UID server.  Otherwise, use UID's
starting from this value.  The default is to use real UID's.
\\

uidServer={\it string} &
Pass this string to modules that access the UID server (currently,
{\tt AS\_TER/terminator} and {\tt AS\_OBT/dumpDistanceEstimates}).
This is undefined by default.
\\

executionWrapper={\it string} &
If defined, this program is used to execute the components of the
assembler.  The program should behave similarily to {\it e.g.}, {\tt
/usr/bin/time}.  It should take no options, treat the first command
line argument as the executable to run, and pass all remaining
arguments to that executable..  This is undefined by default.

An unfortunate quirk:  Some commands are implemented as a pipeline, 
e.g., ``cat file1 file2 | dosomething''.  The wrapper is executed
using perl's system() function:

system(``cd somewhere && wrapper cat file1 file2 | dosomething'').

This means that the wrapper only ``sees'' ``cat file1 file2''.
\\

scratch={\it string} &
A path to a local scratch disk.  Used to store the output of overlap and
consensus as they execute.  This is {\tt /scratch} by default.
\\

\end{longtable}

\subsection{Overlap Based Trimming Options}
\label{sec:obtopts}

Overlap Based Trimming invokes the overlap module, see
Section~\ref{sec:overlapopts} for options to configure the overlapper.
It is not possible to configure the overlapper differently for overlap
based trimming and normal overlaps.

Overlap based trimming writes several log files:

\begin{enumerate}
\item {\it asm}.initialTrimLog -- one line per read.  Immutable reads do not
get modified, and do now appear in the log.  Whitespace separated list
of uid,iid pair, original clear begin, end, quality trim begin and
end, vector clear begin and end, final clear begin, end.

\item {\it asm}.mergeLog -- one line per read.  Whitespace separated list of
IID, final left and right trimming.  Trimming due to chimera and spur
detection are not included here.  All reads are reported.

\item {\it asm}.chimera.report -- many lines per read.  It shows the type of
problem fixed, the resulting clear range, and any evidence for the
change.
\end{enumerate}


\begin{longtable}{lp{3.0in}}
doOverlapTrimming={\it integer} &
If non-zero, do overlap-based trimming.  The default is to do overlap
based trimming.
\\

vectorIntersect={\it path} &
The path to a file containing a list of the vector clear range for
each read.  Format {\it uid vector-left vector-right}, one UID per
line.  Coordiates are base-based.
\\

immutableFrags={\it path} &
The path to a file containing a list of uids that should not be
modified by overlap-based trimming.  One UID per line.
\\

ovlSortMemory={\it integer} &
The amount of memory, in megabytes, to use for sorting overlaps.  The
default is to use 1024MB memory.
\\

\end{longtable}



\subsection{Overlapper Options}
\label{sec:overlapopts}

Overlapper performs an all-fragments against all-fragments alignment.
Each pair of fragments is aligned to decide if they overlap.  In
effect, it is populating an array with alignment information.
Overlapper is able to segment the computation on both axes of the
array.  The fragments along one axis are used to construct a
hash-table to seed the alignments.  The fragments along the other axis
then query the hash-table one at a time.

For small assemblies, one can simply divide the number of fragments by
the amount of parallelization one wishes to get and use that.  To get
16 jobs, divide your number of fragments by 4.

For large assemblies, the author advocates using a large {\it
ovlRefBlockSize}, and using {\it ovlHashBlockSize} to control the number of
jobs.  See {\it ovlMemory}.

\begin{longtable}{lp{3.0in}}
ovlThreads={\it integer} &
The number of compute threads to use.  Usually the number of CPUs your
host has.  Even if your grid schedules N jobs per N CPU host, there is
an advantage to telling each job to use N threads -- when one jobs
does I/O, the other jobs will use the now mostly idle CPU.  The default is 2 threads.
\\

ovlHashBlockSize={\it integer} &
The number of fragments to use for hash table construction.
Overlapper will, internally, fragment this range so as to not exceed
its memory limit.  The default is 40,000 fragments.
\\

ovlRefBlockSize={\it integer} &
The number of fragments to use for generating alignments.  The default
is 2,000,000 fragments.
\\

ovlMemory={\it integer} &
One of a set of predefined memory sizes, optionally followed by any
detailed overlap memory switched (not documented here).  The memory sizes are:
{\it 256MB},
{\it 1GB},
{\it 2GB},
{\it 4GB},
{\it 8GB} and
{\it 16GB}.
\\

ovlOnGrid={\it integer} &
If zero, do not use the grid for overlapping.  This is enabled by
default, however, {\em useGrid} is not enabled by default, thus,
overlapping, by default is not done on the grid.
\\

ovlStoreMemory={\it integer} &
The amount of memory, in megabytes, to use for building the overlap
store.  It is critical that this value be as large as possible,
especially with large assemblies.  The default is 1024MB memory.
\\

merSize={\it integer} &
Sets both {\it merSizeObt} and {\it merSizeOvl}.
\\

merSizeObt={\it integer} &
Sets the overlapper and meryl k-mer size used when computing overlaps
for Overlap Based Trimming.
\\

merSizeOvl={\it integer} &
Sets the overlapper and meryl k-mer size used when computing normal
overlaps.
\\

merylObtThreshold={\tt integer} &
Mers with count larger than this value will not be used to seed
overlaps for Overlap Based Trimming.
\\

merylOvlThreshold={\tt integer} &
Mers with count larger than this value will not be used to seed
normal overlaps.
\\
\end{longtable}



\subsection{Fragment Error Correction Options}

\begin{longtable}{lp{3.0in}}
frgCorrBatchSize={\it integer} &
The number of reads to load into core at once.  Fragment error
correction will then scan the entire fragment store, recomputing
overlaps.  As a (very) rough guide:

\begin{center}
\begin{tabular}{|c|c|}
\hline
frgCorrBatchSize & Memory (MB) \\
\hline
\hline
   10,000 &     132 \\
   50,000 &     650 \\
  100,000 &    1300 \\
  200,000 &    2500 \\
  500,000 &    6300 \\
1,000,000 &   13000 \\
2,000,000 &   23000 \\
2,500,000 &   32000 \\
3,000,000 &   32000 \\
\hline
\end{tabular}
\end{center}

The larger batch sizes assume {\tt correct-frags} is NOT using
a temporary internal store.  This is probably not enabled by default.
\\

doFragmentCorrection={\it integer} &
If non-zero, do fragment error correction (and, implicitly, overlap
error correction).  The default is to do fragment error correction.
\\

frgCorrThreads={\it integer} &
The number of threads to use for fragment error correction.
\\

frgCorrOnGrid={\it integer} &
If zero, do not use the grid.  Fragment error correction makes heavy
use of the fragment store.  Unless your grid has fast
access to this store, use of the grid is strongly discouraged.
\\

frgCorrConcurrency={\it integer} &
If the grid is not enabled, run this many fragment correction jobs at the same
time.  Default is 1.
\\

ovlCorrBatchSize={\it integer} &
{\it more documentation needed!}  1,000,000 uses about 2.5GB memory.  400,000 uses 750MB.
\\

ovlCorrOnGrid={\it integer} &
If zero, do not use the grid.  XXXXX.  Unless your grid has fast
access to this store, use of the grid is strongly discouraged.
\\

ovlCorrConcurrency={\it integer} &
If the grid is not enabled, run this many overlap correction jobs at the same
time.  Default is 4.
\\
\end{longtable}




\subsection{Unitigger Options}

\begin{longtable}{lp{3.0in}}
utgGenomeSize={\it integer} &
The genome size, in bases, to force unitigger to use.  This option is
for advanced use only, and is not usually specified.
\\

utgEdges={\it integer} &
The estimated number of edges unitigger will encounter.  If you find that unitigger
is exhausting its process size, setting this to slightly more than the actual number of edges
might help.  Otherwise, do not use.
\\

utgErrorRate={\it integer} &
The error rate above which unitigger discards overlaps, expressed as errors per thousand.
The default is 15, or 1.5\% error.
\\

utgFragments={\it integer} &
The estimated number of fragments unitigger will encounter.  If you
find that unitigger is exhausting its process size, setting this to
slightly more than the actual number of fragments might help.
Otherwise, do not use.
\\

utgBubblePopping={\it integer} &
If zero, do not pop bubbles in unitigger.  If one, pop bubbles.  Default is to pop bubbles.
//
\end{longtable}



\subsection{Scaffolder Options}

\begin{longtable}{lp{3.0in}}

cgwOutputIntermediate={\it integer} &
if non-zero, intermediate CGW runs will output the .cgw,
.cgw_scaffolds and .cgw_contigs files.  The default is to not output
these files.
\\

cgwPurgeCheckpoints={\it integer} &
If non-zero (the default) remove all but the final CGW checkpoint file.
\\

stoneLevel={\it integer} &
The amount of stones thrown in the last iteration of cgw.
\\

delayInterleavedMerging={\it integer} &
If non-zero, interleaved scaffold merging will be performed only when
stones are thrown, usually the last iteration of cgw, in particular,
after all iterations of {\it extendClearRanges}.  The default is to
not delay merging, and do merging in the very first cgw.
\\

doExtendClearRanges={\it integer} &
If non-zero, do that many round of {\it extendClearRanges}.  Stones are disabled
until after all rounds.
\\

doUpdateDistanceRecords={\it integer} &
If non-zero, update the distance records after each round of {\it extendClearRanges}.  THIS IS
CURRENTLY BROKEN.  Instead, if {\it updateDistanceType} is ``pre'', distance records are
updated by computing a scratch cgw before the normal scaffolding process starts.
\\

doResolveSurrogates={\it integer} &
If non-zero, resolve surrogates.
\\
\end{longtable}



\subsection{Consensus Options}

These options apply to both post-unitigger and post-scaffolder consensus.

\begin{longtable}{lp{3.0in}}
cnsPartitions={\it integer} &
The approximate number of partitions unitigger/scaffolder will
generate for consensus.  There will be no more than this, but likely
will be fewer.  The default is 128 partitions, or partitions
consisting of about {\it cnsMinFrags} fragments, whichever results in fewer
partitions.
\\

cnsMinFrags={\it integer} &
The minimum number of fragments in a consensus partition.  Default
is 75,000.
\\

cnsOnGrid={\it integer} &
If zero, do not use the grid for consensus.  This is enabled by
default, however, {\em useGrid} is not enabled by default, thus,
consensus, by default is not done on the grid.
\\

cnsConcurrency={\it integer} &
If the grid is not enabled, run this many consensus jobs at the same
time.  Default is 2.
\\
\end{longtable}


\subsection{Terminator Options}
\begin{longtable}{lp{3.0in}}
createAGP={\it integer} &
If non-zero, create an AGP file for the scaffolds.  Default is 1.
\\

createACE={\it integer} &
If non-zero, create an ACE file for the scaffolds.  Default is 1.
\\

createPosMap={\it integer} &
If non-zero, create the ``posmap'' files that map fragments, contigs,
variation records, etc, with contig and scaffold coordinates.  Default
is 1.
\\

merQCmemory={\it integer} &
Use xMB of memory, at most, when computing the merQC.  Default is 1024MB.
\\

merQCmerSize={\it integer} &
Use size k mers for the merQC.  Default is 22.
\\

\end{longtable}

\end{document}
