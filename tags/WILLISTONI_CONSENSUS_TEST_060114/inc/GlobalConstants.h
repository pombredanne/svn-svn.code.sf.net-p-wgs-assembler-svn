/* Don't include anything in this file, it is for global constants only and should
 * contain no code
 */
#ifndef GLOBALCONSTANTS_H
#define GLOBALCONSTANTS_H

#define AS_FRAG_MAX_LEN (2048)
#define AS_FRAG_MIN_LEN (64)

#endif
