
Code was never called, appears to have been superceded by
ComputeMatePairStatisticsRestricted()


void ComputeMatePairStatistics( int operateOnNodes,
                                int minSamplesForOverride,
                                char *instance_label)
{
  GraphCGW_T *graph = NULL;
  GraphNodeIterator nodes;
  NodeCGW_T *node;
  DistT *dptr;
  int i, maxSamples = 1;
  MultiAlignT *ma = CreateEmptyMultiAlignT();
  int numPotentialRocks = 0;
  int numPotentialStones = 0;
  //  VA_TYPE(int32) *dptrFrags[GetNumDistTs(ScaffoldGraph->Dists)];
  // VA_TYPE(int32) *dptrMates[GetNumDistTs(ScaffoldGraph->Dists)];
  int NN = (int) GetNumDistTs(ScaffoldGraph->Dists);
  VA_TYPE(CDS_CID_t) *dptrFrags[NN];
  VA_TYPE(CDS_CID_t) *dptrMates[NN];
  

  // make the stat directory if it doesn't exist already
  {
	char filename[128];
	DIR *dout;
	
	sprintf( filename, "stat");
	fprintf( stderr, "opening directory: %s\n", filename);
	dout = opendir( filename);
	if ( dout == NULL )
	{
	  fprintf( stderr, "could not open directory: %s, creating...\n", filename);
	  system( "mkdir stat" );
	  dout=opendir(filename);
	  assert(dout !=NULL);
	}
	closedir(dout);
  }
  
  if (operateOnNodes == UNITIG_OPERATIONS)
    graph = ScaffoldGraph->CIGraph;
  else if (operateOnNodes == CONTIG_OPERATIONS)
    graph = ScaffoldGraph->ContigGraph;
  if (operateOnNodes == SCAFFOLD_OPERATIONS)
    graph = ScaffoldGraph->CIGraph;
  
  // Initialize computed mean/variance and counters, and bounds
  for(i = 1; i < GetNumDistTs(ScaffoldGraph->Dists); i++)
  {
    DistT *dptr = GetDistT(ScaffoldGraph->Dists,i);
    dptr->mu = 0.0;
    dptr->sigma = 0.0;
    dptr->numSamples = 0.0;
    dptr->numReferences = 0;
    dptr->min = CDS_COORD_MAX;
    dptr->max = CDS_COORD_MIN;
    dptr->bsize = 0;
    dptr->numBad = 0;
    dptr->histogram = NULL;
    // Lower and upper are based on nominal mean and stddev
    dptr->lower = dptr->mean - CGW_CUTOFF*dptr->stddev;
    dptr->upper = dptr->mean + CGW_CUTOFF*dptr->stddev;
    
    if(dptr->samples) // starting from scratch
      ResetVA_CDS_COORD_t(dptr->samples);
    else  // if are starting from a checkpoint?
      dptr->samples = CreateVA_CDS_COORD_t(16);
    
#if 1
    dptrFrags[i] = CreateVA_CDS_CID_t(256);
    dptrMates[i] = CreateVA_CDS_CID_t(256);
#endif
  }
  
  InitGraphNodeIterator(&nodes, graph, GRAPH_NODE_DEFAULT);
  
  while(NULL != (node = NextGraphNodeIterator(&nodes)))
  {
    int i;
    int numFrags;
    int numExternalLinks = 0;
    
    if(node->id % 100000 == 0)
    {
      fprintf(GlobalData->stderrc, "node " F_CID "\n", node->id);
      fflush(GlobalData->stderrc);
    }
    //	fprintf(GlobalData->stderrc,"* Marking mates of " F_CID " frags in %s " F_CID "\n",	
    // numFrags, (operateOnNodes == CONTIG_OPERATIONS ? "Contig" : "Unitig"), node->id);
    
    // Don't waste time loading singletons for this
    if(node->flags.bits.isChaff)
      continue;
    
    ReLoadMultiAlignTFromSequenceDB(ScaffoldGraph->sequenceDB, ma, node->id, graph->type == CI_GRAPH);
    numFrags  = GetNumIntMultiPoss(ma->f_list);
    
    if (numFrags < 2)
      continue;
    
    for( i = 0; i < numFrags; i++)
    {
      IntMultiPos *mp = GetIntMultiPos(ma->f_list, i);
      CIFragT *frag, *mate;
      CDS_COORD_t dist;
      
      frag = GetCIFragT(ScaffoldGraph->CIFrags, (CDS_CID_t)mp->source);
      assert(frag->iid == mp->ident);
      
#if 0
      fprintf(GlobalData->stderrc,"* frag " F_CID " (" F_CID ") mate:" F_CID " numLinks:%d extCI:%d extCon:%d\n",
              (CDS_CID_t)mp->source, frag->iid, frag->mateOf, frag->numLinks, 
              frag->flags.bits.hasInternalOnlyCILinks,
              frag->flags.bits.hasInternalOnlyContigLinks);
#endif
      // This is important for keeping our computation as local as possible.
      // We skip fragments that have external links only, or no links
      if (frag->numLinks == 0)
        continue;
      if (frag->numLinks == 1 &&  // the typical case
          (operateOnNodes == CONTIG_OPERATIONS && !frag->flags.bits.hasInternalOnlyContigLinks))
        //     ||    (!operateOnContigs && !frag->flags.bits.hasInternalOnlyCILinks))
        continue;
      
      mate = GetCIFragT(ScaffoldGraph->CIFrags,frag->mateOf);
      
      if (operateOnNodes == UNITIG_OPERATIONS && mate != NULL && (mate->cid != frag->cid))
        numExternalLinks++;
      
      // If the id of the current fragment is greater than its mate, skip it, to
      // avoid double counting.
      //
      if ((CDS_CID_t)mp->source > frag->mateOf){
        continue;		// only examine each pair once
      }
      
      // temp checking 5/9/01 MJF
      {
        if (mate == NULL)
        {
          fprintf( stderr, "NULL mate encountered for frag->iid: " F_CID "\n", frag->iid);
        }
        
        if ( mate->mateOf != (CDS_CID_t)mp->source )
        {
          InfoByIID *info;
          
          fprintf( stderr, "mate->mateOf (" F_CID ") != (CDS_CID_t)mp->source (" F_CID ") for frag->iid: " F_CID "\n", 
                   mate->mateOf, (CDS_CID_t)mp->source, frag->iid);
          
          info = GetInfoByIID( ScaffoldGraph->iidToFragIndex, mate->iid);
          fprintf( stderr, "frag->mateOf (" F_CID "), mate->iid: " F_CID ", mate index: " F_CID "\n", 
                   frag->mateOf, mate->iid, info->fragIndex);
          
          info = GetInfoByIID( ScaffoldGraph->iidToFragIndex, frag->iid);
          fprintf( stderr, "mate->mateOf (" F_CID "), frag->iid: " F_CID ", frag index: " F_CID "\n", 
                   mate->mateOf, frag->iid, info->fragIndex);
        }	  
      }
      
      assert(mate != NULL && mate->mateOf == (CDS_CID_t)mp->source);
      dptr = GetDistT(ScaffoldGraph->Dists, frag->dist);
      dptr->numReferences++;
      
      if (operateOnNodes == UNITIG_OPERATIONS)
      {
        if (frag->cid != mate->cid)
          continue;
        dist = (CDS_COORD_t) mate->offset5p.mean - frag->offset5p.mean; 
        if(	getCIFragOrient(mate) == getCIFragOrient(frag)) 
        {
          //      fprintf(GlobalData->stderrc,"* (" F_CID "," F_CID ") is bad due to orientation problems\n",      frag->iid, mate->iid);
          frag->flags.bits.edgeStatus = UNTRUSTED_EDGE_STATUS;
          mate->flags.bits.edgeStatus = UNTRUSTED_EDGE_STATUS;
          dptr->numBad++;
          continue;
        }
        if((frag->flags.bits.innieMate && getCIFragOrient(frag) == B_A) ||
           (!frag->flags.bits.innieMate && getCIFragOrient(frag) == A_B) )
          dist = -dist;
      }
      else if (operateOnNodes == CONTIG_OPERATIONS)
      {
        assert(frag->contigID == mate->contigID);
        if(GetContigFragOrient(mate) == GetContigFragOrient(frag)) 
        {
          //  fprintf(GlobalData->stderrc,"* (" F_CID "," F_CID ") is bad due to orientation problems\n",      frag->iid, mate->iid);
          frag->flags.bits.edgeStatus = UNTRUSTED_EDGE_STATUS;
          mate->flags.bits.edgeStatus = UNTRUSTED_EDGE_STATUS;
          dptr->numBad++;
          continue;
        }
        
        dist = (CDS_COORD_t) mate->contigOffset5p.mean - frag->contigOffset5p.mean; 
        //   -------------------->          <----------------------
        //     mate                 innie            frag 
        
        //   <-------------------           ---------------------->
        //     mate                 outie            frag 
        
        if((frag->flags.bits.innieMate && GetContigFragOrient(frag) == B_A) ||
           (!frag->flags.bits.innieMate && GetContigFragOrient(frag) == A_B) )
          dist =  -dist;
        
      }
      else if (operateOnNodes == SCAFFOLD_OPERATIONS)
      {
        NodeCGW_T *fragContig, *mateContig;
        CDS_COORD_t fragLeftEnd, fragRightEnd;
        CDS_COORD_t mateLeftEnd, mateRightEnd;
        int fragScaffoldOrientation, mateScaffoldOrientation;
        if (dptr->mean < 5000)
          continue;
        
        fragContig = GetGraphNode( ScaffoldGraph->ContigGraph, frag->contigID);
        AssertPtr(fragContig);
        
        mateContig = GetGraphNode( ScaffoldGraph->ContigGraph, mate->contigID);
        AssertPtr(mateContig);
        
        
        // we want them to be in the same scaffold
        if ( fragContig->scaffoldID != mateContig->scaffoldID || fragContig->scaffoldID == -1)
          continue;
        
        GetFragmentPositionInScaffold( frag, &fragLeftEnd, &fragRightEnd, &fragScaffoldOrientation);
        GetFragmentPositionInScaffold( mate, &mateLeftEnd, &mateRightEnd, &mateScaffoldOrientation);
        
#if 0
        if (frag->contigID != mate->contigID && fragContig->scaffoldID != -1
            && mateContig->scaffoldID != -1
            && mateContig->scaffoldID == fragContig->scaffoldID)
          fprintf( stderr, "frags (" F_CID ", " F_CID ") are in contigs (" F_CID ", " F_CID ") in scaffold " F_CID " at (" F_COORD ", " F_COORD ", %d) and (" F_COORD ", " F_COORD ", %d)\n", 
                   frag->iid, mate->iid, frag->contigID, mate->contigID,
                   fragContig->scaffoldID,
                   fragLeftEnd, fragRightEnd, fragScaffoldOrientation,
                   mateLeftEnd, mateRightEnd, mateScaffoldOrientation);
#endif
        
        if (fragScaffoldOrientation == mateScaffoldOrientation) 
        {
          // fprintf(GlobalData->stderrc,"* (" F_CID "," F_CID ") is bad due to orientation problems\n",      frag->iid, mate->iid);
          frag->flags.bits.edgeStatus = UNTRUSTED_EDGE_STATUS;
          mate->flags.bits.edgeStatus = UNTRUSTED_EDGE_STATUS;
          dptr->numBad++;
          continue;
        }
        
        if (frag->flags.bits.innieMate)  
        {
          if (fragScaffoldOrientation == 0) // frag ---->  <---- mate
            dist = (CDS_COORD_t) mateRightEnd - fragLeftEnd;		  
          else                              // mate ---->  <---- frag
            dist = (CDS_COORD_t) fragRightEnd - mateLeftEnd;
        }
        else  // outtie pair
        {
          if (fragScaffoldOrientation == 0) // mate <----  ----> frag
            dist = (CDS_COORD_t) fragRightEnd - mateLeftEnd;		  
          else                              // frag <----  ----> mate
            dist = (CDS_COORD_t) mateRightEnd - fragLeftEnd;
        }
        
        if (dist < 0)
          fprintf( stderr, "frag, mate: " F_CID ", " F_CID " have negative dist: " F_COORD "\n",
                   frag->iid, mate->iid, dist);
      }
      
      if (dist > 0 && dist < dptr->min)
        dptr->min = dist;
      if (dist > dptr->max)
        dptr->max = dist;
      
      AppendCDS_COORD_t( dptr->samples, &dist);
      AppendCDS_CID_t( dptrFrags[frag->dist], &frag->iid);
      AppendCDS_CID_t( dptrMates[frag->dist], &mate->iid);
      
      // fprintf( stderr, "adding (" F_CID ", " F_CID ") to dist " F_COORD " samples\n", frag->iid, mate->iid, dist);
      
      if (1)
      {
        // See if the mate distance implied is outside of a 5-sigma range
        
        if (dist < dptr->lower || dist > dptr->upper) 
        {
          //if (dist > 5000)
          // fprintf(GlobalData->stderrc,"* (" F_CID "," F_CID ") lib:" F_CID " is bad due to distance problems " F_COORD " is outside [" F_COORD "," F_COORD "]\n",  
          //	frag->iid, mate->iid, frag->dist, dist, dptr->lower, dptr->upper);
          frag->flags.bits.edgeStatus = UNTRUSTED_EDGE_STATUS;
          mate->flags.bits.edgeStatus = UNTRUSTED_EDGE_STATUS;
          dptr->numBad++;
        }
        else
        {
          frag->flags.bits.edgeStatus = TRUSTED_EDGE_STATUS;
          mate->flags.bits.edgeStatus = TRUSTED_EDGE_STATUS;
          
          //if (dist > 5000)
          //fprintf(GlobalData->stderrc,"* (" F_CID "," F_CID ") lib:" F_CID " is trusted due to distance " F_COORD " being inside [" F_COORD "," F_COORD "]\n",  
          //	frag->iid, mate->iid, frag->dist, dist, dptr->lower, dptr->upper);
          
          dptr->numSamples++;
          dptr->mu += dist;
          dptr->sigma += (((double)dist)*(double)dist);
        }
      }
    }
    
    //        UnloadMultiAlignTFromSequenceDB(ScaffoldGraph->sequenceDB, node->id, graph->type == CI_GRAPH);
    
    // Mark unitigs as potential Rocks and Stones
    if (operateOnNodes == UNITIG_OPERATIONS)
    {
      int rock = FALSE;
      int stone = FALSE;
      switch(numExternalLinks){
        case 0:
          stone = TRUE;
          rock = FALSE;
          break;
        case 1:
          stone = TRUE;
          rock = FALSE;
          numPotentialStones++;
          break;
        case 2:
        default:
          stone = rock = TRUE;
          numPotentialRocks++;
          break;
      }
      node->flags.bits.isPotentialRock = rock;
      node->flags.bits.isPotentialStone = stone;
      
#if 0
      fprintf(stderr,"* Unitig " F_CID " has %d external links r:%s s:%s\n", node->id, numExternalLinks, 
              (rock?" YES ": "NO"),
              (stone?" YES ": "NO"));
#endif
    }
  }
  
  if (operateOnNodes == UNITIG_OPERATIONS)
  {
    fprintf(GlobalData->stderrc,
            "* ComputeMatePairStats has marked %d/%d unitigs as potential rocks +  %d/%d as potential stones\n",
            numPotentialRocks, (int) GetNumGraphNodes(graph),
            numPotentialStones, (int) GetNumGraphNodes(graph));
  }
  
#if 1
  // mjf
  
  for (i = 1; i < GetNumDistTs(ScaffoldGraph->Dists); i++)
  {
    fprintf( stderr, "GetNumCDS_COORD_ts( GetDistT(ScaffoldGraph->Dists, " F_CID ")->samples) = %d\n", 
             i, (int) GetNumCDS_COORD_ts( GetDistT(ScaffoldGraph->Dists, i)->samples));
    if ( GetNumCDS_COORD_ts( GetDistT(ScaffoldGraph->Dists, i)->samples) > maxSamples)
      maxSamples = (int) GetNumCDS_COORD_ts( GetDistT(ScaffoldGraph->Dists, i)->samples);
  }
  
  fprintf( stderr, "maxSamples = %d\n", maxSamples);
  
  // now sort the samples, mates, and frags arrays, based on samples
  for (i = 1; i < GetNumDistTs(ScaffoldGraph->Dists); i++)
  {
    MateInfoT matePairs[maxSamples];
    int icnt;
    CDS_COORD_t newLower, newUpper;
    CDS_COORD_t median = 0, lowerSigma = 0, upperSigma = 0;
    
    dptr = GetDistT(ScaffoldGraph->Dists, i);
    if(dptr->numReferences == 0)
      continue;
    if (dptr->numSamples == 0 || dptr->numSamples == 1)
      continue;
    
    for ( icnt = 0; icnt < GetNumCDS_COORD_ts( dptr->samples ); icnt++)
    {
      matePairs[icnt].samples = *GetCDS_COORD_t( dptr->samples, icnt);
      matePairs[icnt].frags = *GetCDS_CID_t( dptrFrags[i], icnt);
      matePairs[icnt].mates = *GetCDS_CID_t( dptrMates[i], icnt);
    }
    
    qsort( matePairs, GetNumCDS_COORD_ts( dptr->samples ),
           sizeof(MateInfoT), &compDists);
    
    // now find median
    median = matePairs[ (int) (GetNumCDS_COORD_ts( dptr->samples ) / 2)].samples;
    
    // find lower and upper "sigmas"
    lowerSigma = median - matePairs[ (int) ((0.5 - 0.34) * GetNumCDS_COORD_ts( dptr->samples ))].samples;
    upperSigma = - median + matePairs[ (int) ((0.5 + 0.34) * GetNumCDS_COORD_ts( dptr->samples ))].samples;	
    
    newLower = median - 5 * max (lowerSigma, upperSigma);
    newUpper = median + 5 * max (lowerSigma, upperSigma);
    
    fprintf( stderr, "\nlib " F_CID ", numSamples: %d, orig mean, sig: ( %.2f, %.2f), calc mean, sig: (%.2f, %.2f) median: " F_COORD "\n", 
             i, dptr->numSamples, dptr->mean, dptr->stddev, dptr->mu/dptr->numSamples, 
             sqrt((dptr->sigma - (dptr->mu*dptr->mu)/dptr->numSamples) / (dptr->numSamples - 1)),
             median);
    fprintf( stderr, "dptr->lower: " F_COORD ", dptr->upper: " F_COORD "\n", dptr->lower, dptr->upper);
    fprintf( stderr, "  dptr->min: " F_COORD ",   dptr->max: " F_COORD "\n", dptr->min, dptr->max);
    fprintf( stderr, "lowerSigma: " F_COORD ", upperSigma: " F_COORD "\n", lowerSigma, upperSigma);						  
    fprintf( stderr, "newLower: " F_COORD ", newUpper: " F_COORD "\n", newLower, newUpper);
    
    // now reset the trusted flag if necessary
    // first see if there are edges marked untrusted that are now considered trusted
    // lower set
    if (dptr->lower > newLower)
    {
      fprintf( stderr, "GetNumCDS_COORD_ts( dptr->samples ): %d\n",
               (int) GetNumCDS_COORD_ts( dptr->samples )); 
      for ( icnt = 0; icnt < GetNumCDS_COORD_ts( dptr->samples ); icnt++)
      {
        if (matePairs[icnt].samples < dptr->lower && matePairs[icnt].samples > newLower)
        {
          CIFragT *frag, *mate;
          
          frag = GetCIFragT( ScaffoldGraph->CIFrags, 
                             GetInfoByIID(ScaffoldGraph->iidToFragIndex, matePairs[icnt].frags)->fragIndex);
          mate = GetCIFragT( ScaffoldGraph->CIFrags,
                             GetInfoByIID(ScaffoldGraph->iidToFragIndex, matePairs[icnt].mates)->fragIndex);
          
          fprintf( stderr, "1 reclassifying samples[%d] (" F_CID ", " F_CID ") from UNTRUSTED to TRUSTED\n", 
                   icnt, frag->iid, mate->iid);
          
          frag->flags.bits.edgeStatus = TRUSTED_EDGE_STATUS;
          mate->flags.bits.edgeStatus = TRUSTED_EDGE_STATUS;
          dptr->numBad--;
          dptr->numSamples++;
          dptr->mu += matePairs[icnt].samples;
          dptr->sigma += (((double) matePairs[icnt].samples) * (double) matePairs[icnt].samples);
        }
      }
    }
    
#if 1
    // upper set
    if (dptr->upper < newUpper)
    {
      fprintf( stderr, "GetNumCDS_COORD_ts( dptr->samples ): %d\n",
               (int) GetNumCDS_COORD_ts( dptr->samples )); 
      for ( icnt = 0; icnt < GetNumCDS_COORD_ts( dptr->samples ); icnt++)
      {	
        if (matePairs[icnt].samples > dptr->upper && matePairs[icnt].samples < newUpper)
        {
          CIFragT *frag, *mate;
          
          frag = GetCIFragT( ScaffoldGraph->CIFrags, 
                             GetInfoByIID(ScaffoldGraph->iidToFragIndex, matePairs[icnt].frags)->fragIndex);
          mate = GetCIFragT( ScaffoldGraph->CIFrags,
                             GetInfoByIID(ScaffoldGraph->iidToFragIndex, matePairs[icnt].mates)->fragIndex);
          
          fprintf( stderr, "2 reclassifying samples[%d] (" F_CID ", " F_CID ") from UNTRUSTED to TRUSTED\n", 
                   icnt, frag->iid, mate->iid);
          
          frag->flags.bits.edgeStatus = TRUSTED_EDGE_STATUS;
          mate->flags.bits.edgeStatus = TRUSTED_EDGE_STATUS;
          dptr->numBad--;
          dptr->numSamples++;
          dptr->mu += matePairs[icnt].samples;
          dptr->sigma += (((double) matePairs[icnt].samples) * (double) matePairs[icnt].samples);
        }
      }
    }
#endif
    
#if 1	
    // now see if there are edges marked trusted that are now considered untrusted
    // lower set
    if (dptr->lower < newLower)
    {
      fprintf( stderr, "GetNumCDS_COORD_ts( dptr->samples ): %d\n",
               (int) GetNumCDS_COORD_ts( dptr->samples )); 
      for ( icnt = 0; icnt < GetNumCDS_COORD_ts( dptr->samples ); icnt++)
      {
        if (matePairs[icnt].samples > dptr->lower && matePairs[icnt].samples < newLower)
        {
          CIFragT *frag, *mate;
          
          frag = GetCIFragT( ScaffoldGraph->CIFrags, 
                             GetInfoByIID(ScaffoldGraph->iidToFragIndex, matePairs[icnt].frags)->fragIndex);
          mate = GetCIFragT( ScaffoldGraph->CIFrags,
                             GetInfoByIID(ScaffoldGraph->iidToFragIndex, matePairs[icnt].mates)->fragIndex);
          
          fprintf( stderr, "3 reclassifying samples[%d] (" F_CID ", " F_CID ") from TRUSTED to UNTRUSTED\n", 
                   icnt, frag->iid, mate->iid);
          
          frag->flags.bits.edgeStatus = UNTRUSTED_EDGE_STATUS;
          mate->flags.bits.edgeStatus = UNTRUSTED_EDGE_STATUS;
          dptr->numBad++;
          dptr->numSamples--;
          dptr->mu -= matePairs[icnt].samples;
          dptr->sigma -= (((double) matePairs[icnt].samples) * (double) matePairs[icnt].samples);
        }
      }
    }
    // upper set
    if (dptr->upper > newUpper)
    {
      fprintf( stderr, "GetNumCDS_COORD_ts( dptr->samples ): %d\n",
               (int) GetNumCDS_COORD_ts( dptr->samples )); 
      for ( icnt = 0; icnt < GetNumCDS_COORD_ts( dptr->samples ); icnt++)
      {	
        if (matePairs[icnt].samples < dptr->upper && matePairs[icnt].samples > newUpper)
        {
          CIFragT *frag, *mate;
          
          frag = GetCIFragT( ScaffoldGraph->CIFrags, 
                             GetInfoByIID(ScaffoldGraph->iidToFragIndex, matePairs[icnt].frags)->fragIndex);
          mate = GetCIFragT( ScaffoldGraph->CIFrags,
                             GetInfoByIID(ScaffoldGraph->iidToFragIndex, matePairs[icnt].mates)->fragIndex);
          
          fprintf( stderr, "4 reclassifying samples[%d] (" F_CID ", " F_CID ") from TRUSTED to UNTRUSTED\n", 
                   icnt, frag->iid, mate->iid);
          
          frag->flags.bits.edgeStatus = UNTRUSTED_EDGE_STATUS;
          mate->flags.bits.edgeStatus = UNTRUSTED_EDGE_STATUS;
          dptr->numBad++;
          dptr->numSamples--;
          dptr->mu -= matePairs[icnt].samples;
          dptr->sigma -= (((double) matePairs[icnt].samples) * (double) matePairs[icnt].samples);
        }
      }
    }
#endif
    // free(matePairs);
  }
  
  for (i = 1; i < GetNumDistTs(ScaffoldGraph->Dists); i++)
  {
    DeleteVA_CDS_CID_t( dptrFrags[i] );
    DeleteVA_CDS_CID_t( dptrMates[i] );
  }
  
#endif
  
  /* now set mean, stddev, size & number of buckets */
  for(i = 1; i < GetNumDistTs(ScaffoldGraph->Dists); i++)
  {
    dptr = GetDistT(ScaffoldGraph->Dists, i);
    if(dptr->numReferences == 0)
    {
      //      fprintf(GlobalData->stderrc,"* No references for mate distance record " F_CID "\n", i);
      continue;
    }
    if (dptr->numSamples == 0)
    {
      fprintf(GlobalData->stderrc,"* No samples for mate distance record " F_CID "... numBad = %d/%d\n", 
              i, dptr->numBad, dptr->numReferences);
      continue;
    }
    if(dptr->numSamples == 1)
      dptr->sigma = -1.0;
    else
      dptr->sigma = sqrt((dptr->sigma - (dptr->mu*dptr->mu)/dptr->numSamples) /
                         (dptr->numSamples - 1));
    
    dptr->mu = dptr->mu/dptr->numSamples;
    
    if(dptr->numSamples > minSamplesForOverride) // hack  || (operateOnNodes > UNITIG_OPERATIONS && dptr->numSamples > 100))
    {
      fprintf(GlobalData->stderrc,
              "**** >>>>>>>>>  Distance record " F_CID " updated from %g +/- %g  ==> %g +/- %g  based on %d samples <<<<<<<< ****\n",
              i,
              dptr->mean, dptr->stddev,
              dptr->mu, dptr->sigma,
              dptr->numSamples);
      
      dptr->mean = dptr->mu;
      dptr->stddev = dptr->sigma;
    }
    
    fprintf(GlobalData->stderrc,
            "* Distance " F_CID ":  mean:%g(nominal %g, delta %g) stddev:%g (nominal %g, delta %g) numSamples:%d numRef:%d\n",
            i, 
            dptr->mu, dptr->mean, fabs((dptr->mu - dptr->mean)/dptr->mean),
            dptr->sigma, dptr->stddev, fabs((dptr->sigma - dptr->stddev)/dptr->stddev),
            dptr->numSamples, dptr->numReferences);
    
    dptr->bsize = dptr->sigma/CGW_NUM_BUCKETS;
    if (dptr->bsize > 1.) 
    {
      dptr->bnum = (dptr->max-dptr->min)/dptr->bsize + 1;
      dptr->bsize = ((float)(dptr->max-dptr->min))/dptr->bnum;
    } 
    else 
    {
      dptr->bnum = 1;
      dptr->bsize = dptr->max-dptr->min;
    }
#if 0
    fprintf(GlobalData->stderrc,"* id:" F_CID " bnum:%d bsize:%g min:" F_COORD " max:" F_COORD "\n",
            i, dptr->bnum, dptr->bsize, dptr->min, dptr->max);
#endif
    free(dptr->histogram);
    dptr->histogram = (int32 *) safe_malloc(sizeof(int32)*dptr->bnum);
    {
      int	j;
      for (j=0; j < dptr->bnum; ++j)
        dptr->histogram[j] = 0;
    }
  }
  
  fprintf(GlobalData->stderrc,"* Bucketizing Data\n");
  fflush(NULL);
  // Now bucketize data
  {
    int i;
    //void *ptr = (void *)ScaffoldGraph->sequenceDB->UnitigStore;
    int32 numDists = GetNumDistTs(ScaffoldGraph->Dists);
    DistT *dist = GetDistT(ScaffoldGraph->Dists,1);
    for(i = 1; i < numDists ; i++, dist++){
      int numSamples = GetNumCDS_COORD_ts(dist->samples);
      CDS_COORD_t *samplep = GetCDS_COORD_t(dist->samples,0);
      int j;
      if(!dist->samples || dist->bsize == 0.0)
        continue;
      for(j = 0; j < numSamples ; j++, samplep++)
      {
        CDS_COORD_t sample = *samplep;
        int32 binNum = ((float)sample - (float)dist->min)/dist->bsize;
        binNum = min(binNum, dist->bnum -1);
        binNum = max(binNum,0); // sample can be negative
        dist->histogram[binNum]++;
      }	  
    }
  }
  {
    int i;
    for(i = 1; i < GetNumDistTs(ScaffoldGraph->Dists); i++)
    {
      int j;
      DistT *dist = GetDistT(ScaffoldGraph->Dists,i);
      if(dist->samples && dist->bsize > 0.0 && dist->numSamples > 0)
      {
        fprintf(GlobalData->stderrc,"* Distance Record " F_CID " min:" F_COORD " max:" F_COORD "  mu:%g sigma:%g samples:%d bad:%d references:%d\n",
                i, dist->min, dist->max, dist->mu, dist->sigma, dist->numSamples, dist->numBad, dist->numReferences);
        fflush(NULL);
        for(j = 0; j < dist->bnum; j++)
        {
          int32 binVal = dist->histogram[j];
          
          fprintf(GlobalData->stderrc,"* [%5d,%5d]\t%d\n",
                  (int32)(dist->min + j * dist->bsize), (int32)(dist->min + (j + 1) * dist->bsize), binVal);
        }
      }
    }
  }
  // output a histogram file for each library
  for( i = 1; i < GetNumDistTs(ScaffoldGraph->Dists); i++)
  {
    int j;
    DistT *dist = GetDistT(ScaffoldGraph->Dists,i);
    if(dist->samples && dist->bsize > 0.0 && dist->numSamples > 0)
    {
      FILE *fout;
      char filename[1024];
      
      sprintf( filename, "stat/%s.distlib_%d.cgm", instance_label, i);
      fprintf( stderr, "writing file %s\n", filename);
      fout = fopen( filename,"w");
      AssertPtr( fout );
      
      fprintf( fout, "stage: %s, lib mean: %f, lib stddev: %f\n", instance_label, dist->mean, dist->stddev);
      
      for( j = 0; j < dist->bnum; j++)
      {
        int32 binVal = dist->histogram[j];
        int k;
        
        for ( k = 0; k < binVal; k++)
          fprintf( fout, "%d\n", (int32)(dist->min + j * dist->bsize));
      }
      fclose( fout );
    }
  }
  DeleteMultiAlignT(ma);
  fprintf(stderr,"* Done bucketizing data\n");
  fflush(NULL);
  
}
