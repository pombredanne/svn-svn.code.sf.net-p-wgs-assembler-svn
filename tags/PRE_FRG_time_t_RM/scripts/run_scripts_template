#!/usr/local/bin/bash
#
###########################################################################
#
# This file is part of Celera Assembler, a software program that 
# assembles whole-genome shotgun reads into contigs and scaffolds.
# Copyright (C) 1999-2004, Applera Corporation. All rights reserved.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received (LICENSE.txt) a copy of the GNU General Public 
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
###########################################################################
#
#
# Usage: $ run_gatekeeper [-P] [-Q] [-c] arg1 arg2 arg3 arg4
# Usage: $ run_screener [-P] [-Q] [-c] arg1 arg2 arg3 arg4

# arg1 : The first argument is the project name.

# arg2 : The last batch IID processed by this script.  Use zero to
# start the pipeline along with the -c command line option.  

# arg3 : The external data inbox containing the input files.

# arg4 : The external data outbox containing pipeline files and stores
# that are no longer necessary for assembly but need to be archived.

# This script is invoked from the directory where the tool is to
# be executed and consequently the directory where all the
# miscellaneous files are created.

# Remember not to edit this script during execution.

# Command line options:

# c : Create the first store. The last batch IID must be 0.

# P : Specifies that the pipeline output file of the gatekeeper is to
# be ASCII rather that binary.

# Q : Run in the simulator mode.  For the gatekeeper this means that
# the quality values are from celsim.  For the screener and
# overlapper, this means that LSF is not used.

QUEUE="-q assembly_wildfire"


#######################################################################

# Remember not to edit this script during execution.

# A problem with this script that the sequence numbers encoded in the
# store names and the batch IIDs of the .inp files can become unequal.

#######################################################################

TIMER="/usr/bin/time"
# AS_BIN="${AS_ROOT}/bin"
# This assumes that the executable is coming directly from the release
# area.  It also assumes that the executable can not be re-built
# during a run.

echo "Began the Celera Assembler at " `date` " in " `pwd` "."
echo "By the way, this script depends on AS_BIN=${AS_BIN}"
echo ""

CREATE_MODE=0
#TOOL=0

#rc=0
#trap "rm -f $s $t; exit \$rc" 0
#case $? in 0);; *) rc=$?; exit;; esac
set -- `getopt cAPQT: "$@"`

while
	test X"$1" != X--
#    getopts "cAPQT:" name
do
	case $1 in
#	case $name in
	-c)	CREATE_MODE=1
            shift 1
		;;
	-P)	OUTPUT_MODE="-P"
            shift 1
		;;
	-Q)	SIM_MODE="-XQ"
            shift 1
		;;
        -A)     ANALYSIS_MODE="-A 1"
            shift 1
		;;
        -T)
#        TOOL="$OPTARG"
		;;
#	?)
#		echo "invalid parameter $1" 1>&2
#                exit
#		;;
	esac
done
shift
#shift $(($OPTIND - 1))
printf "Remaining arguments are: %s\n" "$*"

##########################################################

#
# The command options are now parsed.
#

# CELSIM_CMD="${AS_BIN}/celsim"
GATEKEEPER_CMD="${AS_BIN}/gatekeeper -e 4 -N ${OUTPUT_MODE} ${SIM_MODE}"
URCSCREENER_CMD="${AS_BIN}/lsf_screener.sh -R -s -n 10000 ${QUEUE} -v 1 ${OUTPUT_MODE} ${SIM_MODE}"
OVERLAPPER_CMD="${AS_BIN}/overlap_script.pl -w -K 100 ${QUEUE} ${OUTPUT_MODE} ${SIM_MODE}"
DECHORDER_CMD="${AS_BIN}/fgb ${OUTPUT_MODE} ${ANALYSIS_MODE}"

if [ ${TOOL} -eq 1 ] ; then 
 PIPE_CMD=${GATEKEEPER_CMD}
 store_extension="gkpStore"
 in_pipe_extension="frg"
 out_pipe_extension="inp"
fi

if [ ${TOOL} -eq 2 ] ; then 
 PIPE_CMD=${URCSCREENER_CMD}
 store_extension="urcStore"
 in_pipe_extension="inp"
 out_pipe_extension="urc"
fi


if [ ${TOOL} -eq 3 ] ; then 
 PIPE_CMD=${OVERLAPPER_CMD}
 store_extension="frgStore"
 in_pipe_extension="urc"
 out_pipe_extension="ovl"
fi

if [ ${TOOL} -eq 4 ] ; then 
 PIPE_CMD=${DECHORDER_CMD}
 store_extension="fgbStore"
 in_pipe_extension="ovl"
 out_pipe_extension=""
fi

##########################################################
function run_module {

# type -all run_module


Sprefix=${1}
oldiid=${2}
inbox=${3}
outbox=${4}

echo "* The project name is <${Sprefix}>."
echo "* The external input files are in <${inbox}>."
echo "* The external output files are in <${outbox}>."
echo "* The last batch IID is <${oldiid}>."

if [ ! -d ${inbox} ] ; then
  echo "The inbox is not available" ; exit 1
fi
if [ ! -d ${outbox} ] ; then
  echo "The outbox is not available" ; exit 1
fi

oldiid_formatted=$(printf "%05d" ${oldiid})
oldStore=${Sprefix}_${oldiid_formatted}.${store_extension}
echo "${oldStore} is the old store directory."

#if [ -n $GATEKEEPER_CMD ] ; then
#Blist=${inbox}/*.frg
#else
 newiid=$((oldiid + 1))
 newiid_formatted=$(printf "%05d" ${newiid})
 Bprefix=${Sprefix}_${newiid_formatted}
 Bfile=${Sprefix}_${newiid_formatted}.${in_pipe_extension}
 echo ${Bfile} " is the new input file."
  if ! [ -e ${Bfile} ] ; then
    echo ${Bfile} " is not available" ; exit 1
  fi
#fi

while [ -e ${Bfile} ] ; do 

  echo "The batch file is ${Bfile}."

  newiid=$((oldiid + 1))
  newiid_formatted=$(printf "%05d" ${newiid})
  newStore=${Sprefix}_${newiid_formatted}.${store_extension}
  echo oldiid is ${oldiid}.
  echo newiid is ${newiid}.
  echo oldStore is ${oldStore}.
  echo newStore is ${newStore}.

  if [ ${oldiid} -eq 0 ] ; then
    if [ ${CREATE_MODE} -eq 1 ]; then
      echo "Create an empty store"
      stores="-c -o ${newStore}"
    else
      echo "The -c option is required to create a new store." ; exit 1
    fi
  else
    stores="-i ${oldStore} -o ${newStore}"
  fi
  echo "The stores variable is " ${stores}
  
  if !( [ ${CREATE_MODE} -eq 1 ] || [ -d ${oldStore} ] )
  then
    echo "The old store is not available." ; exit 1
  fi

  if [ -d ${outbox}/${oldStore} ]
  then
    echo "The old store <${oldStore}> is already in the outbox.  Why?"
    exit 1
  fi

  if [ -d ${outbox}/${newStore} ]
  then
    echo "The new store <${newStore}> is already in the outbox.  Why?"
    exit 1
  fi

  if [ -d ${newStore} ]
  then
    echo "The new store <${newStore}> is already in the workbox.  Why?"
    exit 1
  fi

  infile=${Sprefix}_${newiid_formatted}.${in_pipe_extension}
  if ! [ -e ${infile} ]
  then
    echo "The input pipeline file <${infile}> is not in the workbox. Why?"
    exit 1
  fi
  if [ -e ${outbox}/${infile} ]
  then
    echo "The input pipeline file <${infile}> is already in the outbox. Why?"
    exit 1
  fi

  if [ -n "${out_pipe_extension}" ] ; then
  outfile=${Sprefix}_${newiid_formatted}.${out_pipe_extension}
  if [ -e ${outfile} ]
  then
    echo "The output pipeline file <${outfile}> is already in the workbox. Why?"
    exit 1
  fi
  if [ -e ${outbox}/${outfile} ]
  then
    echo "The output pipeline file <${outfile}> is already in the outbox. Why?"
    exit 1
  fi
  fi

if [ ${TOOL} -eq 4 ] ; then 
# Count the number of overlaps in the batch
${AS_BIN}/countmessages < $Bprefix.ovl > $Bprefix.cnt
number_of_fragments=`gawk '/OFG/ { print $3}' $Bprefix.cnt`
number_of_overlaps=`gawk '/OVL/ { print $3}' $Bprefix.cnt`
echo "The number of fragments = ${number_of_fragments:=0}."
echo "The number of overlaps  = ${number_of_overlaps:=0}."
FGBopts="-n ${number_of_fragments} -m ${number_of_overlaps}"
else
FGBopts=""
fi
    echo \
    $TIMER $PIPE_CMD -p ../${Sprefix}.param \
    ${FGBopts} \
    ${stores} ${infile} 2>> ${Bprefix}.log

  if
    $TIMER $PIPE_CMD -p ../${Sprefix}.param \
    ${FGBopts} \
    ${stores} ${infile} 2>> ${Bprefix}.log
  then
    echo "transformation successful"

    echo "Put old store and batch file into outbox..."
    if [ -d ${oldStore} ] ; then 
      if mv ${oldStore} ${outbox} ; then
        echo "The move of ${oldStore} to ${outbox} succeeded."
      else
        echo "The move of ${oldStore} to ${outbox} failed."
        exit 1
      fi
    else
      echo "${oldStore} did not exist and was not moved to ${outbox}."
    fi

    if [ -e ${infile} ] ; then 
      if mv ${infile} ${outbox} ; then
        echo "The move of the input pipeline file <${infile}> to ${outbox} succeeded."
      else
        echo "The move of the input pipeline file <${infile}> to ${outbox} failed."
        exit 1
      fi
    else
      echo "${infile} did not exist and was not moved to ${outbox}."
    fi

    echo "Set up for the next iteration ..."
    oldiid=${newiid}
    oldStore=${newStore}
    newiid=$((oldiid + 1))
    newiid_formatted=$(printf "%05d" ${newiid})
    Bfile=${Sprefix}_${newiid_formatted}.${in_pipe_extension}
  else
    echo "fatal error in tool"
    echo " Check ${Sprefix}_${newiid_formatted}.err and log files."
    exit 1
  fi

done
exit 0

# If the error message is "No such file or directory: /gatekeeper"
#then check the value of the environmental variable AS_BIN.
}

##########################################################
##########################################################

run_module ${1} ${2} ${3} ${4}
