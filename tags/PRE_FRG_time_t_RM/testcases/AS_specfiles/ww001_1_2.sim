#
###########################################################################
#
# This file is part of Celera Assembler, a software program that 
# assembles whole-genome shotgun reads into contigs and scaffolds.
# Copyright (C) 1999-2004, Applera Corporation. All rights reserved.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received (LICENSE.txt) a copy of the GNU General Public 
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
###########################################################################
#
# A member of the "ww" family of DNA specification files are intended for 
# the nematode (Caenorhabditis Elegans) simulations.
#
# The dataset is all 6 Nematode chromosomes concatenated together
# with N's removed. The genome size is 97Mbp.  
#
#
.comment $Id: ww001_1_2.sim,v 1.1.1.1 2004-04-14 13:56:13 catmandew Exp $
.seed
5131
.dna
Z < C_elegans.fasta ;
# A FASTA file has the format: 
# > commment
# ([ACGT]*\n)*
#
# The file length is 98674993 bytes.
# assembly2.celera.com$ cksum C_elegans.fasta
# 209580158 98674993 C_elegans.fasta
#
# Suppose the genome is 97 M bp and each fragment is 500 bp. 
# Then 1x coverage is 194000 fragments.
# Thus we want 2 million fragments for 10x coverage.
#
# USE EQUAL NUMBER OF 10k AND 2k fragments, thus 1_1 File NAME
#
# 30% no mate, 0.7*(80% 2k separated mate, 20% 10k separated mate)
# 30% no mate, 56% 2k separated mate, 14% 10k separated mate
# Let G be the genome length and B the BAC length: 15*G = B*n/2 
# Bac ends are a bit shorter, higher error rate, higher chimer rate,
# low single rate. The desired length range is mean 150k stddev 33k
# (2*10**(-4))*G  BAC END Fragments 
.sample
1333334
300 800 0.5
0 0 0 0
0.005 0.020 0.33 0.33
0.3 1800 2200 0.01
.sample
666666
300 800 0.5
0 0 0 0
0.005 0.020 0.33 0.33
0.3 9000 11000 0.01
.bacends
20000
300 500 0.5
0 0 0 0
.0025 .050 .33 .33
.01 50000 250000 .05
