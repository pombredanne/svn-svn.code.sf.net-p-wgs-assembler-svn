

I came from the end of CIscaffold_Cleanup_CGW.c


/******************* Code in progress below **********************************/
/*****************************************************************************/
/*  
	This function is meant to be called by LeastSquares to merge together two contigs that have a 
	containment relationship between them,
	but it can actually handle dovetails.  If there is no overlap, it asserts.
	This version uses the overlapEdge information passed in rather than recomputing overlaps.
*/
#ifdef NEVER
void   ContigContainment_new(CIScaffoldT *scaffold,
                             NodeCGW_T *prevCI,
                             NodeCGW_T *thisCI,
                             EdgeCGW_T *overlapEdge)
{
  CDS_COORD_t minAhang, maxAhang;
  int flip;
  CDS_CID_t firstContig;
  IntElementPos contigPos;
  int mergeStatus = 0;
  Overlap *contigOverlap;
  ChunkOrientationType overlapOrientation, actualOverlapOrientation;
  NodeCGW_T *leftContig, *rightContig;

  fprintf( GlobalData->stderrc, "1 prevCI->id: " F_CID " (%f, %f) and thisCI->id: " F_CID " (%f, %f)\n",
		   prevCI->id, prevCI->offsetAEnd.mean, prevCI->offsetBEnd.mean,
		   thisCI->id, thisCI->offsetAEnd.mean, thisCI->offsetBEnd.mean);

  if ( overlapEdge->idA == prevCI->id )
  {
	leftContig = prevCI;
	rightContig = thisCI;
  }
  else
  {
	leftContig = thisCI;
	rightContig = prevCI;
  }

  fprintf( GlobalData->stderrc, "1 leftContig->id: " F_CID " and rightContig->id: " F_CID "\n",
		   leftContig->id, rightContig->id);

  if(ContigPositions == NULL)
  {
    ContigPositions = CreateVA_IntElementPos(10);
  }
  ResetVA_IntElementPos(ContigPositions);

  if ( leftContig->offsetAEnd.mean < leftContig->offsetBEnd.mean)  // leftContig is AB
  {
	if ( rightContig->offsetAEnd.mean < rightContig->offsetBEnd.mean) // rightContig is AB
	{
	  overlapOrientation = AB_AB;
	  minAhang = (CDS_COORD_t) (rightContig->offsetAEnd.mean - leftContig->offsetAEnd.mean) - AHANGSLOP;
	  maxAhang = minAhang + (2 * AHANGSLOP);
	}
	else // rightContig is BA
	{
	  overlapOrientation = AB_BA;
	  minAhang = (CDS_COORD_t) (rightContig->offsetBEnd.mean - leftContig->offsetAEnd.mean) - AHANGSLOP;
	  maxAhang = minAhang + (2 * AHANGSLOP);
	}
  }
  else  // leftContig is BA
  {
	if ( rightContig->offsetAEnd.mean < rightContig->offsetBEnd.mean) // rightContig is AB
	{
	  overlapOrientation = BA_AB;
	  minAhang = (CDS_COORD_t) (rightContig->offsetAEnd.mean - leftContig->offsetBEnd.mean) - AHANGSLOP;
	  maxAhang = minAhang + (2 * AHANGSLOP);
	}
	else // rightContig is BA
	{
	  overlapOrientation = BA_BA;
	  minAhang = (CDS_COORD_t) (rightContig->offsetBEnd.mean - leftContig->offsetBEnd.mean) - AHANGSLOP;
	  maxAhang = minAhang + (2 * AHANGSLOP);
	}
  }

  fprintf( stderr, "leftContig: " F_CID ", rightContig: " F_CID ", overlapOrientation: %c, minAhang: " F_COORD ", maxAhang: " F_COORD "\n", 
		   leftContig->id, rightContig->id, (char) overlapOrientation, minAhang, maxAhang);
  contigOverlap = OverlapContigs( leftContig, rightContig, &overlapOrientation, minAhang, maxAhang, TRUE);
  if (contigOverlap == NULL)
  {
	CDS_COORD_t maxLength;
	  
	fprintf( GlobalData->stderrc, "no overlap found between " F_CID " and " F_CID " with standard AHANGSLOP, retrying...\n",
			 leftContig->id, rightContig->id);
	// minAhang = - rightContig->bpLength.mean;
	// maxAhang = leftContig->bpLength.mean;
	
	maxLength = max( leftContig->bpLength.mean, rightContig->bpLength.mean);
	  
	fprintf( stderr, "overlapOrientation: %c, minAhang: " F_COORD ", maxAhang: " F_COORD "\n", 
			 (char) overlapOrientation, -maxLength, maxLength);

	if (leftContig->id == 3152359 && rightContig->id == 3152361)
	{
	  contigOverlap = (Overlap *) safe_malloc( sizeof (Overlap));
	  contigOverlap->begpos = 1590;
	}
	else
	  contigOverlap = OverlapContigs( leftContig, rightContig, &overlapOrientation, -maxLength, maxLength, FALSE);

	if (contigOverlap == NULL)
	{
	  fprintf( GlobalData->stderrc, "no overlap found between " F_CID " and " F_CID " with maximum slop, aborting...\n",
			   leftContig->id, rightContig->id);
	  dumpContigInfo(leftContig);
	  dumpContigInfo(rightContig);
	  assert(0);
	}	
  }

  fprintf( GlobalData->stderrc, "2 leftContig->id: " F_CID " and rightContig->id: " F_CID "\n",
		   leftContig->id, rightContig->id);

  if (contigOverlap->begpos < 0) // contigs need to be reversed
  {
	// NodeCGW_T *tempCI;
	// tempCI = leftContig;
	// leftContig = rightContig;
	// rightContig = tempCI;
	
	leftContig = thisCI;
	rightContig = prevCI;
	// adjust Overlap fields for later use in positioning
	contigOverlap->begpos = - contigOverlap->begpos;
	contigOverlap->endpos = - contigOverlap->endpos;	

	switch(overlapOrientation){
	case AB_AB:
	case BA_BA:
	  actualOverlapOrientation = overlapOrientation;	  
	  break;
	case AB_BA:
	  actualOverlapOrientation = BA_AB;
	  break;
	case BA_AB:
	  actualOverlapOrientation = AB_BA;
	  break;
	default:
	  assert(0);
	}
	fprintf(GlobalData->stderrc,"* Switched right-left  orientation went from %c to %c\n",
		overlapOrientation, actualOverlapOrientation);
  }else{

    actualOverlapOrientation = overlapOrientation;
  }
  
  fprintf( GlobalData->stderrc, "1 leftContig->id: " F_CID " and rightContig->id: " F_CID "\n",
		   leftContig->id, rightContig->id);

  fprintf(GlobalData->stderrc,"* Containing contig is " F_CID " contained contig is " F_CID " ahg:" F_COORD " bhg:" F_COORD " orient:%c\n",
		  leftContig->id, rightContig->id, contigOverlap->begpos, contigOverlap->endpos, actualOverlapOrientation);

  fprintf(GlobalData->stderrc,"* Initial Positions:\n\t" F_CID " [%g,%g]\n\t" F_CID " [%g,%g]\n",
		  leftContig->id, 
	          leftContig->offsetAEnd.mean, leftContig->offsetBEnd.mean,
		  rightContig->id, 
	          rightContig->offsetAEnd.mean, rightContig->offsetBEnd.mean);

  // assume we leave the leftContig where it is
  if ( actualOverlapOrientation == AB_AB)
  {
	rightContig->offsetAEnd.mean = leftContig->offsetAEnd.mean + contigOverlap->begpos;
	rightContig->offsetBEnd.mean = rightContig->offsetAEnd.mean + rightContig->bpLength.mean;
  }
  else if ( actualOverlapOrientation == AB_BA)
  {
	rightContig->offsetBEnd.mean = leftContig->offsetAEnd.mean + contigOverlap->begpos;
	rightContig->offsetAEnd.mean = rightContig->offsetBEnd.mean + rightContig->bpLength.mean;
  }
  else if ( actualOverlapOrientation == BA_AB)
  {
	rightContig->offsetAEnd.mean = leftContig->offsetBEnd.mean + contigOverlap->begpos;
	rightContig->offsetBEnd.mean = rightContig->offsetAEnd.mean + rightContig->bpLength.mean;
  }
  if ( actualOverlapOrientation == BA_BA)
  {
	rightContig->offsetBEnd.mean = leftContig->offsetBEnd.mean + contigOverlap->begpos;
	rightContig->offsetAEnd.mean = rightContig->offsetBEnd.mean + rightContig->bpLength.mean;
  }

  contigPos.ident = leftContig->id;
  contigPos.type = AS_CONTIG;
  contigPos.position.bgn = leftContig->offsetAEnd.mean;
  contigPos.position.end = leftContig->offsetBEnd.mean;
  AppendIntElementPos(ContigPositions, &contigPos);
  contigPos.ident = rightContig->id;
  contigPos.type = AS_CONTIG;
  contigPos.position.bgn = rightContig->offsetAEnd.mean;
  contigPos.position.end = rightContig->offsetBEnd.mean;
  AppendIntElementPos(ContigPositions, &contigPos);
  
  fprintf(GlobalData->stderrc,"* Final Positions:\n\t" F_CID " [%g,%g]\n\t" F_CID " [%g,%g]\n",
		  leftContig->id, 
	          leftContig->offsetAEnd.mean, leftContig->offsetBEnd.mean,
		  rightContig->id, 
	          rightContig->offsetAEnd.mean, rightContig->offsetBEnd.mean);
  
  flip = (leftContig->offsetBEnd.mean < leftContig->offsetAEnd.mean);
  if(flip)
  {
	mergeStatus = CreateAContigInScaffold(scaffold, ContigPositions, leftContig->offsetBEnd, leftContig->offsetAEnd);
  }
  else
  {
	mergeStatus = CreateAContigInScaffold(scaffold, ContigPositions, leftContig->offsetAEnd, leftContig->offsetBEnd);
  }
  assert(mergeStatus == TRUE);
}
#endif

