#if 0
Overlap* OverlapChunksNew( GraphCGW_T *graph,
                           CDS_CID_t cidA, CDS_CID_t cidB,
                           ChunkOrientationType orientation, 
                           CDS_COORD_t minOverlap, CDS_COORD_t maxOverlap,
                           float errorRate,
                           int insertGraphEdges)
{
  /* this function takes two chunks cidA and cidB, their orientation
     and an assumed minimum and maximum overlap for which it checks.
     It then tries to lookup whether the overlap was computed earlier
     or, if not, it computes the overlap and inserts the result in the
     hash table only if there was not such symbol there before.
  */

  int recompute = FALSE;
  int insert    = FALSE;
  int isCanonical;
  // If we init olap the return value is stored
  // here indicating whether the orientation of the two chunks
  // was canonical in Saul's definition or not.

  static int numOverlaps = 0;
  static int numHits = 0;

  ChunkOverlapCheckT *lookup;
  // This pointer holds the return value of LookupCanonicalOverlap

  ChunkOverlapCheckT olap = {0};
  // This is a temporary variable. The return value will be in lookup
  // or the pointer returned by the lookup following the insert

  // make a static to mimic the behavoir of the one returned by DP_Compare
  static Overlap olapReturn;

  isCanonical = InitCanonicalOverlapSpec(cidA,cidB,orientation,&olap.spec);
  // initalize olap with the chunk IDs and their orientation and record 
  // whether the orientation was already canonical or not
  
  lookup = LookupCanonicalOverlap(graph->overlapper,&olap.spec);
  // lookup whether the overlap had already been computed

  
  if( lookup != NULL ){
    olap = *lookup;

    // this indicates that CreateChunkOverlapFromEdge inserted w/o ahg & bhg info
    if (lookup->ahg == 0 && lookup->bhg == 0)  
      {
        recompute = TRUE;
        if(DeleteChunkOverlap(graph->overlapper, &olap) != HASH_SUCCESS)
          assert(0);
        // RemoveComputedOverlapEdge(graph, &olap);
        insert = TRUE;
      }
    else
      {
        if( checkChunkOverlapCheckTLoose(lookup,minOverlap,maxOverlap,errorRate) == FALSE )
          recompute = TRUE;
        insert = FALSE;
      }
  }
  else
    {
      recompute = TRUE;
      insert = insertGraphEdges; 
    }

  // hit rate measuring
  numOverlaps++;
  if (recompute == FALSE)
    numHits++;
  
  if (!(numOverlaps % 10))
    fprintf( stderr, "numOverlaps: %d, numHits: %d\n",
             numOverlaps, numHits);

  if( recompute == TRUE )
    {
      // compute new overlap and store it into the table
      // If it has an overlap add an edge mate to the CG
      // and return TRUE
      olap.computed      = FALSE;
      // olap.overlap       = 0;
      olap.overlap       = (minOverlap + maxOverlap) / 2;
      olap.minOverlap    = minOverlap;
      olap.maxOverlap    = maxOverlap;
      olap.fromCGB       = FALSE;
      olap.cgbMinOverlap = minOverlap;
      olap.cgbMaxOverlap = maxOverlap; 
      olap.errorRate     = errorRate;
      olap.suspicious = FALSE;

      ComputeCanonicalOverlap_new( graph, &olap);
	
      if (insert)
        { // Insert new entries in hashtable, if requested
          //
          int suspicious = olap.suspicious;
          if (olap.suspicious)
            {
              olap.suspicious = FALSE;
              lookup = LookupCanonicalOverlap(graph->overlapper,&olap.spec); // see if it is already in the table
              if(!lookup)
		{
		  fprintf(GlobalData->stderrc,"* Inserting into hash (" F_CID "," F_CID ",%c) " F_COORD "\n",
                          olap.spec.cidA, olap.spec.cidB,
                          olap.spec.orientation, olap.overlap);
                  if(InsertChunkOverlap(graph->overlapper, &olap) !=
                     HASH_SUCCESS)
                    assert(0);
		}
            }
	  else
            {
              if(InsertChunkOverlap(graph->overlapper, &olap) != HASH_SUCCESS)
                assert(0);
            }
          lookup = LookupCanonicalOverlap(graph->overlapper,&olap.spec);
          assert(lookup != NULL);
          // ComputeCanonicalOverlap does not return the olap, so we look it up again.
	  
          olap = *lookup;
          olap.suspicious = suspicious;
        }

      if (olap.overlap && insert)
	{ // Insert all non-zero overlaps, if requested
	  /*
            fprintf(GlobalData->stderrc,"* Inserting into graph (" F_CID "," F_CID ",%c) " F_COORD " ahang: " F_COORD ", bhang: " F_COORD "\n",
            olap.spec.cidA, olap.spec.cidB,
            olap.spec.orientation, olap.overlap,
            olap.ahg, olap.bhg);
	  */
          InsertComputedOverlapEdge(graph, &olap);
        }
    }

  /* Make sure the orientation of the edge we return is IDENTICAL with the spec returned */
  // if the input was not canonical we set the cid's and orientation
  // back to the input value (see als LookupOverlap)
  if( !olap.suspicious  && ! isCanonical )
    {
      NodeCGW_T *a = GetGraphNode(graph, cidA);
      NodeCGW_T *b = GetGraphNode(graph, cidB);
      int swap;
	
      olap.spec.orientation = orientation;
	
      // If this is non-canonical,s wap things back
      olap.spec.cidA = cidA;
      olap.spec.cidB = cidB;
      swap = olap.BContainsA;
      olap.BContainsA = olap.AContainsB;
      olap.AContainsB = swap;
      swap = olap.ahg;
      olap.ahg = olap.bhg;
      olap.bhg = swap;

      // NEW!!!!
	
      if(olap.AContainsB)
	{
	  assert(olap.ahg > 0 && olap.bhg < 0);
	  switch(orientation)
            {
              case AB_AB:
              case AB_BA:
                olap.overlap = b->bpLength.mean + olap.bhg;
                break;
              case BA_AB:
              case BA_BA:
                olap.overlap = b->bpLength.mean - olap.ahg;
                break;
              default:
                assert(0);
                break;
            }
	}
      else if (olap.BContainsA)
	{
	  assert(olap.bhg > 0 && olap.ahg < 0);
	  switch(orientation)
            {
              case AB_AB:
              case AB_BA:
                olap.overlap = a->bpLength.mean - olap.bhg;
                break;
              case BA_AB:
              case BA_BA:
                olap.overlap = a->bpLength.mean + olap.ahg;
                break;
              default:
                assert(0);
                break;
            }
	}
      // END NEW!
    }
  
  CheckScaffoldGraphCache(ScaffoldGraph); // flush the cache if it has gotten too big

  // return olap;
  // convert olap (of type ChunkOverlapCheckT) to olapReturn (of type Overlap)
  if (olap.overlap != 0)
    {
      olapReturn.begpos = olap.ahg;
      olapReturn.endpos = olap.bhg;
      olapReturn.length = olap.overlap;  
      return (&olapReturn);
    }
  else
    return(NULL);
}
#endif
