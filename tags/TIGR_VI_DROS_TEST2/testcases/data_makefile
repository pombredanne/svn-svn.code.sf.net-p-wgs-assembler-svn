#
###########################################################################
#
# This file is part of Celera Assembler, a software program that 
# assembles whole-genome shotgun reads into contigs and scaffolds.
# Copyright (C) 1999-2004, Applera Corporation. All rights reserved.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received (LICENSE.txt) a copy of the GNU General Public 
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
###########################################################################
#
#
# The following is a simplified assembler for small genomes.
# Usage "gmake -f data_makefile"
#
# You must review the following options:

AS_BIN = $(AS_ROOT)/bin
utg_options = -j 5 -A 2
cgw_options = -j 1 -k 5 -r 4 -s 2 -w 1 -T 

#########################################################################
# Let's you decide whether you want it on the screen or in a log file.
#SILENT = 
#SILENT = 2>>$*.log

# Really make these targets
# .PHONY: $(FRAG_STORE)

#.PHONY : $(AS_BIN)/celsim $(AS_BIN)/gatekeeper $(AS_BIN)/overlap $(AS_BIN)/unitigger $(AS_BIN)/cgw $(AS_BIN)/consensus $(AS_BIN)/terminator

# Keep intermediate files
.PRECIOUS : %.sim %.frg %.gkpStore %.inp %.frgStore %.ofg %.ovlStore %.cgb %.cgi %.cgw %.cns %.asm

#########################################################################

default: %.cns

all: default

clean:
	rm -rf $*.*

##################################################

#%.frg:%.sim $(AS_BIN)/celsim
#	$(CELSIM_CMD) $< $(SILENT)

###################################################
OUTPUT_MODE = -P

%.inp %.gkpStore: %.frg $(AS_BIN)/gatekeeper
	$(AS_BIN)/gatekeeper $(OUTPUT_MODE) -N -Q -n $* -f -c -o $*.gkpStore $*.frg $(SILENT)
	cp $*_00001.inp $*.inp $(SILENT)

%.frgStore %.ofg: %.inp $(AS_BIN)/PopulateFragStore
	$(AS_BIN)/PopulateFragStore $(OUTPUT_MODE) -V $*.ofg -f -c -o $*.frgStore $*.inp $(SILENT)

%.ovl: %.frgStore $(AS_BIN)/overlap
	$(AS_BIN)/overlap $(OUTPUT_MODE) -w -r 1- -h 1- -o $*.ovl $*.frgStore $(SILENT)

%.ovlStore: %.ovl $(AS_BIN)/grow-olap-store $(AS_BIN)/correct-script
	$(AS_BIN)/grow-olap-store -cf -o $*.ovlStore $*.ovl $(SILENT) 
	$(AS_BIN)/correct-script -b -s $*.frag_correct_script -B $(AS_BIN) -S $*.ovlStore $*.frgStore ./$*.cor $(SILENT)
	./$*.frag_correct_script $(SILENT)

%.ofgList: %.ofg
	echo "$*.ofg" > $*.ofgList $(SILENT)

%.cgb: %.ofgList %.ovlStore %.frgStore $(AS_BIN)/unitigger
	$(AS_BIN)/unitigger $(OUTPUT_MODE) $(utg_options) -F $*.frgStore -L $*.ofgList -f -c -o $*.fgbStore -I $*.ovlStore $(SILENT)


%.cgi: %.cgb %.frgStore $(AS_BIN)/consensus
	$(AS_BIN)/consensus $(OUTPUT_MODE) -U $*.frgStore $*.cgb $(SILENT)

%.cgw %.cgw_contigs %.cgw_scaffolds : %.cgi %.frgStore %.gkpStore $(AS_BIN)/cgw
	$(AS_BIN)/cgw -c $(OUTPUT_MODE) $(cgw_options) -f $*.frgStore -g $*.gkpStore -o $* $*.cgi $(SILENT) 

%.cns: %.cgw %.cgw_contigs %.cgw_scaffolds %.frgStore $(AS_BIN)/consensus
	cat $*.cgw $*.cgw_contigs $*.cgw_scaffolds > $*.cgw_total $(SILENT) 
	$(AS_BIN)/consensus $(OUTPUT_MODE) $*.frgStore $*.cgw_total $(SILENT)
	rm -f $*.cgw_total $(SILENT)

%.asm: %.cns %.gkpStore %.frgStore $(AS_BIN)/terminator
	$(AS_BIN)/terminator -u $(OUTPUT_MODE) -g $*.gkpStore -f $*.frgStore -i $*.cns -o $*.asm -m $*.map $(SILENT)

$*.mini_cgb : $*.ofg $*.ovl
	$(AS_BIN)/miniunitigger_test $*.ofg $*.ovl 1> $*.mini_cgb $(SILENT)
