#
###########################################################################
#
# This file is part of Celera Assembler, a software program that 
# assembles whole-genome shotgun reads into contigs and scaffolds.
# Copyright (C) 1999-2004, Applera Corporation. All rights reserved.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received (LICENSE.txt) a copy of the GNU General Public 
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
###########################################################################
#
# Makefile for alignment library

LOCAL_WORK = $(shell cd ../../; pwd)

LIB_SOURCES = AS_ALN_dpaligner.c AS_ALN_qvaligner.c \
	      AS_ALN_loverlapper.c AS_ALN_pieceOlap.c AS_ALN_forcns.c \
	      CA_ALN_local.c CA_ALN_overlap.c CA_ALN_scafcomp.c\
	      AS_ALN_boxfilling.c
	      
LIB_OBJECTS = $(LIB_SOURCES:.c=.o)

TEST1_SOURCES = AS_ALN_testdriver1.c
TEST1_OBJECTS = $(TEST1_SOURCES:.c=.o)

TEST4_SOURCES = AS_ALN_testdriver4.c
TEST4_OBJECTS = $(TEST4_SOURCES:.c=.o)

TEST5_SOURCES = AS_ALN_testdriver5.c
TEST5_OBJECTS = $(TEST5_SOURCES:.c=.o)

RC_SOURCES = AS_ALN_revcompl.c
RC_OBJECTS = $(RC_SOURCES:.c=.o)

BOXFILL_SOURCES = AS_ALN_boxfiller.c
BOXFILL_OBJECTS = $(BOXFILL_SOURCES:.c=.o)

SOURCES = $(TEST1_SOURCES) $(TEST2_SOURCES)\
	  $(TEST4_SOURCES) $(TEST5_SOURCES)\
	  $(BOXFILL_SOURCES)\
          $(LIB_SOURCES) $(RC_SOURCES) 
OBJECTS = $(SOURCES:.c=.o)

LIBRARIES = libAS_ALN.a libCA.a

PROGS = AS_ALN_testdriver1 AS_ALN_testdriver4\
	AS_ALN_testdriver5 AS_ALN_revcompl\
	AS_ALN_boxfiller

include $(LOCAL_WORK)/src/c_make.as


all: $(OBJECTS) $(LIBRARIES) $(PROGS)
	@test -n nop

libAS_ALN.a: $(LIB_OBJECTS)

libCA.a: $(LIB_OBJECTS)

AS_ALN_testdriver1: $(TEST1_OBJECTS) $(LIBRARIES) libCA.a

AS_ALN_testdriver4: $(TEST4_OBJECTS) $(LIBRARIES) libCA.a

AS_ALN_testdriver5: $(TEST5_OBJECTS) $(LIBRARIES) libCA.a

AS_ALN_boxfiller: $(BOXFILL_OBJECTS) $(LIBRARIES) libCA.a

AS_ALN_revcompl:  $(RC_OBJECTS) $(LIBRARIES) libAS_UTL.a

AS_ALN_tester:
	time AS_ALN_testdriver1 <dataset1 >out1
	time AS_ALN_testdriver1 <dataset2 >out2
	time AS_ALN_testdriver4 <dataset4 >out4
	time AS_ALN_testdriver5 <dataset5 >out5

package:
	shar *.c *.h >package
