
I came from AS_ALN_scafcomp.c.  Nobody used me, so I ran away and hid
here!




Segment *Compare_Scaffold(Segment *seglist, int numsegs,
                                 Scaffold *AF, Scaffold *BF, int *best)
{ static int *vector = NULL;
  static int  maxvec = -1;

  int      nacs, nbcs;
  Segment *s, *r, *t;
  int     *whom, *trace;
  int lastrow, lastcol, lastadd = 0;
  int segno;
  int j, max, opt;

  nacs = AF->num_gaps+1;
  nbcs = BF->num_gaps+1;

  if (numsegs + 2*nbcs > maxvec)
    { maxvec = (int)(1.2*(numsegs + 2*nbcs) + 500);
      vector = (int *) realloc(vector,sizeof(int)*maxvec);
      if (vector == NULL)
        { fprintf(stderr,"Out of memory allocating DP array\n");
          exit (1);
        }
    }
  whom = vector + nbcs;
  trace = whom + nbcs;
  
  for (j = 0; j <= BF->num_gaps; j++)
    { vector[j] = 0;
      whom[j] = -1;
    }

  lastrow = nacs+1;
  lastcol = nbcs-1;
  segno = 0;
  r = NULL;
  for (s = seglist; s != NULL; s = t)
    { if (s->a_contig != lastrow || s->b_contig != lastcol)
        { for (j = lastcol-1; j >= s->b_contig; j--)
            if (vector[j+1] > vector[j])
              { vector[j] = vector[j+1];
                whom[j] = whom[j+1];
              }
          lastcol = s->b_contig;
          lastrow = s->a_contig;
          lastadd = s->overlap->length;
          vector[lastcol] = lastadd + vector[lastcol]; 
          trace[segno] = whom[lastcol];
          whom[lastcol] = segno;
        }
      else
        { if (s->overlap->length > lastadd)
            { vector[lastcol] += (s->overlap->length - lastadd);
              lastadd = s->overlap->length;
              trace[segno] = trace[whom[lastcol]];
              whom[lastcol] = segno;
            }
          else
            trace[segno] = -1;
        }

      segno += 1;
      t = s->next;
      s->next = r;
      r = s;

#ifdef DEBUG_COMPARE
      fprintf(stderr,"  (a,b,v) = (%d,%d,%d)\n     ",lastrow,lastcol,lastadd);
      for (j = 0; j <= BF->num_gaps; j++)
        fprintf(stderr," %d(%d)",vector[j],whom[j]);
      fprintf(stderr,"\n");
#endif
    }
  seglist = r;

#ifdef DEBUG_COMPARE
  fprintf(stderr,"\nTraceback:\n");
  for (j = 0; j < numsegs; j++)
    fprintf(stderr," %3d: -> %d\n",j,trace[j]);
#endif

  max = vector[0];
  opt = whom[0];
  for (j = 1; j <= BF->num_gaps; j++)
    if (vector[j] > max)
      { max = vector[j];
        opt = whom[j];
      }

  segno = numsegs-1;
  r = NULL;
  for (s = seglist; s != NULL; s = t)
    { t = s->next;
      if (segno == opt)
        { s->next = r;
          r = s;
          opt = trace[opt];
        }
      else
        { free(s->overlap);
          free(s);
        }
      segno -= 1;
    }
  seglist = r;

  r = NULL;
  for (s = seglist; s != NULL; s = t)
    { t = s->next;
      s->next = r;
      r = s;
    }
  seglist = r;

  *best = max;
  return (seglist);
}




Scaffold_Overlap *Compare_Scaffolds(Scaffold_List *AS, Scaffold_List *BS)
{ int i, ip;
  int j, jp;
  int tab;
  Local_Pool       *pool;
  Scaffold_Overlap *list, *list2, *fing;
  Scaffold_List    *a, *b;

  pool = Find_All_Locals(AS,BS);
  
  i  = 0;
  ip = 0;
  list = fing = NULL;
  tab  = 0;
  for (a = AS; a != NULL; a = a->next)
    { j  = 0;
      jp = 0;
      for (b = BS; b != NULL; b = b->next)
        { Segment *seglist, *scflist = NULL;
          int      vw, score, confirmed, numsegs;
	  int first=1;

          seglist = Find_All_Overlaps(a->scaffold,b->scaffold,pool,
                                      i,j,ip,jp,&tab,&numsegs,0);

          for (vw = 0; VarWindows[vw] != 0; vw++)
            { 
#ifdef AHANG_BAND_TEST
	      scflist = Align_Scaffold(seglist,numsegs,VarWindows[vw],
                                       a->scaffold,b->scaffold,&score,
				       -b->scaffold->length,
				       a->scaffold->length);
#else
	      scflist = Align_Scaffold(seglist,numsegs,VarWindows[vw],
                                       a->scaffold,b->scaffold,&score);
#endif

	      if(score==0&&scflist==NULL&&first){
		fprintf(stderr, "It's possible to interleave scaffolds %d and %d with no overlapping contigs at %d sigma\n",i,j,VarWindows[vw]);
		first=0;
	      }


              if (scflist != NULL) break;
            }
          if (scflist != NULL)
            { seglist = scflist;
              confirmed = VarWindows[vw];
            }
          else
            { seglist = Compare_Scaffold(seglist,numsegs,
                                         a->scaffold,b->scaffold,&score);
              confirmed = 0;
            }
          if (seglist != NULL)
            { Scaffold_Overlap *overlap;

              overlap = Analyze_Overlap(a->scaffold,b->scaffold,
                                        seglist,score,0);
              if (list == NULL)
                list = overlap;
              else
                fing->next = overlap;
              fing = overlap;

              fing->asnum = i;
              fing->bsnum = j;
              fing->abase = ip;
              fing->bbase = jp;
              fing->firm  = confirmed;

              { Segment *s;
                for (s = seglist; s != NULL; s = s->next)
                  { s->a_contig += ip;
                    s->b_contig += jp;
                  }
              }
            }

          j  += 1;
          jp += (b->scaffold->num_gaps+1);
        }
      i  += 1;
      ip += (a->scaffold->num_gaps+1);
    }

  if (list != NULL)
    fing->next = NULL;

  for (b = BS; b != NULL; b = b->next)
    Complement_Scaffold(b->scaffold);

  i  = 0;
  ip = 0;
  list2 = fing;
  tab   = 0;
  for (a = AS; a != NULL; a = a->next)
    { j  = 0;
      jp = 0;
      for (b = BS; b != NULL; b = b->next)
        { Segment *seglist, *scflist = NULL;
          int      vw, score, confirmed, numsegs;
	  int first=1;

          seglist = Find_All_Overlaps(a->scaffold,b->scaffold,pool,
                                      i,j,ip,jp,&tab,&numsegs,1);

          for (vw = 0; VarWindows[vw] != 0; vw++)
            { 

#ifdef TRY_NEW_COMPARATOR
#ifdef AHANG_BAND_TEST
	      scflist = Align_Scaffold_ala_Aaron(seglist,numsegs,VarWindows[vw],
                                       a->scaffold,b->scaffold,&score,
				       -b->scaffold->length,
				       a->scaffold->length);
#else
	      scflist = Align_Scaffold_ala_Aaron(seglist,numsegs,VarWindows[vw],
                                       a->scaffold,b->scaffold,&score);
#endif
#else
#ifdef AHANG_BAND_TEST
	      scflist = Align_Scaffold(seglist,numsegs,VarWindows[vw],
                                       a->scaffold,b->scaffold,&score,
				       -b->scaffold->length,
				       a->scaffold->length);
#else
	      scflist = Align_Scaffold(seglist,numsegs,VarWindows[vw],
                                       a->scaffold,b->scaffold,&score);
#endif
#endif

	      if(score==0&&scflist==NULL&&first){
		fprintf(stderr, "It's possible to interleave scaffolds %d and rev_comp(%d) with no overlapping contigs at %d sigma\n",i,j,VarWindows[vw]);
		first=0;
	      }

              if (scflist != NULL) break;
            }
          if (scflist != NULL)
            { seglist = scflist;
              confirmed = VarWindows[vw];
            }
          else
            { seglist = Compare_Scaffold(seglist,numsegs,
                                         a->scaffold,b->scaffold,&score);
              confirmed = 0;
            }
          if (seglist != NULL)
            { Scaffold_Overlap *overlap;

              overlap = Analyze_Overlap(a->scaffold,b->scaffold,
                                        seglist,score,1);
              if (list == NULL)
                list = overlap;
              else
                fing->next = overlap;
              fing = overlap;

              fing->asnum = i;
              fing->bsnum = j;
              fing->abase = ip;
              fing->bbase = jp;
              fing->firm  = confirmed;

              { Segment *s;
                for (s = seglist; s != NULL; s = s->next)
                  { s->a_contig += ip;
                    s->b_contig  = jp + b->scaffold->num_gaps - s->b_contig;
                  }
              }
            }

          j  += 1;
          jp += (b->scaffold->num_gaps+1);
        }
      i  += 1;
      ip += (a->scaffold->num_gaps+1);
    }

  if (list != NULL)
    fing->next = NULL;

  for (b = BS; b != NULL; b = b->next)
    Complement_Scaffold(b->scaffold);

  free(pool->address);
  free(pool->locals);

  if (list2 != NULL && list2->next != NULL)
    { Scaffold_Overlap *x, *y, *n;

      y = list2 = list2->next;
      for (x = list; x != list2; x = x->next)
        { while (y != NULL && (y->asnum < x->asnum || (y->asnum == x->asnum &&
                                                       y->bsnum  < x->bsnum)) )
            y = y->next;
          if (y != NULL && y->asnum == x->asnum && y->bsnum == x->bsnum)
            { int single_rev;
	      Local_Overlap *o;
              single_rev = 0;

	      /* revisions by ALH 12/14/00: 
		 single_rev must consider possibility of y
		 being a single reversed segment as well as x being so;
		 originally, a containment test was applied as well, but
		 this isn't really logically required in order for an
		 overlap consisting of a single reversed segment to be
		 rejected.

		 The logic is ... if an overlap in one direction consists
		 of a single reversed segment, then the overlap in the
		 other orientation had the potential to consist of the
		 very same segment (in forward orientation).  Either that
		 is the case (in which case the original, reversed overlap
		 should be rejected), or the second overlap contains the
		 single segment of the first overlap (in which case, the
		 second overlap was considered to be superior to the single
		 segment alone, and the first, reversed overlap should be
		 rejected) OR the forward overlap doesn't contain the reversed
		 segment BUT was chosen OVER any overlap containing that
		 segment (and thus, we should once again reject the overlap
		 consisting of the single, reversed segment).

	      */

	      o = x->seglist->overlap;
	      if (x->seglist->next == NULL&& o->num_pieces == 1 && o->chain[0].reversed)
		{ single_rev = 1; }
	      else 
		{ o = y->seglist->overlap;
		  if(y->seglist->next == NULL&&o->num_pieces == 1 && o->chain[0].reversed)
		    { single_rev = 2; }
		}

	      if(single_rev>0){
		if(single_rev==1){
		  x->abase = -1;
		} else {
		  y->abase = -1;
		}
	      } else {
		if (y->erate < x->erate ){
		  x->abase = -1;
		} else {

		  /* Revision, ALH, 1/5/01:
		     Additional testing in the case of ties in erate;
		     I came across a case where the forward and reverse
		     overlaps consisted of the same sets of segments,
		     but the statistics were quite different, since all the
		     segments were forward in one overlap and reverse
		     in the other.  Obviously, we prefer that version of
		     the overlap which agrees with the orientation of the
		     segments--and this seems to be the one with the better
		     statistics (better deltas, better confirmed level, etc).
		     In the one known case of this, any of the measures
		     other than erate would have given the right result;
		     I chose to test on D_delta relatively arbitrarily.
		  */
		  if (y->D_delta < x->D_delta) {
		    x->abase = -1;
		  } else {
		    y->abase = -1;
		  }
		}
	      }

            }


        }

      y = NULL;
      for (x = list; x != NULL; x = n)
        { n = x->next;
          if (x->abase < 0)
            { if (y == NULL)
                list = n;
              else
                y->next = n;
              Free_Segments_ScafComp(x->seglist);
              free(x->overlap);
              free(x);
            }
          else
            y = x;
        }
    }

  return (list);
}


/* These are the lines that the AnnoGraph edge_command script parses
   to appropriately highlight the celamy files for display 
   Note that the contig ids are incremented, since the celamy
   view starts numbering at 1.                                 

   REPORT a_ctg_id [low,high] ib_ctg_id [low,high]                  
*/

void Print_Anno_Info(Scaffold_Overlap *sovl)
{ printf("\nAnnograp Info:\n\n");
  for (; sovl != NULL; sovl = sovl->next)
    { Segment  *s;

      for (s = sovl->seglist; s != NULL; s = s->next)
        printf("REPORT %d [%d,%d] %d [%d,%d]\n",
               s->a_contig+1,s->alow,s->ahgh,s->b_contig+1,s->blow,s->bhgh);
    }
}

void Free_Scaffold_Overlaps(Scaffold_Overlap *SO)
{ Scaffold_Overlap *s, *t;

  for (s = SO; s != NULL; s = t)
    { t = s->next;
      Free_Segments_ScafComp(s->seglist);
      free(s->overlap);
      free(s);
    }
}


