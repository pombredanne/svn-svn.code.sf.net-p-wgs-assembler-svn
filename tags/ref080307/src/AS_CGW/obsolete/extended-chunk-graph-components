
// Compute connected components, and label Unique Chunks
void  ExtendedChunkGraphComponents(ChunkGraphT *graph){
  int numChunks;
  CDS_CID_t startChunk;
  UFDataT *UFData;
  int *chunkMap;
  CDS_CID_t ic;
  int set = 0;
  int numComponents;

  if(graph->type == FRAGMENT_CHUNK_GRAPH){
    startChunk = FIRST_CHUNK_INDEX;
    numChunks = graph->numUniqueChunks;
  }else{
    startChunk = 0;
    numChunks = GetNumChunkTs(graph->Chunks);
  }

  UFData = UFCreateSets(numChunks);
  chunkMap = (int *)safe_malloc(sizeof(int) * GetNumChunkTs(graph->Chunks));

  for(ic = startChunk; ic < GetNumChunkTs(graph->Chunks); ic++){
    ChunkT *chunk = GetChunkT(graph->Chunks, ic);
    UFSetT *chunkSet;

    if(!chunk->isUnique){
      chunkMap[ic] = -1;            // Unmapped to a set
      continue;
    }
    chunkSet = UFGetSet(UFData, set);
    assert(chunkSet);
    chunkMap[ic] = set++;           // This maps chunkIDs to setIDs
    chunkSet->data = (void *)chunk; // This maps the set to a chunk
  }


  for(ic = startChunk; ic < GetNumChunkTs(graph->Chunks); ic++){
    ChunkT *chunk = GetChunkT(graph->Chunks, ic);
    EdgeMateTIterator edgeMates;
    EdgeMateT *edge;
    int setA = chunkMap[ic];
    
    if(!chunk->isUnique){
      continue;
    }
    assert(setA >= 0);


    InitEdgeMateTIterator(graph, ic, TRUE /* confirmed */, TRUE /* unique */, ALL_END, FALSE, &edgeMates);
    while(edge = NextEdgeMateTIterator(&edgeMates)){
      int  setB;

      // Graph has two edges for every relation -- only do half.
      if(edge->cidA > edge->cidB)
	continue;

      setB = chunkMap[edge->cidB];
      assert(setB >= 0);

      UFUnion(UFData, setA, setB);

    }
  }

  numComponents = UFRenumberSets(UFData);
  fprintf(GlobalData->stderrc," Extended Chunk Graph has %d components\n",
	  numComponents);

  for(set = 0; set < UFData->numSets; set++){
    UFSetT *chunkSet = UFGetSet(UFData, set);
    ChunkT *chunk = (ChunkT *)chunkSet->data;

    chunk->setID = chunkSet->component;
#ifdef DEBUG_DIAG
    fprintf(GlobalData->stderrc,"* Chunk " F_CID " has component " F_CID "\n",
	    chunk->cid, chunk->setID);
#endif
  }
  UFFreeSets(UFData);
  safe_free(chunkMap);
}
