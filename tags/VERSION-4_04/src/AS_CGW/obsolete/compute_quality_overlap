/************************************************************************/
// Returns FALSE if none found
// Returns TRUE  if found and sets quality bit if necessary

int ComputeQualityOverlap(GraphCGW_T *graph, 
			  EdgeCGW_T *edge,
			  ChunkOrientationType orientation,
			  ChunkOverlapCheckT *olap, QualityFuncT qfunc,
			  float* quality, FILE* fp){
  int isCanonical;
  ChunkOverlapSpecT spec;
  ChunkOverlapCheckT lookup = {0};
  OverlapMesg* omesg = NULL;

  CDS_CID_t cidA = edge->idA;
  CDS_CID_t cidB = edge->idB;


  if(fp == NULL)
    fp = GlobalData->stderrc;

  isCanonical = InitCanonicalOverlapSpec(cidA, cidB, orientation, &spec);

  FillChunkOverlapWithEdge(edge,&lookup);
  *olap = lookup;

  
  // if we are here we have to recompute the alignment 
  // and invoke the appropriate
  // quality function
  switch( qfunc ){
    case BAYESIAN :
      {
        InternalFragMesg IFG1,IFG2;
        static OverlapMesg omesgBuffer;
        CDS_COORD_t length;
        float normalQuality;

        // Compute the DP_Compare alignment and fill the IFG1 and IFG2 with the overlapping
        // pieces of the two chunks (resp. the quality values

        omesg = ComputeCanonicalOverlapWithTrace(graph, &lookup, &IFG1, &IFG2, fp, FALSE);

        if( omesg == NULL )
          return FALSE;

        omesgBuffer.aifrag       = omesg->aifrag;
        omesgBuffer.bifrag       = omesg->bifrag;
        omesgBuffer.ahg          = omesg->ahg;
        omesgBuffer.bhg          = omesg->bhg;
        omesgBuffer.orientation  = omesg->orientation;
        omesgBuffer.overlap_type = omesg->overlap_type;
        omesgBuffer.quality      = omesg->quality;
        omesgBuffer.min_offset   = omesg->min_offset;
        omesgBuffer.max_offset   = omesg->max_offset;
        omesgBuffer.polymorph_ct = omesg->polymorph_ct;

        omesgBuffer.delta = (signed char*) safe_calloc(sizeof(signed char),strlen((char*)omesg->delta)+1);
        strcpy((char*)omesgBuffer.delta,(char*)omesg->delta);
        // compute the quality value and
        // set the quality and the bit in the ChunkOverlapCheckT
        // we do this with and without quality realigning
        compute_bayesian_quality(&IFG1,&IFG2,&omesgBuffer,0,&length,fp);
        // Realign it using quality values
        normalQuality = omesgBuffer.quality;
#if GREEDYDEBUG > 1
        fprintf(fp,"Quality between " F_CID " and " F_CID " WITHOUT QV Realigning = %f\n",
                cidA,cidB,normalQuality);
#endif   

        // Postponed. First integrate greedy walking
        // Then see whether QV Realigning makes it better.
        /*
          omesg = QV_ReAligner_AS(&IFG1,&IFG2,&omesgBuffer);
          compute_bayesian_quality(&IFG1,&IFG2,omesg,0,&length,fp);
          QVQuality = omesg->quality;
          fprintf(fp,"Quality WITH QV Realigning = %f\n",QVQuality);
          if( QVQuality != normalQuality)
	  fprintf(fp,"CHANGED quality values\n");
        */

        *quality = omesgBuffer.quality;
        lookup.quality = *quality;
        lookup.hasBayesianQuality = TRUE;
        *olap = lookup;
      }
  }
  
  // We check whether DP_compare has changed cidA and cidB
  // with respect to the original cidA cidB. Hence we have to
  // pay attention, whether Saul has switched the ids to make
  // them canonical
  // If so, we allow only a sloppy positiv ahang
  // otherwise DP_Compare has computed a different
  // overlap. Hence we return FALSE


  
  if(  lookup.suspicious )
    {
      fprintf(fp,"SUSPICIOUS OVERLAP omesg cidA = " F_CID " , canOlap " F_CID " cidA = " F_CID ", omesg Orientation = %c\n",
              omesg->aifrag,olap->spec.cidA,cidA,omesg->orientation);
      return FALSE;
    }
  else
    fprintf(fp,"OVERLAP omesg cidA = " F_CID " , canOlap " F_CID " cidA = " F_CID ", omesg Orientation = %c\n",
            omesg->aifrag,olap->spec.cidA,cidA,omesg->orientation);


  if(isCanonical){  // we're done
    return TRUE;
  }
  // Otherwise we have to fix up the retrieved canonical overlap for the non-canonical query
  //

  olap->spec.orientation = orientation;
  olap->spec.cidA = cidA;
  olap->spec.cidB = cidB;
  {
    int swap;
    swap = olap->BContainsA;
    olap->BContainsA = olap->AContainsB;
    olap->AContainsB = swap;
  }

  return TRUE;

}

