
/**************************************************************************
 * This file is part of Celera Assembler, a software program that 
 * assembles whole-genome shotgun reads into contigs and scaffolds.
 * Copyright (C) 1999-2004, Applera Corporation. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received (LICENSE.txt) a copy of the GNU General Public 
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *************************************************************************/
/*********************************************************************
   CVS_ID:  $Id: AS_TER_terminator.h,v 1.1.1.1 2004-04-14 13:53:43 catmandew Exp $
 *********************************************************************/
#ifndef AS_TER_TERMINATOR_H
#define AS_TER_TERMINATOR_H

/* FOR THE TIME BEING THERE ARE NO EXTERNAL UID for ISN (they are not passed)
   to the CNS file. Uncomment this in order to check */

// #define CHECK_ISN

#define AS_TER_SNAP_OUT_EXT ".asm"
#define AS_TER_SNAP_MAP_EXT ".map"
#define AS_TER_SNAP_IN_EXT  ".cns"
#define AS_TER_DST_IN_EXT  "/db.dst"

#endif








