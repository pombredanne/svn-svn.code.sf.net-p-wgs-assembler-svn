#!/usr/bin/perl -w
#
###########################################################################
#
# This file is part of Celera Assembler, a software program that 
# assembles whole-genome shotgun reads into contigs and scaffolds.
# Copyright (C) 1999-2004, Applera Corporation. All rights reserved.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received (LICENSE.txt) a copy of the GNU General Public 
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
###########################################################################
#

# The program will generate clone middle celamy file for intra- and inter- scaffold mates.
# The program will also calculate the badclone intervals and check mate statistics. The
# Celamy file drawing is based on asm file value, that is, a negative gap is shown as
# negative.  However, when calculating scaffold length, gaps < 20 bp will be counted as 20.

use Carp;
use DB_File;
use DBI;
use strict 'vars';
use vars qw( $opt_b $opt_s $opt_d $opt_h $opt_H);  #  Whether or not output *.badclones and *.scflinks files
use vars qw( $opt_m $opt_i $opt_S $opt_a $opt_x);  #  Whether or not output mate stats and cloneLink.analysis files
use vars qw( $opt_t $opt_g $opt_f );
use Getopt::Std;

my $USAGE = "USAGE: $0  [-bgms] [-S species] [-i scf_uid] [-a std_cutoff] [-d DB] [-t type] [-x index] *.asm
-b    If the option is set, then there is no output for *.badclones file.
-s    If the option is set, then there is no output for *.scflinks file.
-m    If the option is set, then there is no output for cloneLink.analysis file.
-i    If the option is set, then the program will use index to generate celamy files
      for a single scaffold.  The index file should be *.asm.index.
-d    If the option is set, the program will use DB to get the assembly info.  The default is PRD_HUM.
-a    The STD cutoff, the default is 3 STD
-S    The species (currently supports human and mouse, default is human)
-t    The fragment type.  R: CRA reads,  X: external reads
-g    If the option is set, there will be output file listing all missing mate fragments.
-x    The asm index file.  This option will overwrite -i's index.
-f    The SCF message was specified by the file, not in asm file.  This option is added
      for validating scaffolding errors.  It assumed that -i option is set too.
-h    Print the usage.
-H    Print the usage.
";

BEGIN{
    $ENV{ORACLE_HOME} ="/usr/local/src/oracle/product/8.1.6";
    $ENV{LD_LIBRARY_PATH} ="/usr/local/src/oracle/product/8.1.6/lib";
}

getopts( 'bsghHmd:a:i:S:t:x:f:' );

if ( $opt_h || $opt_H || $#ARGV < 0) {
    print $USAGE;
    exit(0);
}

if ( ! $opt_t ) {
    $opt_t = ".";
}

## Set up the DB connection
my $username = "laizn";
my $passwd = "welcomE";
my $db = "PRD_HUM";
my $dbh;
if ( $opt_d ) {
    $db = $opt_d;
    $dbh = DBI->connect("dbi:Oracle:$db", $username, $passwd,
                      { PrintError => 1, RaiseError => 1  });
}

my $BAC_SIZE = 79999;   # any clone with mean > 79999 will be considered as BAC, which
			# has the range of 70-250 kb.

my $asmFile = shift;
my $scaffold = $asmFile;

if ( $opt_d ) {
    $asmFile .= ".asm";
}

if ( -z $asmFile ) {   # If the file has size of zero.
    exit(0);
}

my $cel1 = $asmFile;

$cel1 =~ s/.*\///;
my $cel2 = $cel1;
my $bad = $cel1;
my $isms = $cel1;   # for inter-scaffold mate statistics

$cel1 =~ s/asm/cel1/;
$cel2 =~ s/asm/cel2/;
$isms =~ s/asm/scflinks/;
$bad =~ s/asm/badclones/;

my $matestat = 0;
if ( ! $opt_m ) {
    $matestat = "cloneLink.analysis";
}

my @temp = split( /\//, $asmFile );
my $comp;

if ( $temp[$#temp] =~ /(.*)\.asm/ ) {
    $comp = $1;
}

if ( $opt_i ) {
    $comp = $opt_i;
    $scaffold = $opt_i;
    $cel1 = "$comp.cel1";
    $cel2 = "$comp.cel2";
    $isms = "$comp.scflinks";
    $bad = "$comp.badclones";
}

### Global variables needed.
my $fragments;
my $positions;
my $utg_frg_pos;
my %utg_len;
my $bactig_positions;
my $unitig_positions;

my %frg_con;
my %frg_utg;

my %scf_con;
my %scf_iid;
my %scf_len;
my %scf_start;

my %utg_src;   # The sources for unitigs
my %utg_con;  # key: utg   value: array of [ctg, start, end]
my %surrogates;  # For Rock, Stone, Pebble, and Unplaced singles

my %con_ori;
my %con_len;
my %con_start;
my %con_scf;
my %con_utg;  # A hash of array.  Key: contig_id  Value: array of unitig_IDs
my %con_iid;  # key: contig_UID   value:  contig_IID
my %con_bactig;
my $con_pos = 0;

my $std_cutoff = 3;  # Determines how many STD it allows to determine a good mate pair
if ( $opt_a ) {
    $std_cutoff = $opt_a;
}
my $DataStorePath = "/prod/overlay/IR3/assemblies/lai_DataStore"; 
### End of global variables

my $out;
open( CEL1, ">$cel1" ) || die "Can't open file $cel1 to write!";
open( CEL2, ">$cel2" ) || die "Can't open file $cel2 to write!";
print_header( "CEL1" );
print_header( "CEL2" );

# Determine which data store to use, which species.
if ( $opt_S ) {
    if ( $opt_S eq "mouse" ) {
	$DataStorePath = "/prod/grande/lai_grande/DataStore";
    } elsif ( $opt_S eq "human" ) {
	$DataStorePath = "/prod/overlay/IR3/assemblies/lai_DataStore";
    } else {
	$DataStorePath = $opt_S;
    }
}

my ($total_mates, $good_mates, $inter_scf_mates, $sameori_mates, $outie_mates, $wrong_size_mates, $missing_mates)  = (0, 0, 0, 0, 0, 0, 0);

my $temp_dir = "/home/dewim/tmp";
if ( $opt_i ) {
    my @cons = ();
    if ( $opt_f ) {
	@cons = get_scf_cons( $opt_f );
    } else {
	@cons = get_scf_cons( $asmFile, $comp );
    }
    open(TEMP, ">$temp_dir/$comp.temp" );
    foreach( @cons ) {
	print TEMP get_value( $_, $asmFile );
    }
    if ( $opt_f ) {
	`cat $opt_f >> $temp_dir/$comp.temp`;
    } else {
	print TEMP get_value( $comp, $asmFile );
    }
    close( TEMP );

    process_asm( "$temp_dir/$comp.temp" );

    open(TEMP, ">$temp_dir/$comp.temp" );
    foreach( @cons ) {
	foreach( @{ $con_utg{ $_ } } ) {
	    my $utg = $_;
	    if ( /(\d+)surro/ ) {
	       $utg = $1;
	    }
	    print TEMP get_value( $utg, $asmFile );
	}
    }
    close( TEMP );

    process_asm( "$temp_dir/$comp.temp" );

    unlink( "$temp_dir/$comp.temp" );
} elsif ( $opt_d ) {
    set_scf_frg( $scaffold, "0" );
    print_ctg( $scaffold, "CEL1" );
    print_ctg( $scaffold, "CEL2" );
    foreach( @{ $scf_con{ $scaffold } } ) {
	print_utg( $_, $_, "CEL1" );
	print_utg( $_, $_, "CEL2" );
    }
} else {
    process_asm( $asmFile );
}

## Output the missing mates
if ( $opt_g ) {
    open( MIS, ">$scaffold.missingMates" ) || die "Can't open $scaffold.missingMates for writing.";
}

my %scf_links;  # Key: "scf1-scf2"  Value: mate count

getIntervals( $fragments, $positions );

if ( !$opt_s ) {
    open( ISMS, ">$isms" ) || die "Can't open file $isms to write!";
    foreach ( keys %scf_links ) {
	my @a = split( /-/ );
	print ISMS "$scf_iid{ $a[0] }\t$scf_len{ $a[0] }\t$scf_iid{ $a[1] }\t$scf_len{ $a[1] }\t$scf_links{ $_ }\t$a[0]\t$a[1]\n";
    }
    close( ISMS );
}

if ( $matestat ) {
    open( MS, ">>$matestat" );
    print MS "$comp\t$total_mates\t$good_mates\t$wrong_size_mates\t$outie_mates\t$sameori_mates\t$inter_scf_mates\t$missing_mates\n";
    close( MS );
}

if ( $opt_g ) {
    close( MIS );
}

close( CEL1 );
close( CEL2 );

$dbh->disconnect() if ( $opt_d );

############################################################################
## End of Main
############################################################################
############################################################################

# The subroutine will process asm file and gets necessary information.
sub process_asm {
    my $asmFile = shift;

    my $cco_flag = 0;
    my $read_flag = 0;
    my $bactig_flag = 0;    # under CCO->MPS->typ:B
    my $scf_flag = 0;
    my $utg_flag = 0;
    my $ups_flag = 0;
    my $utg_acc;
    my $first_flag = 0;
    my $surrogate_flag;
    my $utg_type;
    my $con_count = 1;
    my $count = 0;
    my $acc;
    my $fragment;
    my $first_first_scf = 0;
    my $scf_length;
    my $scf = 0;
    my ($contigid, $prev_con, $ori_val);
    my %ori = (
	    N => 1,
	    I => -1,
	    O => -1,
	    A => 1,
	    );
    my %first_ori = (
	    N => 1,
	    I => 1,
	    O => -1,
	    A => -1,
	    );

    open( ASM, "$asmFile" ) || die "Can't open file $asmFile";
    while( <ASM> ) {
	if ( /^\{UTG$/ ) {
	    $utg_flag = 1;
	    $count = 1;
	} elsif ( $utg_flag ) {
	    if ( /^acc:\((\d+),(\d+)\)$/ ) {
		$utg_acc = $1;
	    } elsif ( /^len:(\d+)$/ ) {
		$utg_len{ $utg_acc } = $1;
	    } elsif ( /^FROM ICM (\d+) in BAC (\d+)$/ ) {
		$utg_src{ $utg_acc } = "BAC$2ICM$1";
		$utg_flag = 0;
	    } elsif ( /^typ:$opt_t$/ ) {
		$read_flag = 1;
	    } elsif ( $read_flag ) {
		if ( /^mid:(\d+)$/ ) {
		    $fragment = $1;
		    $frg_utg{ $fragment } = $utg_acc;
		} elsif ( /^pos:(\d+),(\d+)$/ ) {
		    my $start = $1;
		    my $end = $2;
		    my $ori = 1;
		    if ( $start > $end ) {
			$ori = -1;
			($start, $end) = ($end, $start);
		    }
		    $utg_frg_pos->{ $fragment } = [$start, $end, $ori];
		    $read_flag = 0;
		}
	    } elsif ( /^\{/ ) {
		$count++;
	    } elsif ( /^\}$/ ) {
		$count--;
		$utg_flag = 0 if ( $count == 0 );
	    }
	} elsif ( /^\{SCF$/ ) {
	    $scf_flag = 1;
	    $first_flag = 1;
	    $ori_val = 1;
	    $count = 1;
	} elsif ( $scf_flag ) {
	    if ( /^acc:\((\d+),(\d+)\)$/ ) {
		$acc = $1;
		$scf = $1;
		$scf_iid{ $1 } = $2;
	    } elsif ( /^ct1:(\d+)$/ ) {
		$con_scf{ $1 } = $acc;
		$contigid = $1;
		$prev_con = $1;
		if ( $first_flag ) {
		    push( @{ $scf_con{$scf} }, $contigid );
		    $scf_length = 0;
		    $scf_start{ $acc } = $con_pos;
		    if ( $first_first_scf ) {
			$con_pos += 60;
			$scf_start{ $acc } = $con_pos;
		    } else {
			$first_first_scf = 1;
		    }
		    $con_start{ $contigid } = $con_pos;
		    $con_pos += $con_len{ $contigid };
		    $scf_length += $con_len{ $contigid };
		}
	    } elsif ( /^ct2:(\d+)$/ ) {
		$con_scf{ $1 } = $acc;
		next if ($contigid == $1);
		$contigid = $1;
	    } elsif ( /^mea:(-*\d+)\.(\d+)$/ ) {
		my $gap = $1;
		$con_pos += $gap;
		$gap = 20 if ( $gap < 20 );
		$scf_length += $gap;
	    } elsif ( /^ori:(.)$/ ) {
		if ( $first_flag ) {
		    $ori_val = $first_ori{ $1 };
		    $con_ori{ $prev_con } = $ori_val;
		    $first_flag = 0;
		    $con_count = $con_iid{ $prev_con };
		    print_utg( $prev_con, $con_count, "CEL1" );
		    print_utg( $prev_con, $con_count, "CEL2" );
		    print_bactig( $prev_con, $con_count, "CEL1" );
		    print_bactig( $prev_con, $con_count, "CEL2" );
		}
		$ori_val *= $ori{ $1 };
		if ( $prev_con != $contigid ) {
		    $con_count = $con_iid{ $contigid };
		    push( @{ $scf_con{$scf} }, $contigid );
		    $con_ori{ $contigid } = $ori_val;
		    $con_start{ $contigid } = $con_pos;
		    #print STDERR "ID: $contigid\n";
		    $con_pos += $con_len{ $contigid };
		    $scf_length += $con_len{ $contigid };
		    print_utg( $contigid, $con_count, "CEL1" );
		    print_utg( $contigid, $con_count, "CEL2" );
		    print_bactig( $contigid, $con_count, "CEL1" );
		    print_bactig( $contigid, $con_count, "CEL2" );
		}
	    } elsif ( /^\{/ ) {
		$count++;
	    } elsif ( /^\}$/ ) {
		$count--;
		if ( $count == 0 ) {
		    $scf_flag = 0;
		    $scf_len{ $acc } = $scf_length;
		    print_ctg( $scf, "CEL1" );
		    print_ctg( $scf, "CEL2" );
		}
	    }
	} elsif ( /^\{CCO$/ ) {
	    $cco_flag = 1;
	    $count = 0;
	}
	next if (!$cco_flag);
	if ( /^acc:\((\d+),(\d+)\)$/) {
	    $acc = $1;
	    $con_iid{ $1 } = $2;
	} elsif ( /^len:(\d+)$/ ) {
	    $con_len{ $acc } = $1;
	} elsif ( /^\{UPS$/ ) {
	    $ups_flag = 1;
	    $count++;
	} elsif ( $ups_flag ) {
	    if ( /^typ:(.)$/ ) {
		$utg_type = $1;
		if ( $1 ne "U" && $1 ne "R" ) {
		    $surrogate_flag = $1;
		} else {
		    $surrogate_flag = 0;
		}
	    } elsif ( /^lid:(\d+)$/ ) {
		$fragment = $1;
		if ( $surrogate_flag ) {
		    if ( $surrogates{ $fragment } ) {
			$surrogates{ $fragment }++;
		    } else {
			$surrogates{ $fragment } = 1;
		    }
		}
	    } elsif ( /^pos:(\d+),(\d+)$/ ) {
		my $start = $1;
		my $end = $2;
		my $ori = 1;
		if ( $start > $end ) {
		    ($start, $end) = ($end, $start);
		    $ori = -1;
		}
		if ( $surrogate_flag ) {
		    push( @{ $utg_con{ $fragment } }, [$acc, $start, $end, $ori] );
		    $fragment = $fragment."surrogate$surrogates{ $fragment }".$utg_type;
		} elsif ( $utg_type eq "R" ) {
		    $fragment = $fragment."surrogate1".$utg_type;
		}
		$unitig_positions->{$fragment} = [$start, $end, $ori];
		push( @{$con_utg{ $acc }}, $fragment );
		$ups_flag = 0;
	    }
	} elsif ( /^typ:B$/ ) {
	    $bactig_flag = 1;
	} elsif ( $bactig_flag ) {
	    if ( /^mid:(\d+)$/ ) {
		$fragment = $1;
	    } elsif ( $bactig_flag && /^pos:(\d+),(\d+)$/ ) {
		my $start = $1;
		my $end = $2;
		if ( $start > $end ) {
		    ($start, $end) = ($end, $start);
		}
		$bactig_positions->{$fragment} = [$start, $end];
		push( @{$con_bactig{ $acc }}, $fragment );
		$bactig_flag = 0;
	    }
	} elsif ( /^typ:$opt_t$/ ) {
	    $read_flag = 1;
	} elsif ( $read_flag ) {
	    if ( /^mid:(\d+)/ ) {
		$fragment = $1;
		$frg_con{ $fragment } = $acc;
		push( @{$fragments}, $fragment );
	    } elsif ( $read_flag && /^pos:(\d+),(\d+)$/ ) {
		my $start = $1;
		my $end = $2;
		my $ori = 1;
		if ( $start > $end ) {
		    $ori = -1;
		    ($start, $end) = ($end, $start);
		}
		$positions->{ $fragment } = [$start, $end, $ori];
		$read_flag = 0;
	    }
	} elsif ( /^\{MPS$/ || /^\{CCO$/ ) {
	    $count++;
	} elsif ( /^\}$/ ) {
	    $count--;
	    if ( $count == 0 ) {
		$cco_flag = 0;
	    }
	}
    }
    close( ASM );
}

sub getIntervals {
    my $frags = shift;
    my $pos = shift;
    my @intervals = ();
    my %checked;
    my (%clone_lib, %lib, %mate, %lib_std);

    ## Initialize the library sizes and their standard deviations
    open( LIB, "$DataStorePath/Lib.ascii" ) || croak "Can't open file $DataStorePath/Lib.ascii";
    while( <LIB> ) {
	my ($lib, $size, $std) = split;
	$lib{ $lib } = $size;
	$lib_std{ $lib } = $std;
    }
    close( LIB );

    tie( %clone_lib, 'DB_File', "$DataStorePath/cloneLib.info", O_RDONLY, 0644, $DB_BTREE );
    tie( %mate, 'DB_File', "$DataStorePath/mate.info", O_RDONLY, 0644, $DB_BTREE );
    for( my $i = 0; $i <= $#{$frags}; $i++ ) {
	my $mate1 = $frags->[$i];
	next if ( $checked{ $mate1 } );
	next if ( ! $mate{ $mate1 } );

	my $mate2 = $mate{ $mate1 };
	my $cloneSize = $lib{ $clone_lib{ $mate1 } };
	my $stddev = $lib_std{ $clone_lib{ $mate1 } };

	if ( !$cloneSize ) {
	    print STDERR "$mate1  $mate2 Lib: $clone_lib{ $mate1 }\n";
	}
	$checked{ $mate1 } = 1;
	$checked{ $mate2 } = 1;

	next if ( !$con_scf{ $frg_con{ $mate1 } } ); # next if the mate is on a degenerate contig

	my ($start1, $start2, $end1, $end2, $ori1, $ori2);

	my $scf1 = $con_scf{ $frg_con{ $mate1 } };

	if ( $con_ori{ $frg_con{ $mate1 } } == -1 ) {
	    my $temp = $pos->{ $mate1 }->[0];
	    $pos->{ $mate1 }->[0] = $con_len{ $frg_con{ $mate1 } } - $pos->{ $mate1 }->[1];
	    $pos->{ $mate1 }->[1] = $con_len{ $frg_con{ $mate1 } } - $temp;
	    $pos->{ $mate1 }->[2] *= -1;
	}

	$start1 = $pos->{$mate1}->[0] + $con_start{ $frg_con{ $mate1 } };
	$end1 = $pos->{$mate1}->[1] + $con_start{ $frg_con{ $mate1 } };
	$ori1 = $pos->{$mate1}->[2];

	# The mate is not assembled in the non-degenerate scaffold where it should by distance.
	my $row = "R9";
	if ( !($pos->{$mate2}) ) {
	    my $ctg;
	    ($ctg, $start2, $end2, $ori2) = check_missing_mate($start1, $end1, $ori1, $cloneSize, $mate2, $stddev);
	    if ( $ctg ) {
		$frg_con{ $mate2 } = $ctg;
		$row = "R130";
	    } else {
		if ( $opt_g ) {
		    print MIS "$scf1\t$mate1\t$mate2\t$clone_lib{$mate1}\t$cloneSize\t$start1\t$end1\t$ori1\t$scf_len{ $scf1 }\n";
		}
		if ( $ori1 == 1 ) { 
		    if ( $start1 - $scf_start{ $scf1 } + $cloneSize + $std_cutoff * $stddev < $scf_len{ $scf1 } ) {
			$missing_mates++;
			print CEL1 "${mate1}MissingMate${mate2}Size$cloneSize: ",$start1," A8FragColor ",$start1+$cloneSize," R150\n";
		    }
		} else {
		    if ( $end1 - $scf_start{ $scf1 } - $cloneSize - $std_cutoff * $stddev > 0 ) {
			$missing_mates++;
			print CEL1 "${mate1}MissingMate${mate2}Size$cloneSize: ",$end1-$cloneSize," A9FragColor ",$end1," R150\n";
		    }
		}
		next;
	    }
	}
	next if ( !$con_scf{ $frg_con{ $mate2 } } );

	$total_mates++;

	##  Check whether two mates are on the same scaffold
	my $scf2 = $con_scf{ $frg_con{ $mate2 } };

	my $ctg_uid = $frg_con{ $mate1 };   # The UID for the contig where frag mate1 is on
	if ( $scf1 != $scf2 ) {
	    $inter_scf_mates++;   #  Inter-scaffold mates

	    $out = "CEL2";
	    if ( $scf_links{ "$scf1-$scf2" } ) {
		$scf_links{ "$scf1-$scf2" }++;
	    } elsif ( $scf_links{ "$scf2-$scf1" } ) {
		$scf_links{ "$scf2-$scf1" }++;
	    } else {
		$scf_links{ "$scf1-$scf2" } = 1;
	    }
	} elsif ( $scf1 == $scf2 ) {
	    $out = "CEL1";
	} else {
	    print STDERR "Other conditions happen!\n";
	}

	if ( $con_ori{ $frg_con{ $mate2 } } == -1 && (! $start2) ) {
	    my $temp = $pos->{ $mate2 }->[0];
	    $pos->{ $mate2 }->[0] = $con_len{ $frg_con{ $mate2 } } - $pos->{ $mate2 }->[1];
	    $pos->{ $mate2 }->[1] = $con_len{ $frg_con{ $mate2 } } - $temp;
	    $pos->{ $mate2 }->[2] *= -1;
	}

	if ( ! $start2 ) {
	    $start2 = $pos->{ $mate2 }->[0] + $con_start{ $frg_con{ $mate2 } };
	    $end2 = $pos->{ $mate2 }->[1] + $con_start{ $frg_con{ $mate2 } };
	    $ori2 = $pos->{$mate2}->[2];
	}

	my $outie = 0;
	my $distance;
	my ($low, $high);
	if ( $start1 < $end2 ) {
	    $distance = $end2 - $start1;
	    $low = $end1;
	    $high = $start2;
	    $outie = 1 if ( $ori1 == -1 && $ori2 == 1);
	} elsif ( $start2 < $end1 ) {
	    $low = $end2;
	    $high = $start1;
	    $distance = $end1 - $start2;
	    $outie = 1 if ( $ori2 == -1 && $ori1 == 1 );
	} else {
	    print STDERR "Confused mates!  $start1  $end1  $ori1  $start2  $end2  $ori2\n";
	    next;
	}

	##  The mates have the same orientation.
	if ( $ori1 * $ori2 != -1 ) {
	    if ( $low > $high ) {
		($low, $high) = ($high, $low);
	    }
	    if ( $ori1 == 1 ) {
		print $out "${ctg_uid}CtgFrag${mate1}Mate${mate2}Size$cloneSize: $low A3FragColor $high R60 # SCF $scf1 $scf2\n";
	    } else {
		print $out "${ctg_uid}CtgFrag${mate1}Mate${mate2}Size$cloneSize: $low A4FragColor $high R60 # SCF $scf1 $scf2\n";
	    }
	    $sameori_mates++ if ( $out eq "CEL1" );
	    push( @intervals, [$low, $high] ) if ( $cloneSize < $BAC_SIZE && $out eq "CEL1" );
	    next;
	}

	##  The mates have the distance that doesn't agree with the library size
	if ( abs( $distance - $cloneSize) > $stddev * $std_cutoff ) {
	    if ( $low > $high ) {
		($low, $high) = ($high, $low);
	    }
	    push( @intervals, [$low, $high] ) if ( $cloneSize < $BAC_SIZE && $out eq "CEL1" );
	    if ( $outie ) {
		$outie_mates++ if ( $out eq "CEL1" );
		print $out "${ctg_uid}CtgFrag${mate1}Mate${mate2}Size$cloneSize: $low A5FragColor $high R60 # SCF $scf1 $scf2\n";
	    } else {
		if ( $cloneSize > $BAC_SIZE && $distance >= 70000 && $distance <= 250000 )  {
		    $good_mates++ if ( $out eq "CEL1" );
                    if( $frg_con{ $mate1 } == $frg_con{ $mate2 } ) {
                        print $out "${ctg_uid}CtgFrag${mate1}Mate${mate2}Size$cloneSize: $low A2FragColor $high $row # SCF $scf1 $scf2\n";
                    } else {
                        print $out "${ctg_uid}CtgFrag${mate1}Mate${mate2}Size$cloneSize: $low A1FragColor $high $row # SCF $scf1 $scf2\n";
                    }
		} else {
		    $wrong_size_mates++ if ( $out eq "CEL1" );
                    if( $distance - $cloneSize > $stddev * $std_cutoff ) {
                        print $out "${ctg_uid}CtgFrag${mate1}Mate${mate2}Size$cloneSize: $low A6FragColor $high R60 # SCF $scf1 $scf2\n";
                    } else {
                        print $out "${ctg_uid}CtgFrag${mate1}Mate${mate2}Size$cloneSize: $low A7FragColor $high R60 # SCF $scf1 $scf2\n";
                            
                    }
		}
	    }

	## Then the mates have the distance that agree with the library size
	} else {
	    if ( $low > $high ) {
		($low, $high) = ($high, $low);
	    }
	    if ( $outie ) {
		push( @intervals, [$low, $high] ) if ( $cloneSize < $BAC_SIZE && $out eq "CEL1" );
		$outie_mates++ if ( $out eq "CEL1" );
		print $out "${ctg_uid}CtgFrag${mate1}Mate${mate2}Size$cloneSize: $low A5FragColor $high R60 # SCF $scf1 $scf2\n";
	    } else {
		$good_mates++ if ( $out eq "CEL1" );
                if( $frg_con{ $mate1 } == $frg_con{ $mate2 } ) {
 		    print $out "${ctg_uid}CtgFrag${mate1}Mate${mate2}Size$cloneSize: $low A2FragColor $high $row # SCF $scf1 $scf2\n";
                } else {
 		    print $out "${ctg_uid}CtgFrag${mate1}Mate${mate2}Size$cloneSize: $low A1FragColor $high $row # SCF $scf1 $scf2\n";
                }
	    }
	}
    }
    untie %mate;
    untie %clone_lib;
    if ( !$opt_b ) {
	open( BAD, ">$bad" ) || die "Can't open file $bad to write!";
	calCoverage( \@intervals, $con_pos, $comp, "BAD" );
	close( BAD );
    }
}

# Print the contig and scaffold LNKs.
sub print_ctg {
    my ($scf, $out) = @_;
    my $LNK = "LNK:";
    foreach my $contig ( @{ $scf_con{$scf} } ) {
	print $out "${scf}ScaCtg$contig: $con_start{ $contig } A0ContigColor ", $con_start{ $contig } + $con_len{ $contig }, " R1 # Scaffold $scf Ctg $contig\n";
	$LNK .= " ${scf}ScaCtg$contig";
    }
    print $out "$LNK A0ScaffoldColor\n";
}

sub print_utg {
    my $contig = shift;
    my $count = shift;
    my $out = shift;
    return if ( ! $con_utg{$contig} );
    my $ori = $con_ori{ $contig };
    foreach( @{$con_utg{ $contig }} ) {
	my $seg;
	my $iid = $_;
	if ( $utg_src{ $_ } ) {
	    $seg = "${count}CtgCI$iid$utg_src{$_}";
	} else {
	    $seg = "${count}CtgCI$iid";
	}
        my $color = "A1CGBColor";
        if ( /surrogate\d+(.)$/ ) {
	    if ( $1 eq "R" ) {
		$color = "A11CGBColor";
	    } elsif ( $1 eq "P" ) {
		$color = "A13CGBColor";
	    } elsif ( $1 eq "s" ) {
		$color = "A4CGBColor";
	    } elsif ( $1 eq "S" ) {
		$color = "A12CGBColor";
	    } else {
		#print STDERR "$1\n";
		$color = "A6CGBColor";
	    }
        }
	if ( $ori == -1 ) {
	    my $temp = $unitig_positions->{$_}->[0];
	    $unitig_positions->{$_}->[0] = $con_len{ $contig } - $unitig_positions->{$_}->[1];
	    $unitig_positions->{$_}->[1] = $con_len{ $contig } - $temp;
	}
	print $out "$seg: ", $unitig_positions->{$_}->[0] + $con_start{ $contig }, " $color ", $unitig_positions->{$_}->[1] + $con_start{ $contig }, " R3\n"; # Contig $contig CI $_\n";
    }
}

sub print_bactig {
    my $contig = shift;
    my $count = shift;
    my $out = shift;
    return if ( !$con_bactig{$contig} );
    foreach( @{$con_bactig{ $contig }} ) {
	print $out "${ count }CtgFrag$_: ", $bactig_positions->{$_}->[0] + $con_start{ $contig }, " A0FragColor ", $bactig_positions->{$_}->[1] + $con_start{ $contig }, " R14 # Contig $contig Frag $_\n";
    }
}

## print the header for the celamy file
sub print_header {
    my $out = shift;
print $out <<EOF;
0GBColor: C000000 T2 S  # Unused
1CGBColor: CFFFF00 T2 S  # DUnique
2CGBColor: CFF8040 T2 S  # Consistent
3CGBColor: C808000 T2 S  # OneFrag
4CGBColor: C00FFFF T2 S  # Repeat
5CGBColor: CFF00FF T2 S  # BadUnique
6CGBColor: CFF9A11 T2 S  # Surrogate
7CGBColor: C00FFFF T2 S  # OrphanFrag
8CGBColor: C00FF00 # LeftBP
9CGBColor: CFF0000 # RightBP
10CGBColor: CFF0077 T2 S  # PUnique
11CGBColor: CFF0000 T2 S  # RockCI
12CGBColor: C77EF77 T2 S  # StoneCI
13CGBColor: C8080FF T2 S  # WalkCI
0CGWColor: CFF0000 T2 S # C0
1CGWColor: C00FF00 T2 S # C1
2CGWColor: C0000FF T2 S # C2
3CGWColor: CFFFF00 T2 S # C3
4CGWColor: C00FFFF T2 S # C4
5CGWColor: CFF00FF T2 S # C5
6CGWColor: CFF0040 T2 S # C6
7CGWColor: C00FF40 T2 S # C7
8CGWColor: C4000FF T2 S # C8
9CGWColor: CFF4000 T2 S # C9
10CGWColor: C40FF00 T2 S # C10
11CGWColor: C0040FF T2 S # C11
12CGWColor: CFF4040 T2 S # C12
13CGWColor: C40FF40 T2 S # C13
14CGWColor: C4040FF T2 S # C14
15CGWColor: CFF4040 T2 S # C15
16CGWColor: C40FF40 T2 S # C16
17CGWColor: C4040FF T2 S # C17
0ContigColor: C0040FF T2 S # Contigs
0InvalidContigColor: C00FFFF T2 S # InvalidContigs
0ContigRealColor: C40FF00 T2 S # RealContigs
0ScaffoldColor: CFF0040 T2 S # Scaffolds
0SingleScaffoldColor: CFF4000 T2 S # SingleScaffolds
0ContigLinkColor: C00FF00 T1 S # ContigLinks
0ScaffoldEdgeColor: C00FF40 T2 S # ScaffoldEdges
0FragColor: C00FFFF T2 S # Bactigs
1FragColor: C00CC00 T1 S # GoodInterContig
2FragColor: CA0FFA0 T1 S # GoodIntraContig
3FragColor: CFF00FF T1 S # Normal
4FragColor: CFFFFFF T1 S # AntiNormal
5FragColor: C4000FF T1 S # Outie
6FragColor: CAAAA00 T1 S # TooFar
7FragColor: CFFFF00 T1 S # TooClose
8FragColor: CFF4040 T1 S # MissingToRight
9FragColor: C00FFFF T1 S # MissingToLeft
EOF
}

# The subroutine will calculate the coverage for a given region when
# given all intervals.
sub calCoverage {
    my $intervals = shift;
    my $length = shift;
    my $name = shift;
    my $out = shift;
    my %rec;
    my @pos = ();
    foreach my $r (@{ $intervals }) {
        push( @pos, @{ $r } );
    }
    push( @pos, 0, $length );
    @pos = uniq_num(@pos);
    my %index;

    ## construct data structure
    for( my $i = 0; $i < @pos+0; ++$i ) {
        $index{ $pos[$i] } = $i;
        $rec{ $pos[$i] } = 0;
    }

    ## calculate intervals
    foreach my $r (@{ $intervals } ) {
        my $low = $r->[0];
        my $high = $r->[1];
        ($low, $high) = ($high, $low) if ( $low > $high );
        my $i = $index{ $low };
        my $p = $pos[$i];
        while( $p < $high ) {
            $rec{ $p }++;
            ++$i;
            $p = $pos[$i];
        }
    }

    ## print out the result
    for( my $i = 0; $i < @pos-1; ++$i ) {
        my $s = $pos[$i];
        my $e = $pos[$i+1];
        if ( $out ) {
            print $out "$name\t$s\t$e\t$rec{$s}\t", $e - $s, "\n";
        } else {
            print "$name\t$s\t$e\t$rec{$s}\t", $e - $s, "\n";
        }
    }

    ## clean up the memory
    @pos = ();
    %index = ();
}

sub numerically { $a <=> $b; }

#  Find the minimum number
sub min {
    my $min = shift;
    foreach ( @_ ) {
        if ( $_ < $min ) {
            $min = $_;
        }
    }
    return $min;
}

# Find the maximum number
sub max {
    my $max = shift;
    foreach( @_ ) {
        if ( $_ > $max ) {
            $max = $_;
        }
    }
    return $max;
}

# Get the information block for a given type such as scf, ctg, or utg.
sub get_value {
    my $id = shift;
    my $asmFile = shift;
    my $indexFile = $asmFile . ".index";
    if ( $opt_x ) {
	$indexFile = $opt_x;
    }
    open( IN, $asmFile ) || die "Can't open file asm";
    my %hash;
    tie( %hash, 'DB_File', $indexFile, O_RDONLY, 0644, $DB_BTREE );
    my $str;
    my ($pos, $len) = split( /\|/, $hash{ $id } );
    seek( IN, $pos, 0 );
    read( IN, $str, $len );
    close( IN );
    untie %hash;
    return $str;
}

# Get contigs in a scaffold.
sub get_scf_cons {
    my $asmFile = shift;
    my $scf = shift;
    my @scf_msg = ();
    if ( $scf ) {
	@scf_msg = split(/\n/, get_value($scf, $asmFile));
    } else {
	@scf_msg = `cat $asmFile`;
    }
    my @cons = ();
    my $prev_con = 0;
    foreach( @scf_msg ) {
	if ( /^ct1:(\d+)$/ ) {
	    push( @cons, $1 ) if ( $prev_con != $1 );
	    $prev_con = $1;
	} elsif ( /^ct2:(\d+)$/ ) {
	    push( @cons, $1 ) if ( $prev_con != $1 );
	    $prev_con = $1;
	}
    }
    return (@cons);
}

# Check whether a fragment is in surrogates and agree with mate pair range
sub check_missing_mate {
    my ($start1, $end1, $ori1, $cloneSize, $mate2, $stddev) = @_;
    my $utg = $frg_utg{ $mate2 };

    return (0, 0, 0, 0) if ( !$utg );

    foreach( @{$utg_con{ $utg }} ) {
	my $r = $_;
	my $ctg = $r->[0];
	next if (!$con_start{ $ctg });

	my $utg_start = $r->[1];
	my $utg_end = $r->[2];
	my $utg_ori = $r->[3];

	## If contig ori is -1, flip the utg info
	if ( $con_ori{ $ctg } == -1 ) {
	    $utg_start = $con_len{ $ctg } - $r->[2];
	    $utg_end = $con_len{ $ctg } - $r->[1];
	    $utg_ori *= -1;
	}

	my $utg_frg_start = $utg_frg_pos->{ $mate2 }->[0];
	my $utg_frg_end = $utg_frg_pos->{ $mate2 }->[1];
	my $utg_frg_ori = $utg_frg_pos->{ $mate2 }->[2];

	## If utg ori is -1, flip the fragment info
	if ( $utg_ori == -1 ) {
	    $utg_frg_start = $utg_len{ $utg } - $utg_frg_pos->{ $mate2 }->[1];
	    $utg_frg_end = $utg_len{ $utg } - $utg_frg_pos->{ $mate2 }->[0];
	    $utg_frg_ori *= -1;
	}

	my $start2 = $utg_start + $con_start{ $ctg } + $utg_frg_start;
	my $end2 = $utg_start + $con_start{ $ctg } + $utg_frg_end;
	my $ori2 = $utg_frg_ori;
	if ( $ori1 * $ori2 == -1 ) {
	    if ( abs((max($start1, $end1, $start2, $end2) - min($start1, $end1, $start2, $end2))-$cloneSize) < $std_cutoff * $stddev ) {
		return ($ctg, $start2, $end2, $ori2);
	    }
	}
    }
    return (0, 0, 0, 0);
}

#  Get the uniq numeric list
sub uniq_num {
    my @a = sort { $a <=> $b } ( @_ );
    if ( $#a < 0 ) {
        return @a;
    }
    my @uniq_a = ();
    my $prev = shift( @a );
    push( @uniq_a, $prev );
    foreach ( @a ) {
        if ( $_ eq $prev ) {
            next;
        } else {
            push( @uniq_a, $_ );
            $prev = $_;
        }
    }
    return @uniq_a;
}

# Set variables %con_len, %frg_con, $fragments, $positions, %scf_con, %con_ori, 
# %con_scf, %con_start, %scf_len
sub set_scf_frg {
    my ( $scf, $start) = @_;

    $scf_start{ $scf } = $start;

    my $sql = "
	SELECT scaffold_id, contig_1_id, contig_2_id, mean_distance, contig_orientation, appearance_order
	FROM   scaffold_edge
	WHERE  scaffold_id = $scf
	ORDER BY appearance_order
	";
    my $sth = $dbh->prepare( $sql );
       $sth->execute();
    my $prev_con = 0;
    my $r = \@{ $scf_con{$scf} };
    my %con_gap;
    while(my ($s, $con1, $con2, $mean, $ori, $order) = $sth->fetchrow_array() ) {
	push(@{ $r }, $con1) if ($con1 != $prev_con);
	$prev_con = $con1;
	push(@{ $r }, $con2) if ($con2 != $prev_con);
	$prev_con = $con2;
	$con_scf{ $con1 } = $s;
	$con_scf{ $con2 } = $s;
	if ( $ori =~ /N/ ) {
	    $con_ori{ $con1 } = 1;
	    $con_ori{ $con2 } = 1;
	} elsif ( $ori =~ /I/ ) {
	    $con_ori{ $con1 } = 1;
	    $con_ori{ $con2 } = -1;
	} elsif ( $ori =~ /A/ ) {
	    $con_ori{ $con1 } = -1;
	    $con_ori{ $con2 } = -1;
	} elsif ( $ori =~ /O/ ) {
	    $con_ori{ $con1 } = -1;
	    $con_ori{ $con2 } = 1;
	}
	$mean = $1 if ( $mean =~ /(-*\d+)\.\d+/ );
	$mean = 0 if ( $mean =~ /-\.\d+/ );
	$mean = 0 if ( $mean =~ /\.\d+/ );
	$con_gap{ $con1 } = $mean;
    }
    $sth->finish();

    ## Singleton scaffolds don't have rows in scaffold_edge
    my $sql2 = "
	SELECT scaffold_id, contig_id
	FROM   scaffold_contig
	WHERE  scaffold_id = $scf
	";

    if ( @{ $r }+0 == 0 ) {
	$sth = $dbh->prepare( $sql2 );
	$sth->execute();
	my ($s, $con) = $sth->fetchrow_array();
	push( @{ $r }, $con );
	$con_scf{ $con } = $s;
	$con_ori{ $con } = 1;
	$con_gap{ $con } = 0;
	$sth->finish();
    }
    # Set variables %con_utg, %unitig_positions, %con_len, %frg_con, $fragments, and $positions
    set_con_len( $scf );
    set_frg_con( $scf );
    #set_con_utg( @{ $r });
    set_con_utg( $scf );

    $scf_len{ $scf } = 0;
    my $last_ctg = pop(@{ $r });
    foreach( @{ $r } ) {
	my $gap = $con_gap{ $_ };
	$con_start{ $_ } = $start;
	$start += $con_len{ $_ };
	$start += $gap;
	$scf_len{ $scf } += $con_len{ $_ };
	$gap = 20 if ( $gap < 20 );
	$scf_len{ $scf } += $gap;
    }
    $con_start{ $last_ctg } = $start;
    $start += $con_len{ $last_ctg };
    $scf_len{ $scf } += $con_len{ $last_ctg };
    push( @{ $r }, $last_ctg );
}

# Set variable %con_len
sub set_con_len {
    my $scf = shift;

    my $sql = "
	SELECT C.contig_id, C.ungapped_consensus_length
	FROM   scaffold_contig SC, contig C
	WHERE  SC.scaffold_id = $scf
	AND    SC.contig_id = C.contig_id
	";

    my $sth = $dbh->prepare( $sql );
    $sth->execute();
    while( my ($ctg, $len) = $sth->fetchrow_array() ) {
	$con_len{ $ctg } = $len;
    }
    $sth->finish();
}

# Sets variables $fragments, %frg_con, and $positions
sub set_frg_con {
    my $scf = shift;

    my $sql = "
	SELECT CF.contig_id, CF.fragment_id, CF.ungapped_frag_consensus_start, CF.ungapped_fragment_length, CF.orientation
	FROM   scaffold_contig SC, contig_fragment CF
	WHERE  SC.scaffold_id = $scf
	AND    SC.contig_id = CF.contig_id
	";

    my $sth = $dbh->prepare( $sql );
    $sth->execute();
    while( my ($ctg, $frag, $start, $len, $ori ) = $sth->fetchrow_array() ){
	push( @{$fragments}, $frag );
	$frg_con{ $frag } = $ctg;
	$positions->{$frag} = [$start, $start + $len, $ori];
    }
    $sth->finish();
}

# Sets variables $fragments, %unitig_positions, $utg_con, $utg_len, and $con_utg
sub set_con_utg {
    #my @cons = @_;
    my $scf = shift;

    my @surrogates = ();
    my $sql = "
	SELECT CU.contig_id, CU.unitig_id, CU.ungapped_unitig_consensus_strt, CU.ungapped_unitig_length, CU.placement_type, CU.orientation
	FROM   scaffold_contig SC, contig_unitig CU
	WHERE  SC.scaffold_id = $scf
	AND    SC.contig_id = CU.contig_id
	";
    
    my $sth = $dbh->prepare( $sql );
    $sth->execute();
    my $i = 0;
    while( my ($ctg, $utg, $start, $len, $type, $ori ) = $sth->fetchrow_array() ){
	if ( $type > 1 ) {
	    push( @{ $utg_con{ $utg } }, [$ctg, $start, $start + $len, $ori] );
	    push( @surrogates, $utg );
	}
	if ( $type != 0 ) {
	    if ( $type == 1 ) {
		$type = "R";
	    } elsif ( $type == 2 ) {
		$type = "S";
	    } elsif ( $type == 3 ) {
		$type = "P";
	    } elsif ( $type == 4 ) {
		$type = "s";
	    }
	    $utg .= "surrogate$i$type";
	    $i++;
	}
	push( @{$con_utg{ $ctg }}, $utg );
	$unitig_positions->{$utg} = [$start, $start + $len, $ori];
	$utg_len{ $utg } = $len;
    }
    $sth->finish();
    @surrogates = uniq_num( @surrogates );
    set_utg_frg_pos( @surrogates );
}

## Set variable $utg_frg_pos
sub set_utg_frg_pos {
    my @utgs = @_;

    my $n = 0;
    while( $n < @utgs ) {
	my $end = ($n + 199 >= @utgs) ? $#utgs : ($n + 199);
	my $sql = "SELECT unitig_id, fragment_id, ungapped_frag_consensus_start, ungapped_fragment_length, orientation
		   FROM   unitig_fragment
		   WHERE  unitig_id in (" . join( ",\n", @utgs[$n..$end]) . ")";
	my $sth = $dbh->prepare( $sql );
	$sth->execute();
	while( my ( $utg, $frag, $start, $len, $ori) = $sth->fetchrow_array() ) {
	    $utg_frg_pos->{ $frag } = [$start, $start + $len, $ori];
	}
	$sth->finish();
	$n = $end + 1;
    }
}
