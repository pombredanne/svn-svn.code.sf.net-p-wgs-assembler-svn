#
###########################################################################
#
# This file is part of Celera Assembler, a software program that 
# assembles whole-genome shotgun reads into contigs and scaffolds.
# Copyright (C) 1999-2004, Applera Corporation. All rights reserved.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received (LICENSE.txt) a copy of the GNU General Public 
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
###########################################################################
#
# A member of the "ecoli" family of DNA specification files.
# The genome size is 1.86Mbp.
#
.comment $Id: ecoli001.sim,v 1.1.1.1 2004-04-14 13:55:40 catmandew Exp $
.seed
75171
.dna
Z < E_coli.fasta ;
# A FASTA file has the format: 
# > commment
# ([ACGT]*\n)*
#
# Suppose the genome is 4.716 M bp and each fragment is 500 bp. 
# Then 1x coverage is 9433 fragments.
# Thus we want 94330 fragments for 10x coverage.
#
# 30% no mate, 0.7*(80% 2k separated mate, 20% 10k separated mate)
# 30% no mate, 56% 2k separated mate, 14% 10k separated mate
# Let G be the genome length and B the BAC length: 15*G = B*n/2 
# Bac ends are a bit shorter, higher error rate, higher chimer rate,
# low single rate. The desired length range is mean 150k stddev 33k
# (2*10**(-4))*G  BAC END Fragments 
.sample
75464
300 800 0.5
0 0 0 0
0.005 0.020 0.33 0.33
.3 1200 2349 .01
.sample
18866
300 800 0.5
0 0 0 0
0.005 0.020 0.33 0.33
.3 6050 12800 .01
.bacends
943
300 500 0.5
0 0 0 0
.0025 .050 .33 .33
.01 50000 250000 .20
