#!/usr/bin/perl -w
#
###########################################################################
#
# This file is part of Celera Assembler, a software program that 
# assembles whole-genome shotgun reads into contigs and scaffolds.
# Copyright (C) 1999-2004, Applera Corporation. All rights reserved.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received (LICENSE.txt) a copy of the GNU General Public 
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
###########################################################################
#
#
# Usage: addfbac locale start length fragsize cover dnafile fragfile outfile
#
# Simulate an FBAC of length <length> starting at position <start> in the
# simulated sequence contained in the file <dnafile>.
# (This file is generated and preserved by celsim using the -d option.)
# The fragfile is inspected to figure out which accession numbers to use
#
# The fbac is shredded into fragments of size <fragsize> at the given
# <coverage> and the fragment messages are placed in <outfile>.
#
# Author=DHH
#

# script switches:
$verbose=1;

$quality=chr(48+30);  # quality value for fbac fragments, here: 30


# parse commandline:
if($#ARGV !=7)
{
	die "Usage: ",
	"addfbac locale startpos length fragsize coverage fragfile dnafile outfile\n";
}
$locale=shift;
$startpos=shift;
$length=shift;
$fragsize=shift;
$coverage=shift;
$fragfile=shift;
$dnafile=shift;
$outfile=shift;

if($verbose==1)
{
	print "------addfbac------\n";
	print "locale   = $locale\n";
	print "startpos = $startpos\n";
	print "length   = $length\n";
	print "fragsize = $fragsize\n";
	print "coverage = $coverage\n";
	print "fragfile = $fragfile\n";
	print "dnafile  = $dnafile\n";
	print "outfile  = $outfile\n";
}

# figure out the first unused accession number by peaking in to the .frg file

$accession=`grep -c acc: $fragfile`
	or die "Can't read file: $fragfile\n";
$accession++;
if($verbose==1)
{
	print "accession= $accession\n";
}

# We need to compute the increment that will yield the given coverage for
# the given fragsize

$increment=int $fragsize/$coverage;
if($verbose==1)
{
	print "increment= $increment\n";
}

# Read the dna:
$seq="";
$seqlen=0;

open FP, "$dnafile" or die "Can't open file to read: $dnafile\n";

$state=0; # 0: before data, 1: reading data
$_=<FP>;
while(<FP>)
{
	$line=$_;
	if($state==0) # in this state we ignore everything
	{
		if($line eq "> 0\n") # we leave this state after finding ">"
		{
			$state=1;
		}
	}
	else # $state==1
	{
		chomp $line;
		$seq="$seq$line";
	}
}
close FP;

$seqlen=length $seq;

if($verbose)
{
	print "Sequence = [0,",$seqlen-1,"], length= $seqlen\n";
}

# we cut out the fbac from the dna:

$fbac=substr($seq,$startpos,$length);
$fbaclen=length $fbac;

if($verbose)
{
	print "Fbac     = [$startpos,",$startpos+$fbaclen-1,"], length= $fbaclen\n";
}

# open the outfile:
open FP, ">$outfile" or die "Can't open file to write: $outfile\n";

# shred the fbac and write the output:
$done=0;
$count=0;
$pos=0;
$linelength=70;

while($done==0)
{
	$count++;
	if($pos+$fragsize>=$fbaclen)	# last fragment, make it fit and after
									# printing it, we're done!
	{
		$pos=$fbaclen-$fragsize;
		$done=1;
	}
	$frag=substr($fbac,$pos,$fragsize);
	$fraglen=length $frag;

	print FP "{FRG\n";
	print FP "act:A\n";
	print FP "acc:",$accession++,"\n";
	print FP "typ:F\n";
	print FP "loc:$locale\n";
	print FP "pos:$pos,",$pos+$fragsize-1,"\n";
	print FP "src:\n";
	print FP "f\n";
	print FP "[",$startpos+$pos,",",$startpos+$pos+$fragsize-1,"]\n.\n";
	print FP "etm:0\n";
	print FP "seq:\n";
	for($i=0;$i<$fraglen;$i+=$linelength)
	{
		print FP substr($frag,$i,$linelength),"\n";
	}
	print FP ".\n";
	print FP "qlt:";
	for($i=0;$i<$fraglen;$i++)
	{
		if((($i)%$linelength)==0)
		{
			print FP "\n";
		}
		print FP $quality;
	}
	print FP "\n.\n";
	print FP "clr:0,",$fraglen-1,"\n";
	print FP "}\n";
	$pos+=$increment;
}
if($verbose)
{
	print "Wrote $count fragments to file: $outfile\n";
	print "-------end-------\n";
}
close FP;

# EOF
