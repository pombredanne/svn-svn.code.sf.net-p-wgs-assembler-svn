
    /*
      HISTORICAL NOTE:
      The following code was used for the Rat assembly. An initial run was
      performed after which it was determined that LBAC mate pairs were
      of poor quality. CGW was run again from scratch, disabling LBAC
      mate pairs and rerun using contigs as unitigs & reactivating LBAC
      mate pairs. The code below was used for the rerun.
      It may be useful in the future for similar reruns.
     */
    
    // get rid of all unitig edges
    {
      int32 ne = GetNumGraphEdges(ScaffoldGraph->CIGraph);
      Clear_VA(ScaffoldGraph->CIGraph->edges);
      ScaffoldGraph->CIGraph->edges = CreateVA_EdgeCGW_T(ne);
      ScaffoldGraph->CIEdges = ScaffoldGraph->CIGraph->edges;
    }
    ScaffoldGraph->CIGraph->numActiveEdges = 0;
    ScaffoldGraph->CIGraph->freeEdgeHead = NULLINDEX;
    ScaffoldGraph->CIGraph->tobeFreeEdgeHead = NULLINDEX;
    NullifyAllNodeEdges(ScaffoldGraph->CIGraph);

    // get rid of all contig edges
    {
      Clear_VA(ScaffoldGraph->ContigGraph->edges);
      ScaffoldGraph->ContigGraph->edges =
        CreateVA_EdgeCGW_T(GetNumGraphEdges(ScaffoldGraph->CIGraph));
      ScaffoldGraph->ContigEdges = ScaffoldGraph->ContigGraph->edges;
    }
    ScaffoldGraph->ContigGraph->numActiveEdges = 0;
    ScaffoldGraph->ContigGraph->freeEdgeHead = NULLINDEX;
    ScaffoldGraph->ContigGraph->tobeFreeEdgeHead = NULLINDEX;
    NullifyAllNodeEdges(ScaffoldGraph->ContigGraph);
    
    // get rid of all scaffold graph
    DeleteGraphCGW(ScaffoldGraph->ScaffoldGraph);
    ScaffoldGraph->ScaffoldGraph = CreateGraphCGW(SCAFFOLD_GRAPH, 1024, 1024);
    ScaffoldGraph->numLiveScaffolds = 0;
    ScaffoldGraph->CIScaffolds = ScaffoldGraph->ScaffoldGraph->nodes;
    ScaffoldGraph->SEdges = ScaffoldGraph->ScaffoldGraph->edges;
    
    fprintf(GlobalData->stderrc,"* Deleted all node edges & scaffold graph\n");
    fprintf(data->logfp,"* Deleted all node edges & scaffold graph\n");
    
    // activate LBAC mate pairs that were disabled for round 1
    fprintf(GlobalData->stderrc,"* Activating LBAC mate pairs\n");
    fprintf(data->logfp,"* Activating LBAC mate pairs\n");
    ActivateLBACMatePairs();

    // rebuild unitig edges as performed in phase 2
    BuildGraphEdgesDirectly(ScaffoldGraph->CIGraph);
    ComputeOverlaps( ScaffoldGraph->CIGraph, TRUE, alignOverlaps);  
    CheckEdgesAgainstOverlapper(ScaffoldGraph->CIGraph);
    MergeAllGraphEdges(ScaffoldGraph->CIGraph, FALSE);
    CheckEdgesAgainstOverlapper(ScaffoldGraph->CIGraph);
    if(checkPoint)
    {
      fprintf(GlobalData->stderrc,
              " Dumping checkpoint %d after enabling LBAC edges & building unitig graph edges for rat rerun\n",
              ScaffoldGraph->checkPointIteration); 
      fprintf(GlobalData->timefp,
              " Dumping checkpoint %d after enabling LBAC edges & building unitig graph edges for rat rerun\n",
              ScaffoldGraph->checkPointIteration);
      CheckpointScaffoldGraph(ScaffoldGraph, -1);
    }

    {
      int qq;
      for(qq=0;qq<GetNumCIFragTs(ScaffoldGraph->CIFrags);qq++)
      {
        CIFragT * qfrag = GetCIFragT(ScaffoldGraph->CIFrags, qq);
        if(qfrag->contigOffset5p.mean == qfrag->contigOffset3p.mean &&
           qfrag->cid != NULLINDEX)
        {
          fprintf(stderr, "ALERT - frag " F_CID " has zero length: (%f,%f)\n",
                  qfrag->iid,
                  qfrag->contigOffset5p.mean, qfrag->contigOffset3p.mean);
          if(qfrag->contigID == NULLINDEX)
          {
            if(qfrag->offset5p.mean == qfrag->offset3p.mean)
            {
              fprintf(stderr, "\tFRAG " F_CID " also has zero length in unitig " F_CID "\n",
                      qfrag->iid, qfrag->cid);
              fprintf(stderr, 
                      "\tSetting to A_B by incrementing 3p end coord to %f.\n",
                      ++(qfrag->offset3p.mean));
            }
          }
          else
          {
            NodeCGW_T * qtig = GetGraphNode(ScaffoldGraph->CIGraph, qfrag->cid);
            if(qtig->offsetAEnd.mean < qtig->offsetBEnd.mean)
            {
              if(qfrag->offset5p.mean < qfrag->offset3p.mean)
                qfrag->contigOffset3p.mean++;
              else
                qfrag->contigOffset3p.mean--; 
            }
            else if(qtig->offsetAEnd.mean > qtig->offsetBEnd.mean)
            {
              if(qfrag->offset5p.mean < qfrag->offset3p.mean)
                qfrag->contigOffset5p.mean++;
              else
                qfrag->contigOffset5p.mean--;
            }
            else
            {
              fprintf(stderr, "TRULY MESSED UP FRAG " F_CID " AND UNITIG " F_CID "\n",
                      qfrag->iid, qfrag->cid);
              fprintf(stderr, "Adjusting by incrementing 3p to %f and %f\n",
                      ++(qfrag->offset3p.mean),
                      ++(qfrag->contigOffset3p.mean));
            }
          } 
        }
      }
    }

    // rebuild contig edges
    BuildGraphEdgesDirectly(ScaffoldGraph->ContigGraph);
    CheckEdgesAgainstOverlapper(ScaffoldGraph->ContigGraph);
    MergeAllGraphEdges(ScaffoldGraph->ContigGraph, FALSE);
    CheckEdgesAgainstOverlapper(ScaffoldGraph->ContigGraph);
    if(checkPoint)
    {
      fprintf(GlobalData->stderrc,
              " Dumping checkpoint %d after building contig graph edges for rat rerun\n",
              ScaffoldGraph->checkPointIteration); 
      fprintf(GlobalData->timefp,
              " Dumping checkpoint %d after building contig graph edges for rat rerun\n",
              ScaffoldGraph->checkPointIteration);
      CheckpointScaffoldGraph(ScaffoldGraph, 0);
    }
    
    fprintf(GlobalData->stderrc, "Continuing with standard cgw run for rat\n");
    fprintf(GlobalData->timefp, "Continuing with standard cgw run for rat\n");
