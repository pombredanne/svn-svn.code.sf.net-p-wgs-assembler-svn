#ifdef AHANG_BAND_TEST
Segment *Align_Scaffold(Segment *seglist, int numsegs, int varwin,
			Scaffold *AF, Scaffold *BF, int *best,
			int bandbeg, int bandend)
#else
Segment *Align_Scaffold(Segment *seglist, int numsegs, int varwin,
                               Scaffold *AF, Scaffold *BF, int *best)
#endif
{ int optc = 0, mval=-1;
 COvlps *optco = NULL; 
 int     score=0, term;

#ifdef XFIG
 static int Case=0; 
 char fname[50]; 
  
 sprintf(fname,"Graphic_case_%d.fig",Case++); 
 fprintf(stderr,"Align_Scaffold writing xfig to %s\n", fname); 
 figfile = fopen(fname,"w"); 
 Draw_Matrix(seglist,numsegs,AF,BF,varwin); 
#endif


#ifdef AHANG_BAND_TEST
  assert(bandbeg<=bandend);
  assert(bandend<=AF->length);
  assert(bandbeg>=-(BF->length));
#endif

  if (numsegs > MaxAlign)
    { MaxAlign = (int)(1.3*numsegs + 100);
      CtgOvls  = (COvlps *) realloc(CtgOvls,sizeof(COvlps)*MaxAlign);
      if (CtgOvls == NULL)
        { fprintf(stderr,"Out of memory allocating DP array\n");
          exit (1);
        }
    }

  if (AF->num_gaps + BF->num_gaps + 2 > MaxBucket)
    { MaxBucket = (int)(1.3*(AF->num_gaps + BF->num_gaps + 2) + 100);
      ABuckets  = (COvlps **) realloc(ABuckets,sizeof(COvlps *)*MaxBucket);
      if (ABuckets == NULL)
        { fprintf(stderr,"Out of memory allocating segment sort arrays\n");
          exit (1);
        }
    }
  BBuckets  = ABuckets + (AF->num_gaps+1);

  { int i,c;
    Segment *s;


    for (i = 0; i <= AF->num_gaps; i++)
      ABuckets[i] = NULL;
    for (i = 0; i <= BF->num_gaps; i++)
      BBuckets[i] = NULL;

    c = numsegs;
    for (s = seglist; s != NULL; s = s->next)
      { c -= 1;
        CtgOvls[c].seg = s;
        CtgOvls[c].best = -1;
        CtgOvls[c].trace = NULL;

#ifdef DEBUG_SEGORDER
	fprintf(stderr,"CtgOvls[%d] actg: %d bctg: %d\n",
		c,CtgOvls[c].seg->a_contig,
		CtgOvls[c].seg->b_contig);
#endif
	// push segment onto Alink list; this needs to result in all 
	// segments involving s->a_contig being linked together,
	// and the order of the elements should be such that
	// s->b_contig <= s->Alink->b_contig (if s->Alink != NULL)

        CtgOvls[c].Alink = ABuckets[s->a_contig];
        ABuckets[s->a_contig] = CtgOvls+c;
	if(ABuckets[s->a_contig]->Alink!=NULL)
	  assert(ABuckets[s->a_contig]->seg->b_contig <= ABuckets[s->a_contig]->Alink->seg->b_contig);

	// original code did something similar for BBuckets and Blink,
      }


    // push segment onto Blink list; this needs to result in all 
    // segments involving s->b_contig being linked together,
    // and the order of the elements should be such that
    // s->a_contig <= s->Blink->a_contig (if s->Blink != NULL)
    
    for(i=AF->num_gaps;i>=0;i--){
      COvlps *co;
      co = ABuckets[i];
      while(co!=NULL){
	co->Blink = BBuckets[co->seg->b_contig];
        BBuckets[co->seg->b_contig] = co;
	if(co->Blink!=NULL)
	  assert(co->seg->a_contig <= co->Blink->seg->a_contig);
	co=co->Alink;
      }
    }

  }

#ifdef DEBUG_ALIGN
  { Segment *s;
    COvlps  *c;
    int      i;

    fprintf(stderr,"\nAlign Scaffolds\n\n  Seglist:\n");
    for (s = seglist; s != NULL; s = s->next)
      fprintf(stderr,"    (%d,%d)\n",s->a_contig,s->b_contig);
    fprintf(stderr,"\n  A-Buckets:\n");
    for (i = 0; i <= AF->num_gaps; i++)
      { fprintf(stderr,"    %2d:",i);
        for (c = ABuckets[i]; c != NULL; c = c->Alink)
          fprintf(stderr," %d",c->seg->b_contig);
        fprintf(stderr,"\n");
      }
    fprintf(stderr,"\n  B-Buckets:\n");
    for (i = 0; i <= BF->num_gaps; i++)
      { fprintf(stderr,"    %2d:",i);
        for (c = BBuckets[i]; c != NULL; c = c->Blink)
          fprintf(stderr," %d",c->seg->a_contig);
        fprintf(stderr,"\n");
      }
    fprintf(stderr,"\n");
  }
#endif


  { int i;

    for (i = 0; i < AF->num_gaps; i++)
      { int low, hgh;
  
        low = AF->ctgs[i].lft_end + AF->ctgs[i].length;
        hgh = AF->ctgs[i+1].lft_end;

#ifdef AHANG_BAND_TEST
	if(low>bandend||hgh<bandbeg)continue;
	if(low<bandbeg)low=bandbeg;
	if(hgh>bandend)hgh=bandend;
#endif


#ifdef DEBUG_ALIGN
        fprintf(stderr,"  Start ray A[%d,%d] at %d,%d\n",low,hgh,0,i);
#endif

	term = Link_Horizontal(AF, BF, varwin, i, 0, 
			     // begin point negative by hgh - gap begin
			     -( hgh - (AF->ctgs[i].lft_end+AF->ctgs[i].length)),
			     // end point negative by low - gap begin
			     -( low - (AF->ctgs[i].lft_end+AF->ctgs[i].length)),
			     NULL);

#ifdef ALLOW_NO_OVERLAP_INTERLEAVINGS
	// COMMENT ABC: if we allow the no-overlap interleavings, we need to 
	// set term and test whether we found a solution with
	// no overlaps

        if (term && score > mval)
          { mval = score;
  #ifdef STATS
            edgecount += 1;
  #endif
            optc = -1; // special value indicating no segments used
          }

#else
	// COMMENT CBA: with overlaps required, we don't need to check for
	// whether we reached a boundary; if we did *and* it involved
	// an overlap (that is, a successful overlapping), we will discover it
	// when we test the relevant segment(s) below
#endif



      }
    for (i = 0; i < BF->num_gaps; i++)
      { int low, hgh;
   
        low = BF->ctgs[i].lft_end + BF->ctgs[i].length;
        hgh = BF->ctgs[i+1].lft_end;

#ifdef AHANG_BAND_TEST
	// adjust low and hgh to conform to the banding 
	if(-hgh>bandend||-low<bandbeg)continue;
	if(-hgh<bandbeg)hgh=-bandbeg;
	if(-low>bandend)low=-bandend;
#endif

#ifdef DEBUG_ALIGN
        fprintf(stderr,"  Start ray B[%d,%d] at %d,%d\n",low,hgh,0,i);
#endif


	term = Link_Vertical(AF, BF, varwin, 0, i, 
			     // begin point negative by hgh - gap begin
			     -( hgh - (BF->ctgs[i].lft_end+BF->ctgs[i].length)),
			     // end point negative by low - gap begin
			     -( low - (BF->ctgs[i].lft_end+BF->ctgs[i].length)),
			     NULL);

#ifdef ALLOW_NO_OVERLAP_INTERLEAVINGS

	// see comment ABC above

        if (term && score > mval)
          { mval = score;
  #ifdef STATS
            edgecount += 1;
  #endif
            optc = -1; // special value indicating no segments used
          }
#else
	// see comment CBA above
#endif

      }
  }


  { COvlps *c;

    // over all segments involving the first A contig
    for (c = ABuckets[0]; c != NULL; c = c->Alink)
      // if ahang is negative, the overlap starts along the B edge, so it is a starting point
      if (c->seg->overlap->begpos <= 0 
#ifdef AHANG_BAND_TEST
	  && (c->seg->overlap->begpos - BF->ctgs[c->seg->b_contig].lft_end) <= bandend
	  && (c->seg->overlap->begpos - BF->ctgs[c->seg->b_contig].lft_end) >= bandbeg
#endif
	  )
        { score = c->seg->overlap->length - max(0,-c->seg->overlap->begpos) - max(0,-c->seg->overlap->endpos);
	assert(score>=0);
          if (score > c->best)
            { c->best  = score;
              c->trace = NULL;
            }
#ifdef DEBUG_ALIGN
          fprintf(stderr,"  Start path (%d,%d) = %d\n",
                 c->seg->a_contig,c->seg->b_contig,c->best);
#endif
        }
    // over all segments involving the first B contig
    for (c = BBuckets[0]; c != NULL; c = c->Blink)
      // if ahang is positive, the overlap starts along the A edge, so it is a starting point
      if (c->seg->overlap->begpos >= 0
#ifdef AHANG_BAND_TEST
	  && (c->seg->overlap->begpos + AF->ctgs[c->seg->a_contig].lft_end) <= bandend
	  && (c->seg->overlap->begpos + AF->ctgs[c->seg->a_contig].lft_end) >= bandbeg
#endif
)
        { score = c->seg->overlap->length - max(0,-c->seg->overlap->begpos) - max(0,-c->seg->overlap->endpos);
	assert(score>=0);
          if (score > c->best)
            { c->best  = score;
              c->trace = NULL;
            }
#ifdef DEBUG_ALIGN
           fprintf(stderr,"  Start path (%d,%d) = %d\n",
                 c->seg->a_contig,c->seg->b_contig,c->best);
#endif
        }
  }


  {
    int ca;
    for( ca=0; ca<=AF->num_gaps;ca++){
      COvlps *co = ABuckets[ca];
      while(co!=NULL){
	Segment *s = co->seg;
	int pnt;

	if(co->Alink!=NULL){
	  Segment *t = co->Alink->seg;
	  assert(s->a_contig==t->a_contig);
	  assert(s->b_contig<=t->b_contig);
	}


#ifdef DEBUG_ALIGN
	fprintf(stderr,"working on ctg[%d,%d] score %d\n",s->a_contig,s->b_contig,co->best);
#endif

        term = 0;

	// if this is either along the starting boundary or is an internal segment that has
	// already been reached from starting in a gap ...
        if ((score = co->best) >= 0){
            if (s->overlap->endpos < 0)
              { pnt = AF->ctgs[s->a_contig].lft_end +
                      (AF->ctgs[s->a_contig].length + s->overlap->endpos); 
#ifdef DEBUG_ALIGN
                fprintf(stderr,"  Point ray B%d at %d, source (%d,%d) = %d\n",
                       s->b_contig,pnt,s->a_contig,s->b_contig,score);
#endif
                term = Link_Vertical(AF, BF, varwin,
                                     s->a_contig, s->b_contig,
                                     pnt, pnt, co);
              }
            else 
              { pnt = BF->ctgs[s->b_contig].lft_end +
                      (BF->ctgs[s->b_contig].length - s->overlap->endpos); 
#ifdef DEBUG_ALIGN
                fprintf(stderr,"  Point ray A%d at %d, source (%d,%d) = %d\n",
                       s->a_contig,pnt,s->a_contig,s->b_contig,score);
#endif
                term = Link_Horizontal(AF, BF, varwin,
                                       s->a_contig, s->b_contig,
                                       pnt, pnt, co);
              }
          }
        if (term && score > mval)
          { mval = score;
#ifdef STATS
            edgecount += 1;
#endif

            optco = co;
#ifdef DEBUG_OPTTAIL
	    fprintf(stderr, "Best path so far ends at ctgs[%d,%d} score  %d\n",
		    optco->seg->a_contig,
		    optco->seg->b_contig,
		    mval);
#endif
          }
	co=co->Alink;
        }

      }
  }



  if (mval <= 0)
    { seglist = NULL;
#ifdef DEBUG_ALIGN
      fprintf(stderr,"  NO OVERLAP\n");
#endif

      // remember to NOT free seglist members here 
      // --- they will be used in Compare_Scaffold if this fcn returns NULL


      // BUT IF WE ARE NOT USING COMPARE_SCAFFOLD, OR OTHER CODE THAT KEEPS
      // THE ORIGINAL POINTER, 
      // I.E. cgw InterleavedMerging CODE,
      // IS THIS A MEMORY LEAK? -- ALH

    }
  else
    { Segment *s, *r;
      COvlps  *c;

      for(optc=0;optc<numsegs;optc++){
	if(CtgOvls+optc==optco)break;
      }
      assert(optc<numsegs);
#ifdef DEBUG_OPTTAIL
      fprintf(stderr,"Optimal path ends at ctgs[%d,%d] score %d\n",
	      CtgOvls[optc].seg->a_contig,
	      CtgOvls[optc].seg->b_contig,
	      CtgOvls[optc].best);
#endif
      assert(optc>=0);

      for (c = CtgOvls + optc; c != NULL; c = c->trace)
	c->seg->alow = - (c->seg->alow+1);

      for (s = seglist; s != NULL; s = r)
        { r = s->next;
          if (s->alow >= 0)
            { free(s->overlap);
              free(s);
            }
          else
            s->alow = - (s->alow+1);
        }

      r = NULL;
      for (c = CtgOvls + optc; c != NULL; c = c->trace)
	{ s = c->seg;
	s->next = r;
	r = s;
	}

      seglist = r;

#ifdef DEBUG_ALIGN
      fprintf(stderr,"  Best overlap ends at (%d,%d) = %d\n",
             CtgOvls[optc].seg->a_contig,CtgOvls[optc].seg->b_contig,
             CtgOvls[optc].best);
#endif
    }

#ifdef XFIG
  fclose(figfile);
#endif

#ifdef STATS
  vertcount  += numsegs+2;
  boundcount += AF->num_gaps + BF->num_gaps;
#endif

  *best = mval;
  return (seglist);
}
